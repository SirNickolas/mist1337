screen menu_mist_1337:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover.jpg"
    textbutton "Начать":       
        xalign .9
        yalign .33
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('label_mist_1')

    textbutton "Часть 2":
        xalign .9
        yalign .44
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('label_mist_2')

    textbutton "Бонусная история":        
        xalign .9
        yalign .55
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('mist1337_bonus_story')
        
    textbutton "Информация":        
        xalign .9
        yalign .66
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('info_1337')
        
    textbutton "Выход":        
        xalign .9
        yalign .77
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('mist1337_quit')
        
screen info_1337:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover.jpg"
    textbutton "Назад":
        xalign 0.07
        yalign 0.9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337')

    textbutton "Использованные материалы":
        xalign .5
        yalign .3
        style "log_button"
        text_style "dashotamgovorit"
        action OpenURL("https://pastebin.com/PzqNgwUe")

    textbutton "Благодарности":
        xalign .5
        yalign .5
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('mercibeakoup')

    textbutton "Legal information":
        xalign .5
        yalign .7
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('legal')

screen mercibeakoup:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover.jpg"
    textbutton "Благодарности:":       
        xalign .5
        yalign .1
        style "log_button"
        text_style "dashotamgovorit"
    textbutton "Всем участникам группы ВК.\nБез поддержки этой небольшой группы людей проект не увидел бы свет ещё месяцев шесть;":       
        xalign .3
        yalign .3
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Всем-всем талантливым людям, чьи работы были использованы в данном моде (и пользователю EVEREST за превосходную картинку на превью!);":       
        xalign .9
        yalign .4
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Артуру Конан Дойлю, Агате Кристи, студиям Frogwares и Big Bad Wolf Interactive, чьи работы послужили вдохновением;":       
        xalign .8
        yalign .5
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Сэру Николасу за вычитку текста и исправление ошибок;":       
        xalign .15
        yalign .6
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Всем мододелам и читателям за сохранение и развитие вселенной Бесконечного лета.":       
        xalign .3
        yalign .7
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Назад":
        xalign 0.07
        yalign .9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337')

screen legal:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover.jpg"
    textbutton "Legal:":       
        xalign .5
        yalign .2
        style "log_button"
        text_style "dashotamgovorit"
    textbutton "Данный мод является полностью вымышленным. Любые совпадения с городами, зданиями, историческими личностями и персоналиями являются случайными.\n \nВ работе были использованы изображения, музыка и звуки, взятые из Интернета. Данный проект не нацелен на получение прибыли и выпущен по лицензии Creative Commons BY-SA, подразумевающей его бесплатное использование и распространение.\n \nЕсли вы выступаете против использования своей работы в данном моде, пожалуйста, свяжитесь с разработчиком.":
        xalign .3
        yalign .5
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Назад":
        xalign 0.07
        yalign .9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337')



screen menu_mist_1337_good:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover_good.jpg"
    textbutton "Начать":       
        xalign .9
        yalign .33
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('label_mist_1')

    textbutton "Часть 2":
        xalign .9
        yalign .44
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('label_mist_2')

    textbutton "Бонусная история":        
        xalign .9
        yalign .55
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('mist1337_bonus_story')
        
    textbutton "Информация":        
        xalign .9
        yalign .66
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('info_1337_good')
        
    textbutton "Выход":        
        xalign .9
        yalign .77
        style "log_button"
        text_style "dashotamgovorit"
        action Jump('mist1337_quit')
        
screen info_1337_good:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover_good.jpg"
    textbutton "Назад":
        xalign 0.07
        yalign 0.9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337_good')

    textbutton "Использованные материалы":
        xalign .5
        yalign .3
        style "log_button"
        text_style "dashotamgovorit"
        action OpenURL("https://pastebin.com/PzqNgwUe")

    textbutton "Благодарности":
        xalign .5
        yalign .5
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('mercibeakoup_good')

    textbutton "Legal information":
        xalign .5
        yalign .7
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('legal_good')

screen mercibeakoup_good:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover_good.jpg"
    textbutton "Благодарности:":       
        xalign .5
        yalign .1
        style "log_button"
        text_style "dashotamgovorit"
    textbutton "Всем участникам группы ВК.\nБез поддержки этой небольшой группы людей проект не увидел бы свет ещё месяцев шесть;":       
        xalign .3
        yalign .3
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Всем-всем талантливым людям, чьи работы были использованы в данном моде (и пользователю EVEREST за превосходную картинку на превью!);":       
        xalign .9
        yalign .4
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Артуру Конан Дойлю, Агате Кристи, студиям Frogwares и Big Bad Wolf Interactive, чьи работы послужили вдохновением;":       
        xalign .8
        yalign .5
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Сэру Николасу за вычитку текста и исправление ошибок;":       
        xalign .15
        yalign .6
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Всем мододелам и читателям за сохранение и развитие вселенной Бесконечного лета.":
        xalign .3
        yalign .7
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Назад":
        xalign 0.07
        yalign .9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337_good')

screen legal_good:
    tag menu
    modal True
    add "mods/mist1337/menu/menu_hover_good.jpg"
    textbutton "Legal:":       
        xalign .5
        yalign .2
        style "log_button"
        text_style "dashotamgovorit"
    textbutton "Данный мод является полностью вымышленным. Любые совпадения с городами, зданиями, историческими личностями и персоналиями являются случайными.\n \nВ работе были использованы изображения, музыка и звуки, взятые из Интернета. Данный проект не нацелен на получение прибыли и выпущен по лицензии Creative Commons BY-SA, подразумевающей его бесплатное использование и распространение.\n \nЕсли вы выступаете против использования своей работы в данном моде, пожалуйста, свяжитесь с разработчиком.":
        xalign .3
        yalign .5
        style "log_button"
        text_style "dashotamgovorit_2"
    textbutton "Назад":
        xalign 0.07
        yalign .9
        style "log_button"
        text_style "dashotamgovorit"
        action ShowMenu('menu_mist_1337_good')