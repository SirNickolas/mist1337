init:

    $ mods["mist13337"]=u"Схватка с неизвестностью. Полная версия."


#Characters
    $ waeber_unknown = Character(u'Человек', color="E8E8E8", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ iam = Character(u'Я', color="128119", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ kw = Character(u'Вебер', color="E8E8E8", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ voice_unknown = Character(u'Неизвестный', color="F01E14", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ kit = Character(u'Девочка с ушками', color="46c124", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ bkm = Character(u'Продавец', color="3862e0", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ pol = Character(u'Военный', color="9b9014", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ col = Character(u'Коллега', color="1fa834", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ whoisthat = Character(u'Голос позади', color="46c124", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")
    $ uv_voice = Character(u'Голос', color="46c124", what_color="E2C778", drop_shadow = [ (2, 2) ], drop_shadow_color = "#000", what_drop_shadow = [ (2, 2) ], what_drop_shadow_color = "#000")

#Sprites
    #Pioneer
    image pi2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_normal.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_normal.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_normal.png"))
    
    image pi2 smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_smile.png"))
    
    image pi2 laugh = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_grin.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_grin.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_grin.png"))
    
    image pi2 ser = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_serious.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_serious.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_serious.png"))
    
    image pi2 shocked = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_shocked.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_shocked.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_shocked.png"))
    
    image pi2 upset = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_upset.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_upset.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_upset.png"))

    image pi2 surp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_surprise.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_surprise.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_pioneer.png", (0, 0), "mods/Mist1337/spr/pi213/pi_2_surprise.png"))

    #KW_pioneer
    image kw = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw.png"))
    
    image kw smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile.png"))

    image kw ser = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_ser.png"))

    image kw disapp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_disapp.png"))
    
    image kw sad = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_sad.png"))
    
    image kw smile_2_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2_2.png"))

    image kw smile_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smile_2.png"))

    image kw smirk = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_smirk.png"))

    image kw surp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_surp_2.png"))
    
    image kw thinking = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/pi/kw_thinking_2.png"))

    #KW_tie
    image kw_tie = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie.png"))

    image kw_tie disapp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_disapp.png"))

    image kw_tie smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png", (0, 0), "mods/Mist1337/spr/kw/tie/kw_tie_smile.png"))

    #KW_jacket
    image kw jacket = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png"))

    image kw jacket ser = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_ser.png"))

    image kw jacket smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile.png"))

    image kw jacket sad = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_sad.png"))
     
    image kw jacket disapp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_disapp.png"))

    image kw jacket smile_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2.png"))

    image kw jacket smile_2_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smile_2_2.png"))

    image kw jacket thinking = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_thinking_2.png"))

    image kw jacket smirk = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/jacket/kw_jacket_smirk_2.png"))

    #KW_spring
    image kw spring = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring.png"))

    image kw spring disapp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_disapp.png"))

    image kw spring sad = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_sad.png"))

    image kw spring ser = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_ser.png"))

    image kw spring smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile.png"))

    image kw spring smile_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2.png"))

    image kw spring smile_2_2 = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smile_2_2.png"))

    image kw spring smirk = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smilrk_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_smirk_2.png"))

    image kw spring surp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_surp_2.png"))

    image kw spring thinking = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png", (0, 0), "mods/Mist1337/spr/kw/spring/kw_spring_thinking_2.png"))

    #BKM
    image bkm = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png", (0, 0), "mods/Mist1337/spr/bkm/bkm.png"))

    image bkm angry = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm_angry/bkm.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_angry.png"))

    image bkm laugh = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_laugh.png"))

    image bkm smile = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_smile.png"))

    image bkm far = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far.png"))

    image bkm far angry = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1125, 1080), (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png", (0, 0), "mods/Mist1337/spr/bkm/bkm_far_angry.png"))

    #Policeman
    image pol = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png", (0, 0), "mods/Mist1337/spr/pol/pol.png"))

    image pol disapp = ConditionSwitch(
        "persistent.sprite_time == 'sunset'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png"), im.matrix.tint(0.94, 0.82, 1.0)), 
        "persistent.sprite_time == 'night'", im.MatrixColor(im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png"), im.matrix.tint(0.63, 0.78, 0.82)), 
        True, im.Composite((1050, 1080), (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png", (0, 0), "mods/Mist1337/spr/pol/pol_disapp.png"))


#Music
    $ fear = "mods/Mist1337/music/fear.ogg"
    $ think_deeply = "mods/Mist1337/music/think_deeply.ogg"
    $ endless_rain = "mods/Mist1337/music/endless_rain.ogg"
    $ strange = "mods/Mist1337/music/strange.mp3"
    $ gadget = "mods/Mist1337/music/gadget.ogg"
    $ sunset = "mods/Mist1337/music/sunset.ogg"
    $ quiet_night = "mods/Mist1337/music/quiet_night.ogg"
    $ tricky_code = "mods/Mist1337/music/tricky_code.ogg"
    $ curious_critters = "mods/Mist1337/music/curious_critters.ogg"
    $ gangster = "mods/Mist1337/music/gangster.ogg"
    $ what_you_can = "mods/Mist1337/music/what_you_can.ogg"
    $ otl_lolor = "mods/Mist1337/music/otl_lolor.ogg"
    $ tchaikovsky = "mods/Mist1337/music/tchaikovsky.ogg"
    $ radio_on = "mods/Mist1337/music/radio_on.mp3"
    $ radio_tuning = "mods/Mist1337/music/radio_tuning.mp3"
    $ roulette = "mods/Mist1337/music/roulette.mp3"
    $ tsoy = "mods/Mist1337/music/tsoy.mp3"
    $ erika = "mods/Mist1337/music/erika.mp3"
    $ zuruck_zu_dir = "mods/Mist1337/music/zuruck_zu_dir.mp3"
    $ texas = "mods/Mist1337/music/texas.mp3"
    $ vietnam_1 = "mods/Mist1337/music/vietnam_1.mp3"
    $ preparations = "mods/Mist1337/music/preparations.mp3"
    $ butterflies_sonata = "mods/Mist1337/music/butterflies_sonata.mp3"
    $ neboslavyan = "mods/Mist1337/music/neboslavyan.mp3"
    $ heartbeat = "mods/Mist1337/music/heartbeat.mp3"
    $ fineoutside = "mods/Mist1337/music/fineoutside.mp3"
    $ fineoutside2 = "mods/Mist1337/music/fineoutside2.mp3"
    $ lonesome_summer = "mods/Mist1337/music/lonesome_summer.mp3"
    $ bestfriends = "mods/Mist1337/music/bestfriends.mp3"
    $ snowflakes = "mods/Mist1337/music/snowflakes.mp3"
    $ reminiscences2 = "mods/Mist1337/music/reminiscences2.mp3"
    $ hard_rock = "mods/Mist1337/music/hard_rock.mp3"
    $ ab_ovo = "mods/Mist1337/music/ab_ovo.mp3"
    $ just_memories = "mods/Mist1337/music/just_memories.mp3"
    $ happy_ending_melody = "mods/Mist1337/music/happy_ending_melody.mp3"
    $ association = "mods/Mist1337/music/association.mp3"
    $ chilmilk = "mods/Mist1337/music/chilmilk.mp3"
    $ loved_ones = "mods/Mist1337/music/loved_ones.mp3"
    $ blue_h_one = "mods/Mist1337/music/blue_h_one.mp3"
    $ final_inquiry = "mods/Mist1337/music/final_inquiry.mp3"
    $ loveis = "mods/Mist1337/music/loveis.mp3"
    $ nowayback = "mods/Mist1337/music/nowayback.mp3"


#Sound
    $ ddlc = "mods/Mist1337/sound/ddlc.ogg"
    $ paper = "mods/Mist1337/sound/paper.mp3"
    $ paper_bucket = "mods/Mist1337/sound/paper_bucket.mp3"
    $ water_bucket = "mods/Mist1337/sound/water_bucket.mp3"
    $ badumtssch = "mods/Mist1337/sound/badumtssch.mp3"
    $ sniff = "mods/Mist1337/sound/sniff.mp3"
    $ jacket = "mods/Mist1337/sound/jacket.wav"
    $ making_tea = "mods/Mist1337/sound/making_tea.mp3"
    $ watchoutforthepolice = "mods/Mist1337/sound/watchoutforthepolice.mp3"
    $ bike_around = "mods/Mist1337/sound/bike_around.mp3"
    $ the_machine_stops = "mods/Mist1337/sound/the_machine_stops.mp3"
    $ telephone_ring = "mods/Mist1337/sound/telephone_ring.mp3"
    $ boom_01 = "mods/Mist1337/sound/boom_01.mp3"
    $ street_crowded = "mods/Mist1337/sound/street_crowded.mp3"
    $ steps = "mods/Mist1337/sound/steps.mp3"
    $ street_consular = "mods/Mist1337/sound/street_consular.mp3"
    $ train_moving = "mods/Mist1337/sound/train_moving.mp3"
    $ city_traffic = "mods/Mist1337/sound/city_traffic.mp3"
    $ sob_sl = "mods/Mist1337/sound/sob_sl.mp3"
    $ inhale = "mods/Mist1337/sound/inhale.mp3"
    $ button_01 = "mods/Mist1337/sound/button_01.mp3"
    $ scary_turnitoff_reverse = "mods/Mist1337/sound/scary_turnitoff_reverse.mp3"
    $ scary_turnitoff = "mods/Mist1337/sound/scary_turnitoff.mp3"
    $ air_01 = "mods/Mist1337/sound/air_01.mp3"
    $ air_02 = "mods/Mist1337/sound/air_02.mp3"
    $ air_03 = "mods/Mist1337/sound/air_03.mp3"
    $ coughing = "mods/Mist1337/sound/coughing.mp3"
    $ clapclap = "mods/Mist1337/sound/clapclap.mp3"
    $ dialling = "mods/Mist1337/sound/dialling.mp3"
    
#Background
    image bg ext_houses_night_7dl = "mods/Mist1337/bg/ext_houses_night_7dl.jpg"
    image bg ext_backdoor_night_7dl = "mods/Mist1337/bg/ext_backdoor_night_7dl.jpg"
    image bg ext_warehouse_day_7dl ="mods/Mist1337/bg/ext_warehouse_day_7dl.jpg"
    image bg house_dv_bucket = "mods/Mist1337/bg/house_dv_bucket.png"
    image bg ext_banya_something = "mods/Mist1337/bg/ext_banya_something.jpg"
    image bg ext_bathhouse_night_1 = "mods/Mist1337/bg/ext_bathhouse_night_1.jpg"
    image bg ext_clubs_sunset = "mods/Mist1337/bg/ext_clubs_sunset.jpg"
    image bg ext_bathhouse_night_1_blur = "mods/Mist1337/bg/ext_bathhouse_night_1_blur.jpg"
    image bg ext_bathhouse_night_2 = "mods/Mist1337/bg/ext_bathhouse_night_2.jpg"
    image bg od_house_close = "mods/Mist1337/bg/od_house_close.jpg"
    image bg darkroom = "mods/Mist1337/bg/darkroom.jpg"
    image bg ext_bathhouse_night_1_close = "mods/Mist1337/bg/ext_bathhouse_night_1_close.jpg"
    image bg ext_old_building_day_7dl = "mods/Mist1337/bg/ext_old_building_day_7dl.jpg"
    image bg int_house_of_sl_sunset = "mods/Mist1337/bg/int_house_of_sl_sunset.jpg"
    image bg ext_house_of_sl_night = "mods/Mist1337/bg/ext_house_of_sl_night.jpg"
    image bg int_musclub_day_2 = "mods/Mist1337/bg/int_musclub_day_2.jpg"
    image bg ext_music_club_verandah_day = "mods/Mist1337/bg/ext_music_club_verandah_day.jpg"
    image bg darkroom_camp = "mods/Mist1337/bg/darkroom_camp.jpg"
    image bg corridor = "mods/Mist1337/bg/corridor.png"
    image bg pi213_cabinet = "mods/Mist1337/bg/pi213_cabinet.jpg"
    image bg tv_screen = "mods/Mist1337/bg/tv_screen.png"
    image bg kw_office = "mods/Mist1337/bg/kw_office.jpg"
    image bg kw_office_close = "mods/Mist1337/bg/kw_office_close.jpg"
    image bg ext_musclub_night_7dl = "mods/Mist1337/bg/ext_musclub_night_7dl.jpg"
    image bg int_music_club_mattresses_night = "mods/Mist1337/bg/int_music_club_mattresses_night.jpg"
    image bg widnow_outside = "mods/Mist1337/bg/widnow_outside.jpg"
    image bg widnow_outside_marked = "mods/Mist1337/bg/widnow_outside_marked.jpg"
    image bg widnow_outside_marked_close = "mods/Mist1337/bg/widnow_outside_marked_close.jpg"
    image bg pi213_cabinet_night = "mods/Mist1337/bg/pi213_cabinet_night.jpg"
    image bg corridor_day = "mods/Mist1337/bg/corridor_day.jpg"
    image bg stained = "mods/Mist1337/bg/stained.jpg"
    image bg stained_close = "mods/Mist1337/bg/stained_close.jpg"
    image bg rocambole = "mods/Mist1337/bg/rocambole.jpg"

#Background_bonus
    image bg consular = "mods/Mist1337/bg/bonus_story/consular.jpg"
    image bg consular_street = "mods/Mist1337/bg/bonus_story/consular_street.jpg"
    image bg consular_square = "mods/Mist1337/bg/bonus_story/consular_square.jpg"
    image bg garden = "mods/Mist1337/bg/bonus_story/garden.jpg"
    image bg garden_2 = "mods/Mist1337/bg/bonus_story/garden_2.jpg"
    image bg garden_bookshop = "mods/Mist1337/bg/bonus_story/garden_bookshop.jpg"
    image bg garden_outside_path = "mods/Mist1337/bg/bonus_story/garden_outside_path.jpg"
    image bg garden_stones = "mods/Mist1337/bg/bonus_story/garden_stones.jpg"
    image bg hotel_nearby = "mods/Mist1337/bg/bonus_story/hotel_nearby.jpg"
    image bg hotel_window = "mods/Mist1337/bg/bonus_story/hotel_window.jpg"
    image bg lunch_time = "mods/Mist1337/bg/bonus_story/lunch_time.jpg"
    image bg park_1 = "mods/Mist1337/bg/bonus_story/park_1.jpg"
    image bg park_jul = "mods/Mist1337/bg/bonus_story/park_jul.jpg"
    image bg park_lake = "mods/Mist1337/bg/bonus_story/park_lake.jpg"
    image bg park_path = "mods/Mist1337/bg/bonus_story/park_path.jpg"
    image bg pingjianglu_boat = "mods/Mist1337/bg/bonus_story/pingjianglu_boat.jpg"
    image bg railway_station_1 = "mods/Mist1337/bg/bonus_story/railway_station_1.jpg"
    image bg scenery_qiao = "mods/Mist1337/bg/bonus_story/scenery_qiao.jpg"
    image bg scenery_qiao_2 = "mods/Mist1337/bg/bonus_story/scenery_qiao_2.jpg"
    image bg scenery_qiao_3 = "mods/Mist1337/bg/bonus_story/scenery_qiao_3.jpg"
    image bg shanghai_str = "mods/Mist1337/bg/bonus_story/shanghai_str.jpg"
    image bg su_railway = "mods/Mist1337/bg/bonus_story/su_railway.jpg"
    image bg su_str = "mods/Mist1337/bg/bonus_story/su_str.jpg"
    image bg train_inside = "mods/Mist1337/bg/bonus_story/train_inside.jpg"
    image bg su_ppl = "mods/Mist1337/bg/bonus_story/su_ppl.jpg"
    image bg pi213_cabinet_box = "mods/Mist1337/bg/bonus_story/pi213_cabinet_box.jpg"


#Sprites (additional)
    image un_normal_wet_1 = "mods/Mist1337/spr/un_normal_wet.png"
    image un_rage_wet_1 = "mods/Mist1337/spr/un_rage_wet.png"
    image waeber_unknown = "mods/Mist1337/spr/waeber_unknown.png"

     
#CG
    image cg book = "mods/Mist1337/cg/book.jpg"
    image cg semen_smile = "mods/Mist1337/cg/semen_smile.jpg"
    image cg semen_reserved = "mods/Mist1337/cg/semen_reserved.jpg"
    image cg miku_piano = "mods/Mist1337/cg/miku_piano.jpg"
    image cg us_sneakpeak_7dl = "mods/Mist1337/cg/us_sneakpeak_7dl.png"
    image cg sl_mirror_1 = "mods/Mist1337/cg/sl_mirror_1.jpg"
    image cg sem_sl_room = "mods/Mist1337/cg/sem_sl_room.jpg"
    image cg ext_park_evening = "mods/Mist1337/bg/ext_park_evening.png"

#Pictures
    image note_02 = "mods/Mist1337/pictures/note_02.png"
    image note_02_2 = "mods/Mist1337/pictures/note_02_2.png"
    image note_03 = "mods/Mist1337/pictures/note_03.png"
    image note_03_3 = "mods/Mist1337/pictures/note_03_3.png"
    image red_pen = "mods/Mist1337/pictures/red_pen.png"
    image red_pen_1 = "mods/Mist1337/pictures/red_pen_1.png"
    image red_pen_2 = "mods/Mist1337/pictures/red_pen_2.png"
    image note_04 = "mods/Mist1337/pictures/note_04.png"
    image note_04_4 = "mods/Mist1337/pictures/note_04_4.png"
    image note_05_5 = "mods/Mist1337/pictures/note_05_5.png"
    image note_06_6 = "mods/Mist1337/pictures/note_06_6.png"
    image note_07_7 = "mods/Mist1337/pictures/note_07_7.png"
    image note_07_8 = "mods/Mist1337/pictures/note_07_8.png"
    image note_07_9 = "mods/Mist1337/pictures/note_07_9.png"
    image note_07_10 = "mods/Mist1337/pictures/note_07_10.png"
    image vietnam = "mods/Mist1337/pictures/vietnam.png"
    image note_08_11 = "mods/Mist1337/pictures/note_08_11.png"
    image partone = "mods/Mist1337/pictures/partone.jpg"
    image parttwo = "mods/Mist1337/pictures/parttwo.jpg"
    image partthree = "mods/Mist1337/pictures/partthree.jpg"
    image bonus_cover = "mods/Mist1337/pictures/bonus_cover.jpg"
    image redscreen = "mods/Mist1337/pictures/redscreen.png"
    image redscreen2 = "mods/Mist1337/pictures/redscreen2.png"
    image man_sitting = "mods/Mist1337/pictures/man_sitting.png"
    image phone_01 = "mods/Mist1337/pictures/phone_01.png"
    image phone_02 = "mods/Mist1337/pictures/phone_02.png"
    image phone_03 = "mods/Mist1337/pictures/phone_03.png"
    image phone_04 = "mods/Mist1337/pictures/phone_04.png"
    image cigarette = "mods/Mist1337/pictures/cigarette.png"
    image book_bookshop = "mods/Mist1337/pictures/book_bookshop.png"
    image book_title = "mods/Mist1337/pictures/book_title.png"
    image letter_ending = "mods/Mist1337/pictures/letter_ending.png"
    image stain = "mods/Mist1337/pictures/stain.png"
    image stain_2 = "mods/Mist1337/pictures/stain_2.png"
    image notepad = "mods/Mist1337/pictures/notepad.png"
    image note_final_1 = "mods/Mist1337/pictures/note_final_1.png"
    image note_final_2 = "mods/Mist1337/pictures/note_final_2.png"
    image note_guesswho = "mods/Mist1337/pictures/note_guesswho.png"
    image note_guesswho_2 = "mods/Mist1337/pictures/note_guesswho_2.png"

#Fonts 

    $ dashotamgovorit = "mods/mist1337/menu/gothic_norm.ttf"
    $ style.dashotamgovorit = Style(style.default)
    $ style.dashotamgovorit.font  = dashotamgovorit
    $ style.dashotamgovorit.size = 60
    $ style.dashotamgovorit.color = "#ffffff"
    $ style.dashotamgovorit.hover_color = "#0000ff"

    $ dashotamgovorit_2 = "mods/mist1337/menu/gothic_norm_2.ttf"
    $ style.dashotamgovorit_2 = Style(style.default)
    $ style.dashotamgovorit_2.font  = dashotamgovorit_2
    $ style.dashotamgovorit_2.size = 30
    $ style.dashotamgovorit_2.color = "#ffffff"
    $ style.dashotamgovorit_2.hover_color = "#0000ff"


label mist13337:
    play music butterflies_sonata
    play ambience ambience_camp_entrance_night
    call screen menu_mist_1337

label mist13337_bad:
    call screen menu_mist_1337

label mist13337_good:
    call screen menu_mist_1337_good

label label_mist_1:
    $ new_chapter(1, u"Схватка с неизвестностью.\nЧасть 1: Восхождение.")
    $ renpy.pause (2,hard=True)
    show partone with dissolve
    $ renpy.pause (2,hard=True)
    hide partone with dissolve
    $ prolog_time()
    scene anim prolog_1 with dissolve
    play music fear fadein 2
    "Вдох… {w} Выдох." 
    "Я закрываю глаза и представляю дом.{w} Город… работу."
    "Ту реальность, которую я считаю настоящей."
    "Ту, из которой меня вырвали."
    "Это словно дурной сон, но…{w} Все дурные сны рано или поздно заканчиваются, так?"
    "Закончится и этот."
    me "Ну… {w} Давай!"
    stop music fadeout 2
    window hide
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg int_library_day with flash
    $ renpy.pause (1)
    play ambience ambience_library_day
    window show
    $ renpy.pause (1)
    "Я резко открываю глаза и трясу головой."
    "Ничего…{w} Как и в прошлый раз."
    "Никакого эффекта моя минутная слабость не дала…"
    show mz angry glasses pioneer far at left with dissolve
    mz "Эй! Ты чего это там делаешь?!"
    "…лишь привлекла внимание Жени."
    show mz angry glasses pioneer at center with dissolve
    mz "Правила читал? Пункт третий гласит: «Спать в библиотеке воспрещается».{w} Хочешь дрыхнуть — дрыхни в комнате!"
    mz "Совсем сдурел, что ли?"
    "С каждым словом она будто распаляется всё сильнее."
    show mz rage glasses pioneer with dspr
    mz "А ну, встал! {w}И вышел отсюда!"
    "От её громкого голоса никуда не деться: он, словно незримый туман, заполняет всё пространство большой комнаты, отвлекая пионеров от чтения или отлынивания от поручений вожатых."
    "Ну а что ещё в такую погоду в библиотеке делать?"
    "Но всё же…{w} Как-то всё равно неудобно."
    $ renpy.pause (1)
    "Я склоняю голову и придаю лицу виноватое выражение." 
    me "Женя, прости, пожалуйста! Я просто задумался…"
    $ renpy.pause (2)

    menu:
        "Над классовыми различиями в одежде Древнего Египта":
            me "Ну очень важная тема же!"
            show mz angry glasses pioneer at center with dspr
            me "Ты вот, например, знала, что калазирис носили и царицы, и обычные женщины?"
            me "Даёт ли это нам право заявить, что коммунистический строй берёт своё рождение в Египте?..{w} Или это всё лишь домыслы, придуманные западной пропагандой с целью очернить светлое имя Маркса…{w} и Ленина?"
            "Как ни странно, этот довольно простой приём срабатывает. Женя морщится и смотрит на меня уже с гораздо меньшей яростью."
            show mz normal glasses pioneer at center with dspr
            mz "Ладно-ладно, хватит. На первый раз обойдёмся предупреждением."
            show mz angry glasses pioneer at center with dspr
            mz "Но чтобы впредь такого не было!"
        
        "Почему около столовой каждый вечер поют Цоя":
            show mz angry glasses pioneer at center with dspr
            mz "А мне откуда знать?"
            mz "Я-то с ними не пою, будь уверен!"
            $ renpy.pause (1)
            show mz normal glasses pioneer at center with dspr
            mz "А что поют, кстати?"
            me "Да из последнего в основном…"
            me "Можешь тоже как-нибудь прийти послушать! Алиса на гитаре ух как исполняет!"
            me "Лучше самого Цоя!"
            show mz angry glasses pioneer at center with dspr
            mz "Лучше самого Цоя может исполнить только сам Цой!"
            mz "И с чего ты взял, что мне это вообще интересно?"
            $ renpy.pause (2)
            show mz normal glasses pioneer at center with dspr
            mz "Ладно, не дрыхни тут. По ночам вообще-то спать надо, а не Цоя петь."
        
        "Почему небо голубое, а травка зелёная":
            show mz normal glasses pioneer at center with dspr
            $ renpy.pause (1)
            mz "А. {w}Я поняла."
            mz "Знаешь, Семён, крыша должна ехать последовательно.{w} Постепенно, так сказать."
            mz "Ты, я вижу, прыгнул с места в карьер."
            mz "Но не буду отвлекать."
            mz "С такими связываться — себе дороже."
            $ renpy.pause (0.5)

    hide mz with dissolve
    "Она удаляется, что-то бормоча себе под нос."
    "Я смотрю ей вслед и думаю, что этой библиотеке с руководителем явно повезло.{w} Не забалуешь!"
    "Помню, в институте тоже была библиотека."
    "Там не было такого большого количества коммунистической литературы, но стоял электрический чайник на два литра, а маленькие ящички для каталогов использовались для хранения чая. Каких только сортов там не было!"
    "Я частенько проводил там вечера в компании трудов Мэхэна и Маккиндера…{w} И не я один, кстати."
    "Студенты заходили после лекций с целью немного взбодриться и оставались до самого закрытия, поглощённые какой-нибудь интересной книгой."
    th "Метод пряника? Вполне возможно."
    "Я улыбнулся приятным воспоминаниям и перевёл взгляд на книгу, лежащую передо мной."
    scene cg book with dissolve
    $ renpy.pause (0.5)
    "Что-то про Китай…"
    "Взгляд скользил от одной строки к другой, но мысли были где-то вдалеке."
    "И главная из них…"
    "«Зачем я здесь?»" with hpunch
    window hide
    $ renpy.pause (1)
    stop ambience fadeout 1
    jump memo

label memo:
    $ prolog_time()
    $ set_mode_nvl()
    play music endless_rain fadein 2
    "Как я попал сюда, думаю, объяснять не стоит. Обойдёмся лишь тем, что во время рабочего дня я прилёг на диванчик и очень некстати заснул (стыдно!)."
    "Проснулся уже здесь."
    th "Сон? Конечно, сон!"
    "Так я думал на протяжении недели и думаю сейчас, но вера в эту теорию с каждым днём становится всё призрачнее."
    "Сны не бывают такими детальными. Во снах нельзя спать и просыпаться на следующий день, отчётливо помня всё, что произошло вчера."
    nvl clear
    "Вероятно, не стоит забивать голову ерундой. Пора бы уже успокоиться." 
    "Если от меня ничего не зависит, то к чему все эти тревоги?"
    "В мире есть множество вещей, которым нельзя найти рациональное объяснение."
    "Как неживая материя становится живой?"
    "Каковы истинные принципы работы человеческого мозга?"
    "Почему лазер наносит чистый урон, а ракеты — магический?"
    "Хм."
    nvl clear
    "Мы живём и не задумываемся об этом. Мы принимаем эти факты такими, какие они есть."
    "Может, и мне стоит прекратить искать ответы и начать просто наслаждаться жизнью?"
    stop music fadeout 3
    $ renpy.pause (0.5)
    $ set_mode_adv()
    $ renpy.pause (0.5)
    window show
    play ambience ambience_library_day
    $ persistent.sprite_time = 'day'
    $ day_time()
    scene bg int_library_day with dissolve
    "Я вздохнул, потёр голову и бросил взгляд на часы."
    "Половина четвёртого. {w}Два часа пролетели до ужаса незаметно." 
    "Я нашарил на столе какую-то бумажку, положил её в книгу и поднялся."
    th "До ужина ещё полно времени, так что почему бы не прогуляться?"
    th "Благо уже наверняка не так жарко." 
    "Сунув том под мышку, я махнул Жене и вышел."
    window hide
    stop ambience fadeout 1
    $ renpy.pause (1)
    jump street_01
    
label street_01:
    play sound sfx_close_door_1
    play ambience ambience_camp_center_day fadein 3
    $ volume(0.8,'ambience')
    scene bg ext_library_day with dissolve
    $ renpy.pause (0.5)
    window show
    scene bg ext_library_day with dissolve
    "Крыльцо перед библиотекой встретило меня громким пением птиц."
    "Солнце всё ещё пригревало, дул прохладный ветерок, принося с собой свежесть наступающего вечера."
    th "Всё-таки в этом мире есть и свои плюсы. Как же классно в лесу!"
    "Как и любой городской работяга, я очень редко выбирался на природу, поэтому свежий воздух, пряный еловый запах и шелест листвы всё ещё казались для меня чем-то особенным."
    "Странно, что за неделю всё это так и не приелось."
    window hide
    $ renpy.pause (0.5)
    scene bg ext_square_day with dissolve
    window show
    "На площади никого.{w} Пожалуй, это даже к лучшему — никто не сможет помешать мне наслаждаться приятной погодой и предаваться всяким бесполезным, но крайне интересным мыслям."
    $ renpy.pause (2)
    "Например, куда делись мои усы?" with hpunch
    play music curious_critters fadein 1
    "До «прибытия» в лагерь у меня были тонкие чёрные (тараканьи) усы а-ля Фандорин, над которыми не пошутил только ленивый."
    "Не знаю… мне они нравились."
    "Усы должны молодить, но с ними мне вместо 25 лет давали 28 или все 30. Странность, но странность определённо приятная."
    scene bg ext_musclub_day with dissolve
    "Незаметно для себя я подошёл к зданию музыкального клуба."
    "Около него тоже не было ни души."
    "И это было по меньшей мере… странно."
    "Обычно тут народу пруд пруди."
    "И не только из-за музыки или огромного количества разных инструментов…"
    "С клубом было не всё так просто."
    stop ambience fadeout 1
    window hide
    $ renpy.pause (0.5)
    scene bg int_musclub_day with dissolve
    play ambience ambience_music_club_day fadein 2
    $ volume(0.8,'ambience')
    window show
    show mi normal pioneer far with dissolve
    "Я сразу увидел её."
    "Она стояла там же, где я всегда привык её видеть, — около зелёной доски."
    "В её красоте было что-то невообразимое, невероятное. Она притягивала взгляд, словно магнит."
    "Посмотрев на неё однажды, можно быть уверенным — забыть её не удастся никогда."
    "Увидев её впервые, я сразу понял, что она должна быть моей."
    "Она уже моя.{w} И я никому её не отдам."
    stop music fadeout 1
    $ renpy.pause (1)
    "Моя синяя электрогитара."
    show mi smile pioneer far with dspr
    mi "Ой, привет, Семён! Как дела?"
    mi "Каким ветром сюда занесло?"
    "Повернув голову, я заметил Мику, которая, как оказалось, всё это время стояла в центре комнаты."
    me "О, Мику-тян! Коннитива!"
    me "О-гэнки дэс ка?"
    show mi dontlike pioneer far with dspr
    $ renpy.pause (1.2)
    me "Ладно-ладно! Долой грустные глазки!"
    me "Русский так русский."
    "Всё равно мои познания в японском заканчиваются на этих двух выражениях."
    me "Гулял вот и решил заскочить."
    me "А чего так пусто? Стёпка-гитарист не заходил?"
    show mi normal pioneer far with dspr
    mi "Да что-то нет вот никого! Самой интересно, куда они все подевались!"
    mi "А Стёпка заходил, да. Насчёт гитары я ему сказала, как ты просил. Что трещину дала и всё прочее. Он даже поверил!"
    me "Умная девочка! А больше никого не было?"
    mi "Нет. {w} Хотя… "
    show mi smile pioneer far with dspr
    extend "Точно! Алиса была!"
    mi "Она и на гитаре поиграла, кстати."
    me "Это что же получается… "
    play music music_list["sweet_darkness"] fadein 1
    extend "Мне опять сидеть настраивать?!"
    show mi grin pioneer far with dspr
    mi "Да-а-а-а!{w} Заодно и со мной поболтаешь!"
    mi "А то знаешь как тут скучно одной!"
    mi "Давай садись!"
    "Я сел на стул и придвинул гитару поближе. Мику тут же села рядом."
    show mi normal pioneer with dspr
    mi "Что нового? Как без Ольги Дмитриевны в комнате? Лучше?"
    me "Ну… Лучше, может быть, и нет, но бардака уж точно поменьше!"
    "Хотя это как посмотреть. Что для одного бардак, для другого — творческий беспорядок."
    me "Да нет, живётся нормально, на самом деле. Делаю что душе вздумается!"
    me "Никаких глупых поручений нет."
    me "На линейку можно не ходить."
    me "Отбой не в десять, а во сколько хочу!"
    show mi grin pioneer with dspr
    mi "В десять тридцать?"
    me "Эм…"
    me "Ну а что? Ложиться рано — полезно!"
    me "Япония разве не страна жаворонков?"
    mi "Не-е-ет! Япония — страна… "
    show mi happy pioneer with dspr
    show mi smile pioneer with dspr
    extend "сычей!"
    $ renpy.pause (0.2)
    mi "Когда я жила в Токио, то ложилась в три! А вставала в час, а то и в два. И мои одноклассники тоже! Мы иногда гуляли по вечерам и рассказывали страшные истории."
    mi "Знаешь, как здорово гулять по ночному городу и рассказывать страшные истории?"
    me "А ночной Токио может быть страшным?"
    show mi dontlike pioneer with dspr
    mi "Конечно! Не везде ведь фонари горят!"
    mi "Иногда идёшь себе, идёшь, а за тобой мужик какой-нибудь идёт. И не поймёшь зачем!"
    mi "То ли свиной рамэн у него закончился, то ли гуль какой-нибудь на охоту выходит!"
    me "А что, неплохой сюжет для аниме!"
    show mi surprise pioneer with dspr
    mi "А-а?"
    me "Ничего!"
    me "Думаю вот, третья струна нормально настроена или фальшивит?"
    show mi normal pioneer with dspr
    stop music fadeout 2
    "Мику забирает у меня гитару и щиплет струны, одну за другой."
    $ renpy.pause (0.5)
    mi "Нет, слишком низко звучит. Сам справишься или помочь?"
    me "Да сам, наверное… Никуда вроде не тороплюсь."
    show mi happy pioneer with dspr
    mi "Вот и замечательно!"
    show mi happy pioneer far with dspr
    mi "Я пока что-нибудь сыграю! Ты не против же, да?"
    "— спросила Мику, отойдя к фортепиано."
    hide mi with dspr
    "Против я, разумеется, не был."
    $ renpy.pause (2)
    "Мику достала откуда-то ноты, установила их на пюпитр и начала играть."
    stop ambience fadeout 1
    play music tchaikovsky
    $ volume(1.3, "music")
    $ renpy.pause (7,hard=True)
    "Это было превосходно."
    "Не как гитара, но всё-таки."
    scene cg miku_piano with dissolve2
    $ renpy.pause (0.3)
    "Кто бы мог это написать? Чайковский? Шопен?!"
    "Ференц Лист, портрет которого всегда наполовину закрыт крышкой пианино?"
    $ renpy.pause (1)
    "Да нет, вряд ли. Раньше такой мелодии я точно не слышал."
    "Ну а кто тогда? Кто-то из советских композиторов?"
    "Или сама Мику?"
    "Она играла, закрыв глаза и словно забыв об остальном мире."
    "Так отрешиться и полностью погрузиться в процесс может далеко не каждый."
    "Кто-то слишком нервничает, кто-то боится попасть не по той клавише, а кто-то просто стесняется."
    $ renpy.pause (1)
    "Но не Мику."
    "Для неё это словно была сама жизнь — с таким упоением она играла."
    $ renpy.pause (1)
    th "Так она же даже на ноты не смотрит!"
    me "Мику…"
    "— проговорил я еле слышно. Она не ответила."
    me "Мику!"
    scene bg int_musclub_day with dspr
    stop music fadeout 1
    show mi surprise pioneer far with dspr
    mi "Ой, Семён! Я совсем про тебя забыла!"
    mi "Что-то… задумалась."
    me "Классно исполняешь! А кто автор?"
    mi "Автор?"
    me "Ну мелодии… Которую ты играла."
    mi "Э-э-э… Не знаю. Я сама играла."
    mi "Что в голову приходило, то и играла. А что?"
    me "Не может быть! Мелодия-то вон какая сложная."
    mi "Не веришь? Я серьёзно!"
    mi "Мне даже не приходилось задумываться над нотами или чем-то ещё, будто… моими руками кто-то управлял. Это было немного странно, но страшно интересно!"
    hide mi with dspr
    "Я недоверчиво посмотрел на Мику и подошёл к фортепиано."
    "Взял с пюпитра лист и обнаружил, что он…{w} пуст."
    $ renpy.pause (1)
    "Я покрутил его, посмотрел на обратную сторону, даже попробовал что-то разглядеть на свет."
    "Ничего. Совершенно пустой белый лист, если не считать нотного стана."
    me "Поверить не могу."
    show mi smile pioneer with dspr
    mi "А ты возьми и поверь! Ведь не всё в этом мире можно объяснить."
    "Какая знакомая мысль."
    hide mi with dspr
    $ renpy.pause (1)
    play sound ddlc
    $ volume(0.8, "sound")
    "Я в задумчивости постучал по клавишам фортепиано."
    $ renpy.pause (1)
    "В это время Мику взяла в руки гитару и с удовлетворением отметила, что в этот раз все струны звучат как надо."
    "Я поставил «ноты» обратно и подошёл к ней."
    show mi smile pioneer with dspr
    me "Ну, я, пожалуй, пойду."
    me "Завтра зайду поиграть!"
    show mi happy pioneer with dspr
    mi "Удачи, Семён! Заходи, не забывай меня!"
    me "Не забуду!"
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    $ renpy.pause (0.5)
    play ambience ambience_camp_center_day fadein 2
    scene bg ext_house_of_mt_day with dissolve
    window show
    "Дойдя до своего домика (точнее, нашего с Ольгой Дмитриевной — но она вчера куда-то уехала, и домик пока что принадлежал только мне), я уселся в шезлонг и принялся смотреть вокруг."
    "Казалось удивительным и даже немного странным, что никто каждые пять минут не подходил задать какой-нибудь идиотский вопрос."
    "Все в лагере были осведомлены об отъезде главной вожатой и наверняка надоедали кому-–то другому."
    "И я был этому крайне рад, ибо меня подобные «посетители» всегда жутко отвлекали."
    "Стоит после обеда лечь часок подремать — и всем внезапно становится интересно, будет ли сегодня после ужина какое-то мероприятие, а если не будет, то «КАК ЖЕ ТАК ОЛЬГАДМИТРИННАВЫЖЕОБЕЩАЛИ!!!»."
    "Прежде чем зайти, пионеры (обычно из младших отрядов) громко стучали, заставляя стёкла дрожать, а меня — подпрыгивать на кровати и говорить пару неприличных слов."
    "Но сейчас всё было тихо. В поле зрения вражеских элементов не наблюдалось."
    "Я потянулся, зевнул и решил ещё немного почитать."
    "Открыв книгу, я нашарил взглядом место, где остановился, как вдруг моё внимание привлекла закладка."
    "Кусок бумаги, который я сунул в книгу перед выходом."
    $ renpy.pause (1)
    stop ambience fadeout 2
    me "Хм-м… Интересно…{w} Этого я точно не писал."
    "На обратной стороне узкой полоски бумаги красными чернилами было выведено:"
    play sound paper
    show note_02 with dissolve
    play music music_list["sunny_day"] fadein 2
    $ renpy.pause (5,hard=True)
    "…"
    $ renpy.pause (1)
    hide note_02 with dspr
    "Странно."
    $ renpy.pause (1,hard=True)
    "Я прочитал надпись трижды. {w}Потом ещё раз."
    "Если в этих строках и был какой-то смысл, то он уверенно ускользал от моего понимания."
    $ renpy.pause (1)
    "А был ли он там вообще?"
    stop music fadeout 1
    $ renpy.pause (1)
    "В таком случае спасает лишь одно:{w} дедуктивный метод."
    "Я мысленно надел шляпу с двумя козырьками и принялся размышлять."
    $ renpy.pause (1)
    jump street_deduction

label street_deduction:
    play music think_deeply fadein 2
    $ volume(0.8, "music")
    me "Клочок бумаги явно попал ко мне случайно… {w}Его, должно быть, забыл кто-то сидевший за столом до меня, а я и не заметил, что на обороте что-то написано."
    me "Точка в конце каждой строки. Предложения словно отчеканены, и всё написано как-то… {w} тезисно, что ли?"
    me "Словно это главные мысли в чьём-то монологе."
    show note_02 with dspr
    "Я взглянул на бумажку ещё раз."
    me "Написано красной перьевой ручкой."
    $ renpy.pause (1,hard=True)
    me "Хотя нет, шариковой: линии слишком тонкие."
    me "Неаккуратно, но без разводов и помарок.{w} Судя по всему, писали в спешке."
    $ renpy.pause (0.5,hard=True)
    me "В качестве чернил использована кровь."
    $ renpy.pause (1,hard=True)
    hide note_02 with dspr
    me "Ладно-ладно, не кровь. Страшилок надо меньше читать."
    $ renpy.pause (0.5,hard=True)
    me "А вообще, если бы это действительно была кровь, то чернила были бы менее яркими."
    me "Хотя во многом зависит от того, какую кровь использовать — венозную или артериальную…"
    $ renpy.pause (0.5)
    me "Бр-р, стоп!" with hpunch
    me "Это вообще к делу не относится!"
    $ renpy.pause (0.7)
    show note_02 with dspr
    $ renpy.pause (0.3,hard=True)
    me "«уверенное существование…»"
    "…"
    me "Последнее предложение написано большими буквами. Самое главное?"
    me "И что значит «и он реален»?"
    me "Хм…"
    me "Непонятно."
    stop music fadeout 1
    $ renpy.pause (2)
    voice "Скоро поймёшь…"
    stop music
    hide note_02 with dspr
    play sound sfx_scary_sting
    me "А-а-а-а-а!" with hpunch
    scene bg od_house_close with dspr
    play sound sfx_fall_grass
    play sound_loop sfx_head_heartbeat
    "Блин, а земля-то твёрдая!"
    jump talk_sl_1
label talk_sl_1:
    show sl scared pioneer close with dspr
    sl "Семён?"
    sl "Семён, ты чего? Это же я!"
    "Я хватаю ртом воздух и пытаюсь унять бешено бьющееся сердце."
    me "Г-господи, Славя, зачем же так… {w} подкрадываться?"
    sl "Прости-прости! Я тебя напугала?"
    hide sl with dspr
    scene bg ext_house_of_mt_day with dissolve
    "Я медленно встаю и возвращаюсь на шезлонг."
    show sl scared pioneer with dspr
    sl "Ты с таким рвением смотрел себе под нос и что-то бормотал, что… " 
    show sl laugh pioneer with dspr
    extend "я не смогла удержаться!"
    "Я поднимаю взгляд на Славю и вижу, что она смеётся." 
    stop sound_loop fadeout 1
    play music music_list["my_daily_life"] fadein 3
    play ambience ambience_camp_center_day fadein 3
    $ volume(0.8,'ambience')
    "Поняв всю глупость ситуации, я заставляю себя улыбнуться в ответ."
    "Дышать становится легче."
    me "Весело тебе над пожилыми людьми шутить? Вот будешь в моём возрасте, будет тебе 54 года!.."
    show sl smile pioneer with dspr
    sl "А чего ты так дёргаешься-то?"
    me "Да у меня тут… {w} одно интересное дельце!"
    sl "Какое?"
    me "Вот, взгляни."
    "Я отдаю бумажку и наблюдаю, как её лицо, сначала весёлое, принимает удивлённое выражение."
    show sl surprise pioneer with dspr
    sl "И… Что это значит?"
    sl "Графоманствовать вздумал?"
    me "Графо… что?"
    me "А, нет, это вообще не я написал."
    "Славя удивляется ещё больше."
    me "Можно сказать, эта штука у меня случайно оказалось. Когда из библиотеки уходил, положил в книгу первое, что под руку попалось, чтобы страницу не потерять."
    "Славя некоторое время с сомнением крутит бумажку в руках и спрашивает:"
    show sl normal pioneer with dspr
    sl "Как странно…"
    sl "«cуществование на самом деле», «восход»… {w}А что за «восход»?"
    me "Понятия не имею… А есть идеи?"
    sl "Хм…"
    $ renpy.pause (1)
    show sl smile2 pioneer with dspr
    sl "Может, мотоцикл «Восход»? У моего брата такой."
    me "В самом деле? Здорово!{w} Но вряд ли."
    me "Мотоцикл в такой записке — как-то уж слишком банально…"
    show sl smile pioneer with dspr
    sl "Ну а что тогда?"
    me "Даже не знаю…"
    me "Может, какой-нибудь {i}Sunrise Avenue{/i}?" 
    show sl surprise pioneer with dspr
    th "Ой-ой. Видимо, в СССР об этом пока не знают."
    th "Мир своеобразно напомнил мне о необходимости соблюдения легенды."
    th "М-да, будь я шпионом-нелегалом из «Бюро легенд», прокололся бы уже раз десять, не меньше."
    $ renpy.pause (1,hard=True)
    me "Эм… Что-то у меня программа забарахлила…"
    $ renpy.pause (0.4,hard=True)
    me "И я виню лесной воздух! Это всё он на меня так влияет!"
    me "Вот у нас в славном городе Ниж… Горьком знаешь сколько машин? В Москве столько нет!"
    me "Особенно у площади в час дня, когда все на обед едут. Неудивительно, что там в это время никто не ходит, — дым такой, хоть топором руби."
    show sl normal pioneer with dspr
    sl "Да брось врать-то. Была я в прошлом году в твоём Горьком." 
    me "И как?" 
    th "Уф. Вроде получилось."
    show sl laugh pioneer with dspr
    sl "Как? Деревня деревней! На мою похожа, только чуть больше."
    show sl normal pioneer with dspr
    sl "Но Кремль действительно большой."
    me "А то!"
    me "Хотя мне до сих пор обидно, что оригинал памятника Минину и Пожарскому стоит в Москве."
    me "Правильнее было бы его в Горьком оставить, как думаешь?"
    sl "Ну может быть. Честно говоря, его-то я и не посмотрела…"
    jump talk_sl_2
    
label talk_sl_2:
    $ renpy.pause (1)
    show sl serious pioneer with dspr
    "Вдруг Славя хлопнула себя по лбу, что-то внезапно вспомнив."
    sl "Совсем заболтал ты меня со своими бумажками и памятниками. Ольга Дмитриевна у себя?"
    me "Нет. Ты чего, она же уехала вчера. Я вроде бы всем сообщил."
    show sl surprise pioneer with dspr
    sl "В самом деле? Как же я так… {w}И все уже знают, говоришь?"
    "Славя прислонилась к шезлонгу и стала машинально мять бумажку, которую до сих пор держала в руке."
    me "Эй-эй! Это же вещдок!"
    show sl surprise pioneer with vpunch
    "Славя встрепенулась и непонимающим взглядом уставилась на свою руку."
    sl "А, да, извини…"
    show sl normal pioneer with dspr
    "Она отдала бумажку и спросила:"
    sl "Так что ты об этом думаешь?"
    me "Да ерунда какая-то. Одно из двух: либо кто-то решил пошутить (и у него явно не удалось), либо…"
    show sl smile2 pioneer with dspr
    sl "Либо?.."
    me "Либо у нас в лагере…"
    stop ambience fadeout 2
    stop music fadeout 2
    $ renpy.pause (1)
    "Я выдержал эффектную паузу."
    $ renpy.pause (1)
    $ volume(0.3,'sound')
    play sound sfx_scary_sting
    $ volume(0.3,'sound')
    me "Поехавший!" with hpunch
    $ renpy.pause (0.3)
    show sl surprise pioneer with dspr
    sl "Что?"
    sl "Поехавший?{w} Куда поехавший?"
    me "А… забудь."
    me "Так в одном фильме называли не совсем…{w} здравомыслящих людей."
    show sl smile pioneer with dspr
    play music music_list["my_daily_life"] fadein 3
    play ambience ambience_camp_center_day fadein 3
    sl "Интереснейшие же фильмы вы смотрите, товарищ!"
    sl "И книги странные читаете. Это вот что такое?"
    "Она показала на книгу, которую я всё это время держал в руках."
    me "А, это… Да так, без Ольги Дмитриевны со скуки иностранными языками надумал заниматься."
    me "Товарищ в городе очень уж увлёкся Китаем и его культурой… Меня вот заставил учить!"
    $ renpy.pause (1)
    me "Кстати, а ведь Китай же наш ближайший союзник, наш младший брат, тоже полный несгибаемой воли построить коммунизм!"
    me "Слава коммунизму! Слава марксизму-ленинизму! Слава Коммунистической партии Ки…"
    $ renpy.pause (1,hard=True)
    show sl serious pioneer with dspr 
    $ renpy.pause (0.5)
    sl "Эй, что за улыбки? Всё верно."
    sl "Хоть отношения с Китаем сейчас переживают не самый лучший период, всё ещё впереди."
    sl "Комсомольцы рассказывали, что у них в университете проводят вечерние курсы маоцзэдунидей. Можешь себе такое представить?"
    sl "Я бы послушала. Должно быть, очень интересно."
    th "Да уж. Очень."
    th "Достаточно одного названия —{w} «маоцзэдунидеи».{w} Слабо сразу сказать?"
    show sl normal pioneer with dspr
    sl "Кстати, а как по-китайски «Советский Союз» будет, в этой книге не написано?"
    me "Написано. Да я и сам знаю!{w} Если ручка есть, могу написать."
    "Славя извлекла невесть откуда ручку, и я написал два иероглифа на обороте записки."
    th "Неужели вера в СССР и светлое коммунистическое будущее здесь всё ещё настолько крепка?"
    th "М-да уж."
    th "Узнать бы, какой здесь год…"
    play sound sfx_dinner_horn_processed
    "Я открыл рот, собираясь озвучить вопрос, как прозвучал сигнал на ужин."
    "Ну вот. Придётся приберечь эту тему для следующего разговора."
    show sl smile2 pioneer with dspr
    sl "Нам, вероятно, стоит поторопиться, пока все места не заняли."
    "— произнесла Славя, улыбнувшись."    
    me "Секунду, только книгу занесу."
    window hide
    stop music fadeout 1
    stop ambience fadeout 1
    $ renpy.pause (1)
    jump dinner_01
    
label dinner_01:
    scene bg ext_dining_hall_away_sunset with dissolve2
    $ persistent.sprite_time = 'sunset'
    $ sunset_time()
    $ renpy.pause (1)
    window show
    play music sunset fadein 2
    play ambience ambience_camp_entrance_evening fadein 2
    $ volume(1.1,'ambience')
    "После ужина моё настроение улучшилось, и я с довольным видом уселся на лавочку перед столовой."
    "Возвращаться в комнату и читать не хотелось, ложиться спать было рано.{w} Нужно было найти себе занятие, чтобы скоротать вечер."
    $ renpy.pause (1)
    "Погода стояла замечательная."
    "Я подумал и точно решил, что никуда не пойду.{w} Проводить вечер в помещении в такую погоду — кощунство!"
    "Именно в такие моменты начинаешь сомневаться, какой из миров реальный — тот, что был в прошлом, или этот, где нет никаких забот и ты волен делать всё, что пожелаешь…"
    "Но забываться нельзя. В том мире меня ждут. В том мире у меня есть дела."
    "К этому же стоит относиться как к вынужденному отпуску — и наслаждаться, пока есть время."
    th "Может, за подушкой кого-нибудь отправить?"
    "А что? В летнюю ночь можно и на улице поспать. Вряд ли замерзну, зато потом будет о чём внукам рассказать."
    "Я тут же представил себя дедом 80 лет, гордо повествующим малышне о том, как один раз совершил этот поистине героический подвиг."
    "Да, можно будет сказать, что вокруг летали полчища комаров, скамейка была жесткой, а подушку так никто и не принёс."
    show us normal pioneer with dspr
    "Пока я мечтал и обрисовывал историю деталями, подошла настоящая малышня в лице Ульяны и тут же принялась сверлить меня взглядом."
    jump us_01
    
label us_01:
    me "А, мелкая! Тебе чего?"
    show us dontlike pioneer with dspr
    $ renpy.pause (0.3)
    us "Посмотрите на него! Ольги Дмитриевны нет — вмиг распустился!"
    us "Хорошо без власти жить, да?"
    me "Конечно! Анархия — мать порядка!"
    me "Пой, революция!"
    "Получив в ответ недоуменный взгляд Ульяны, я понял, что необходимо сменить тему."
    me "Э-э-э… {w}Послушай, Ульяна. Ты можешь сделать одно ма-а-а-аленькое одолжение?"
    me "Я тебе…{w} эм…{w} конфет дам."
    show us dontlike pioneer with dspr
    "Памятуя о «мелкой», Ульяна смотрит на меня взглядом, в котором читается неодобрение."
    stop music fadeout 3
    me "Хорошо-хорошо! Много конфет дам! Тебе на год хватит!"
    show us surp1 pioneer with dspr
    "Её лицо внезапно озаряется."
    play music music_list["always_ready"] fadein 2
    us "Хорошо, тогда с тебя… {w}Хм-хм… {w} 5 275 конфет!"
    me "Сколько?!" with hpunch
    us "Сколько слышал!"
    me "А с чего это так много?!"
    us "Ну смотри… В день я ем 10–20 конфет. В среднем — 15. Так?"
    me "Ну так."
    us "Ну и всё! Умножаем 15 на 365 и получаем как раз 5 275!"
    me "Ничего себе! Это ты сейчас всё в уме сбацала?"
    show us laugh2 pioneer with dspr
    "Ульяна кивнула и зажмурилась."
    "Наверняка от осознания морального превосходства над гуманитариями."
    voice "Это-то да, но…{w} С расчётами ты малость напортачила."
    show dv grin pioneer at left with moveinleft
    show us surp2 pioneer with dspr
    "— вставила оказавшаяся рядом Алиса."
    me "А кто это тут у нас подслушивает?"
    dv "Да я только мимо проходила."
    me "Люблю запах…{w} отговорок поутру!"
    show dv smile pioneer at left with dspr
    "В это время Ульяна загибала пальцы."
    us "Да не может быть! Всё же правильно!"
    "Я нахмурил лоб, подвигал бровями и через пару секунд радостно объявил:"
    me "Не, а правда! Не 275, а 475!"
    me "Три в уме-то что, забыла?"
    show dv laugh pioneer at left with dspr
    dv "В точку!"
    me "И как только вас в школе математике учат, сопляки?"
    me "Неужто через пару-тройку лет мы неизбежно встретим закат советской науки?"
    me "А из-за кого? Из-за таких вот!"
    show us angry pioneer with dspr
    "Ульяна посмотрела на меня как на полностью неразумное существо."
    us "Да я тебя, дылда!.."
    us "Да будет тебе известно, я в городе в тройке лидеров по быстрому счёту!{w} А в школе мне вообще равных нет!"
    me "Что, Алис, не врёт она?"
    show dv smile pioneer at left with dspr
    dv "Нет, всё так и есть."
    hide dv with moveoutleft
    "Алиса улыбнулась и отошла, видимо посчитав свою миссию по восстановлению математической справедливости выполненной."
    "Блин, а на самом деле это было здорово."
    "Так быстро посчитать в её возрасте не каждый сможет.{w} Да и не в её возрасте тоже!"
    "И Алиса… Удивительно!"
    "А я мастерски несмешно зашутил.{w} В принципе, как всегда?"
    $ renpy.pause (1)
    stop music fadeout 2
    "Но Ульяна тем временем всерьёз задумала обидеться."
    hide us with dspr
    "Она надула губы и повернулась, чтобы уйти." 
    "Ситуацию срочно надо было спасать!"
    me "Ладно-ладно, постой."
    "Ульяна остановилась."
    me "Ну извини, извини, не думал, что тебя это так заденет."
    me "Больше так не буду шутить."
    me "Честное слово пожилого птеродактиля."
    "Ульяна медленно повернулась, и я увидел… "
    show us laugh pioneer with dspr
    extend "что рыжая бестия хохочет!"
    play music music_list["always_ready"]
    us "А-а-а, поверил! Поверил же, да?"
    us "4:2 в мою пользу!"
    me "И ничего не поверил…"
    me "Эй, и как это четыре? Вчера же поровну было!"
    us "А помнишь, сегодня на завтраке у тебя в тарелке было две тефтельки?" 
    us "Ты ещё ходил жаловался всем!"
    us "«Почему у всех три, а у меня две?»"
    us "Про заговоры что-то кричал!"
    show us grin pioneer with dspr 
    us "Догадайся почему!"
    "Я стал медленно приподниматься со скамейки."
    me "Лучше беги…"
    stop music fadeout 2
    show us surp1 pioneer with dspr
    us "А?.."
    me "Даю пять секунд. Время пошло! Один… два…"
    show us grin pioneer with dspr 
    us "Ой-ой."
    hide us with easeoutright
    "Ульяна с места выдала первую космическую и через клумбу, через кусты принялась удирать в лес."
    $ renpy.pause (1)
    stop ambience fadeout 2
    "Я досчитал до пяти и как… "
    with hpunch
    extend "сел обратно."
    "Нет, ну а что? Бегать за маленькими девочками?"
    "Вы серьёзно?"
    window hide
    stop ambience fadeout 1
    $ renpy.pause (1)
    scene black with dissolve
    jump forest_01
    

label forest_01:
    scene bg ext_dining_hall_away_night with dissolve
    $ persistent.sprite_time = 'night'
    $ night_time()
    $ renpy.pause (1)
    play ambience ambience_camp_center_evening fadein 2
    $ volume(0.5,'ambience')
    window show
    "На лагерь незаметно опустился вечер."
    "Я поднялся со скамейки и решил немного прогуляться на ночь глядя."
    stop ambience fadeout 2
    window hide
    scene black with dissolve
    $ renpy.pause (0.3)
    scene bg ext_path_night with dissolve
    window show
    play music music_list["reminiscences"] fadein 1
    play ambience ambience_camp_center_evening fadein 2
    "За всё время пребывания здесь я только дважды не ходил в лес по вечерам."
    "В первый раз Ольга Дмитриевна попросила помочь перенести в клуб кибернетиков какую-то здоровенную колонку.{w} Во второй — та же Ольга Дмитриевна где-то потеряла панамку, и мы искали её всем лагерем."
    "Панамка, разумеется, оказалась в домике: она лежала на кровати, накрытая одеялом."
    "Увидев её, вожатая сначала чуть меня не расцеловала, а затем чуть не поколотила, решив, что я это всё специально устроил с целью её позлить. {w}М-да."
    "Но ОД нет, так что можно вдоволь нагуляться, не опасаясь, что {i}«для лучшего пионера своего отряда»{/i} даже вечером найдётся какое-то внезапное и, разумеется, очень важное поручение."
    $ renpy.pause (1)
    scene bg ext_path2_night with dissolve
    $ renpy.pause (1)
    "Я бродил по протоптанным тропинкам, пиная шишки и пугая своим присутствием белок."
    "Но, как оказалось, в лесу я был не один: чуть впереди виднелась белая рубашка и знакомые фиолетовые волосы."
    "Ну… Не подкрадываться же, в самом деле."
    me "Лена! Дороу!"
    show un normal pioneer with dspr
    "Лена, тоже наверняка погружённая в свои мысли, вздрогнула и обернулась."
    un "Семён? Добрый вечер."
    me "Прошу прощения, не хотел тебя потревожить."
    un "Да нет, что ты."
    un "Я просто немного задумалась." 
    $ renpy.pause (0.5)
    show un smile pioneer with dspr
    un "Блуждаю в лабиринте грёз…"
    "Лена мечтательно улыбнулась и посмотрела на меня."
    "Я же стоял столбом, изо всех сил стараясь придумать ответ на такую метафору."
    un "А я тебя тут часто замечаю. Тоже есть о чём поразмыслить или просто гуляешь?"
    "Обычно многословный, с Леной я почему-то чувствую себя неудобно. {w}Не то что бы нам не о чем говорить, но…{w} В общем, не знаю."
    me "Гуляю… После города лес кажется чем-то необычным. Словно попадаешь в другой мир — так всё незнакомо и непривычно."
    me "Будь я писателем, то сказал бы, что перед лицом природы всё принимает какой-то иной смысл."
    me "Как-то успокаиваешься, понимаешь, что все твои проблемы и головная боль — это так, суета."
    me "Поэтому и гуляю по вечерам."
    un "А для меня летний лес прекрасен в любое время суток."
    me "В самом деле?"
    un "Да."
    un "Ранним утром он чист и безмолвен." 
    un "В его тишине словно сливаешься с природой — то странное чувство единения с пространством, которое испытываешь только в самых знакомых, родных местах."
    un "Гуляя в его утреннем тумане, невольно задумываешься о том, что остального мира нет — есть лишь ты и этот лес."
    me "А днём?"
    un "Днём же в лесу прохладно, даже если снаружи очень жарко."
    me "А мне всегда казалось, что лес наиболее красив вечером. Когда уже стемнеет, он кажется каким-то… {w} необычным, что ли."
    "За неделю я довольно хорошо изучил тропинки и даже нашёл дорогу к старому лагерю, про который здесь ходит полно легенд."
    "Один туда идти я, правда, не решился."
    "Странное место, от которого наверняка следует держаться подальше."
    un "Интересно…{w} А я и не думала, что ты такой…"
    me "Какой?"
    show un shy pioneer with dspr
    un "Ну такой…{w} Думающий."
    "(сентиментальный)"
    "До сих пор не могу решить, плюс это или минус."
    me "По большей части мы всегда видим лишь «изнанку» человека. {w}То, какой он внутри, порой бывает непонятно даже ему самому."
    me "Когда я учился в школе, был у меня друг. Для всех он был типичный лидер, этакий классный заводила, — прекрасно шутил, спорил с учителями (и практически всегда оказывался прав). Иногда дрался."
    me "Все его уважали и считали «своим в доску парнем»."
    me "Пожалуй, лишь я один знал, что вне общества он совсем другой."
    me "Дома он писал стихи и выращивал суккуленты. Помогал младшей сестре. Кормил бездомных кошек."
    me "Для него самого было загадкой, почему в разное время он вёл себя как два разных человека."
    me "Даже думал к психологу обращаться, представь?"
    me "Хорошо, что я его отговорил."
    show un normal pioneer with dspr
    un "Ты сказал, что друг «был»… {w}А где он сейчас?"
    me "Он… {w}Не знаю.{w} В один день он и его родители внезапно сорвались с места и уехали… {w}Наши пути разошлись."
    un "Жаль…"
    "Было видно, что Лене действительно жаль. Но необходимо сознаться — в истории я рассказал не всю правду."
    window hide
    hide un with dspr
    stop ambience fadeout 2
    $ renpy.pause (1)
    scene bg ext_path2_night with dissolve
    $ set_mode_nvl()
    "Карл Иванович Вебер не был моим одноклассником — он был на несколько лет старше."
    "Увидев его впервые, я сразу почувствовал, что от него исходит какая-то странная сила. Кто-то называет это волей, кто-то — харизмой."
    "Он обладал уникальным даром объединять людей: работая в компании над каким-то проектом, все чувствовали приобщённость к общему делу, независимо от своей роли — большой или малой."
    "Мы проводили с ним вместе много времени. {w}Играли в морской бой, шахматы, советовали друг другу книги.{w} А когда появились компьютеры — играли в Civilization III по локальной сети."
    "Он научил меня многим важным и интересным вещам, рассказал основы психологии общения с людьми, которые вычитал у каких-то немецких философов.{w} Показал семь разных способов завязывания галстука!"
    nvl clear
    "Он был не по годам умён, и это видели все."
    "В один прекрасный день он взялся учить…{w} китайский язык.{w} Никто не верил, что он сможет хотя бы как-то понять его основы самостоятельно, — ведь даже с учителями это получается далеко не у всех."
    "Но он никого не слушал.{w} Он проводил вечера, обложившись учебниками."
    "Сначала он изучил язык на уровне обывателя — самые простые выражения и историю Китая. Со своими вопросами о Троецарствии достал в городской библиотеке всех и каждого!"
    "Затем стал читать стихи и поэмы. От {i}«Сна в красном тереме»{/i} до {i}«Переполоха в Небесном царстве»{/i}.{w} Понравившиеся выражения выписывал в отдельную тетрадь, а потом заучивал наизусть."
    "Потом он откуда-то достал «Общественно-политический перевод».{w} Кстати, та самая книга, которая сейчас лежит в моей комнате."
    th "Удивительно, что она есть в здешней библиотеке! Всегда думал, что она вышла гораздо позже…"
    nvl clear
    "Через несколько лет Карл с блеском сдал вступительные экзамены и поступил в Дипломатическую академию в Москве."
    "Он всегда казался мне не таким, как все. {w} Я восхищался им. {w} И не только я — его талант отмечали и преподаватели."
    "Сейчас он работает в одном государственном учреждении России в КНР."
    "А я…"
    "Я работаю вместе с ним. Он мой руководитель и бессменный начальник."
    "При всех — Карл Иванович. Наедине — Карл."
    $ renpy.pause (1.5,hard=True)
    scene black with dissolve2
    $ renpy.pause (1)
    stop music fadeout 2
    $ set_mode_adv()
    scene bg ext_backdoor_night_7dl with dissolve
    show un smile pioneer with dspr
    window show
    $ renpy.pause (1)
    "В молчании мы дошли до задних ворот лагеря."

    play ambience ambience_camp_center_evening fadein 2
    me "Лена, спасибо тебе."
    show un surprise pioneer with dspr
    un "За что?"
    me "За хороший вечер…{w} И за интересный разговор."
    un "Но мы ведь даже и не говорили толком."
    me "Эта встреча навеяла старые воспоминания, и это явно стоит благодарности."
    show un smile with dspr
    un "Тогда пожалуйста. Я тоже много о чём… {w}успела вспомнить."
    un "Завтра увидимся." 
    me "Доброй ночи!"
    hide un with dissolve
    "И мы пошли — каждый своей дорогой."
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    $ renpy.pause (1)
    scene black with dissolve
    jump home_ext_01
    
label home_ext_01:
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    scene bg ext_house_of_mt_night_without_light with dspr
    "Подойдя к домику, я потянулся в карман за ключом."
    "Помимо ключа там лежало три леденца, расчёска и…{w} злосчастная записка."
    "Надо взглянуть на неё повнимательнее."
    scene bg int_house_of_mt_noitem_night with dissolve
    play sound sfx_close_door_1
    $ renpy.pause (2)
    play sound paper
    show note_02 at cright with dspr
    $ renpy.pause (2.2,hard=True)
    hide note_02 with dspr
    me "Да нет.{w} Не может быть, ерунда это всё."
    me "Просто кто-то пошутил, причём довольно глупо, а я повёлся… {w}Завтра же об этом забуду."
    play sound paper_bucket
    "Я смял бумажку и бросил её в мусорное ведро."
    $ renpy.pause (2)
    show bg int_house_of_mt_night2 with dissolve
    "В комнате было прохладно, так что я побыстрее забрался под одеяло.{w} Поёжился." 
    th "Нет, ну и чертовщина же творится в последнее время!"
    th "То засыпаешь и просыпаешься непонятно где, то какие-то бумажки с непонятными текстами!" 
    th "Этот мир явно дал сбой. Завозите новый!"
    window hide
    $ renpy.pause (1)
    scene black with dissolve2
    stop ambience fadeout 1
    $ renpy.pause (3,hard=True)
    jump day_two_night

label day_two_night:
    $ prolog_time()
    play music music_list["sunny_day"] fadein 1
    scene bg darkroom with dissolve2
    window show
    $ renpy.pause (2,hard=True)
    "Комната, залитая красным светом."
    "Силуэт человека впереди."
    "Я не помню, как здесь оказалась.{w} Был день — и сразу ночь…"
    "Я потеряла счёт времени. Не могу сказать точно, сколько я тут нахожусь — два дня или неделю."
    "Не могу пошевелиться.{w} Совершенно не чувствую тела."
    iam "Ты…{w} Ты чудовище!"
    iam "Зачем тебе всё это?"
    "Человек остался неподвижным."
    voice_unknown "Потерпи. Скоро ты всё поймёшь."
    voice_unknown "Как говорится, ждать осталось недолго."
    "Едва слышный смешок."
    iam "Отпусти меня, живо!"
    voice_unknown "Не кричи.{w} Не стоит."
    voice_unknown "Иначе…"
    "Человек взял со стола шприц. Достал из кармана ампулу."
    voice_unknown "Ты этого хочешь?"
    iam "Да иди ты!.."
    "Человек усмехнулся."
    voice_unknown "Да, всё-таки придётся.{w} Тем более скоро рассвет."
    show blink
    "Я закрыла глаза."
    $ renpy.pause (1,hard=True)
    "Я знала, что будет дальше."
    stop music fadeout 2
    scene black
    window hide
    $ renpy.pause (1,hard=True)
    jump mist_day_two

    
label mist_day_two:
    $ day_time()
    $ persistent.sprite_time = "day"
    $ renpy.pause (2,hard=True)
    scene bg int_house_of_mt_day with dissolve
    play ambience ambience_int_cabin_day fadein 1
    $ renpy.pause (1)
    window show
    $ renpy.pause (0.5)
    "Я открыл глаза и сделал глубокий вдох."
    "Проснулся, разумеется, всё там же — в этом иллюзорном мире, который очень старается быть похожим на реальный." 
    "Хотя…"
    play music curious_critters fadein 3
    $ renpy.pause (2)
    "Булочки здесь вкусные и вполне реальные.{w} Окружающая действительность тоже не так уж и плоха."
    "Пора бы уже привыкнуть."
    $ renpy.pause (0.5)
    "Прежде чем встать, я бросил взгляд на кровать Ольги Дмитриевны."
    "Пуста."
    "На часах семь двадцать. Ольга Дмитриевна обычно будит меня в семь и ещё полчаса даёт наставления, прежде чем идти будить остальных."
    th "Хоть бы её подольше не было…"
    scene cg semen_reserved with dspr
    "Я надел рубашку и принялся вертеться перед зеркалом, пытаясь идеально завязать галстук."
    "Идеально — это чтобы он был ровно по центру рубашки, оба его конца были одной длины и он не шевелился во время ходьбы."
    "Задача непростая, но джентльменский этикет предполагает, что субъект всегда должен выглядеть опрятно. В своём мире я неукоснительно следовал этому правилу. Не собирался нарушать его и здесь."
    $ renpy.pause (2)
    scene cg semen_smile with dspr
    "Через пять минут галстук был повязан идеально."
    scene bg int_house_of_mt_day with dissolve
    "Я довольно улыбнулся и потянулся за ботинками."
    stop music fadeout 2
    "А…{w} А где они?"
    me "Странно."
    "Кроссовки, как обычно, стояли около кровати, но оба ботинка по какой-то неизвестной причине находились не на своём месте около порога, а лежали едва ли не у шкафа отсутствующей ОД."
    "Не знаю, как это сразу не бросилось мне в глаза."
    "Точно помню, что вчера вечером снимал их не там… Хм."
    $ renpy.pause (1,hard=True)
    me "Видимо, ночью кто-то заходил в домик, а я спал так крепко, что даже не заметил."
    $ renpy.pause (0.5,hard=True)
    "Неприятно. {w}Наводит на определённые мысли."
    "Первая — что с этого дня придётся запирать дверь на замок. {w}Сегодня только ботинки подвинули, а завтра что? {w} От этого мира можно ожидать чего угодно."
    "Вторая — вполне вероятно, что приходила Ольга Дмитриевна."
    "Но это крайне маловероятно.{w} Она бы обязательно меня разбудила."
    me "Но ведь её же нет в лагере!"
    "К тому же зачем ей трогать мои ботинки?!"
    "Вопросов становилось всё больше."
    "Я вздохнул и вышел из домика."
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    $ renpy.pause (1)
    jump mist_day_two_ext
    
label mist_day_two_ext:
    play sound sfx_close_door_1
    show bg ext_house_of_mt_day with dissolve
    window show
    "В «Совёнке» почему-то всегда стоит ясная погода."
    "Облака появляются крайне редко, а грозовых туч за восемь дней я не видел ни разу."
    "Словно сама вселенная бережёт обитателей лагеря от плохой погоды и плохого настроения."
    play music gangster fadein 2
    "Закрывая дверь, я краем глаза увидел какое-то шевеление около шезлонга."
    "Поиграть в шпионов? С удовольствием!"
    "Я аккуратно закрыл дверь. {w}Плавными движением развернулся и положил ключ в карман. {w} Взмахнув руками, легко сделал шаг и…"
    me "А-а! Попавсь!" with hpunch
    "…едва не опрокинул шезлонг. {w}Но ни в нём, ни позади него никого не оказалось."
    "Досадно. {w}Ну ладно, надеюсь, свидетелей не было."
    "С разочарованием я поправил собственность лагеря (или Ольги Дмитриевны — велосипед-то её!) и намеревался было уйти, как внезапно моё внимание привлек какой-то клочок бумаги, лежащий на шезлонге."
    "Это что, сигаретная пачка?"
    $ renpy.pause (0.5)
    "Нет.{w} Судя по всему, обычная бумажка."
    me "Господа. Это никуда не годится."
    me "Мало того, что сидят на {i}частной собственности{/i}, так ещё и мусорят!"
    "Чертыхаясь, я двумя пальцами взял клочок и заозирался в поисках ближайшей клумбы. {w} Период распада бумаги — год, так что ничего не случится (наверное){w} (но это не точно)."
    "Спустя три секунды искомый объект был найден, и я с радостью понёсся к нему. {w} Встав перед строем из цветов, я вытянул руку вперёд, уставился на бумажку и строгим голосом произнёс:"
    me "Неустановленная бумажка! За все свои грехи и грехи твоего хозяина ты приговариваешься к… {w} сожжению в печи."
    me "Но печи у нас нет.{w} И усы у меня не такие."
    me "В таком случае наказание заменяется на прогулку по доске."
    me "Но доски у нас тоже нет, есть только шланг.{w} Это я."
    me "Короче говоря, по случаю отсутствия иных инструментов исполнения наказания приговор будет приведён в действие обычным способом, а именно — сбрасыванием виновного с высоты… {w} в метр и восемьдесят три сантиметра."
    stop music fadeout 3
    me "Приговор окончательный и обжалованию…{w} Это ещё что?"
    $ renpy.pause (0.5)
    "Неся всю эту заумную ерунду, я бросил взгляд на бумажку и…"
    play music music_list["orchid"] fadein 2
    "…обнаружил там знакомые красные чернила."
    me "Как…{w} Ещё одна?!"
    "Стоило мне забыть про вчерашний инцидент, как он снова дал о себе знать."
    "Да ещё на том месте, куда, скорее всего, посмотрю только я."
    "Мне стало не по себе. Прямо очень не по себе."
    "Дрожащими руками я развернул записку."
    play sound paper
    show note_02_2 at cright with dspr
    "«уверенное существование… существование на самом деле».{w} Но ведь это всё было!"
    hide note_02_2 with dspr
    "Я перевернул записку и увидел:"
    play sound paper
    show note_03 with dspr
    $ renpy.pause (4,hard=True)
    stop music fadeout 1
    $ renpy.pause (0.5)
    hide note_03 with dspr
    $ renpy.pause (0.5)
    "Бумажка полетела на землю."
    "Опять?"
    play music what_you_can fadein 2
    "Я обхватил голову руками и принялся лихорадочно думать."
    me "Та-а-ак…"
    me "Видимо, кто-то знает, что я не отсюда… {w}не из этого мира…"
    me "И хочет меня напугать."
    me "Чтобы я суетился.{w} Бегал.{w} Подозревал всех подряд."
    $ renpy.pause (1)
    "Ну уж нет…{w} Этого они не дождутся." 
    "«Бегал и суетился», говорите…"
    me "Да ни в жизнь!"
    play sound sfx_pat_shoulder_hard
    "Я похлопал себя по щекам, стараясь успокоиться."
    th "Думай, думай!" with hpunch
    $ renpy.pause (0.5)
    "Эту бумажку выбрасывать явно не стоит. {w} На досуге её стоит изучить поподробнее: вдруг удастся разузнать что-то важное, что поможет выйти на след злоумышленника (или шутника)."
    "Ведь привидений и призраков не бывает. {w} Получается, это всё сделал человек."
    "Кто-то из нашего лагеря. {w} Кто-то, кого я, возможно, знаю лично."
    "Я поднял бумажку и сунул было в карман, как моё внимание привлёк какой-то синий значок."
    $ renpy.pause (0.5)
    play sound paper
    show note_03_3 at cright with dspr
    me "«Советский Союз»… {w} Я же его вчера для Слави рисовал!" 
    me "Так это…"
    hide note_03_3 at cright with dspr
    "В мгновение осознав, я бросился обратно к домику."
    $ renpy.pause (0.5)
    play sound sfx_open_door_2
    show bg int_house_of_mt_day with dspr
    "Кое-как нашарив в кармане ключ, я открыл дверь и кинулся к мусорному ведру."
    "По всей комнате разлетелись старые чайные пакетики, салфетки, яблочные огрызки и даже два кожаных ремешка, невесть откуда взявшиеся."
    "Среди всего этого мусора я так и не нашёл того клочка бумаги, который выбросил вчера перед сном."
    $ renpy.pause (0.5)
    "Вот тут мне стало действительно страшно."
    th "Как же крепко нужно спать, чтобы не заметить, как обшаривают мусорное ведро?!"
    "Причём этот злоумышленник (пусть будет господин Х) наверняка осмотрел не только ведро, но и другие вещи. Ведь его главной целью было найти этот клочок бумаги, а где он лежал — неизвестно."
    "Я мельком проверил тумбочку и шкаф. Вещи казались нетронутыми."
    "Либо господин Х не оставил следов, либо осмотрел мусорную корзину первой и сразу же преуспел. {w}Тем не менее, остаётся резонный вопрос…"
    me "Зачем было двигать ботинки?!" with hpunch
    play ambience ambience_int_cabin_day fadein 1
    stop music fadeout 2
    $ renpy.pause (2,hard=True)
    "Я принялся думать. {w}Думал."
    "Думал."
    "Думал."
    "…"
    $ renpy.pause (1,hard=True)
    "Логике этот вопрос не поддавался."
    $ renpy.pause (1)
    "Вздохнув, я всё-таки направился умыться, надеясь, что по пути подобных сюрпризов не предвидится."
    window hide
    stop ambience fadeout 1
    jump day_two_bath

label day_two_bath:
    
    play ambience ambience_camp_center_day fadein 1
    show bg ext_washstand_day with dissolve
    window show
    "Вокруг никого. {w} Неудивительно."
    "Ещё нет восьми часов, а значит, всё мирно спят.{w} Только Ольга Дмитриевна имела обыкновение будить всех с утра пораньше, а пока её нет, остальные вожатые позволяют пионером (и себе) поспать чуть подольше."
    play sound_loop sfx_water_sink_stream fadein 1
    th "Вот что значит руководить лагерем железной рукой!"
    $ renpy.pause (2)
    stop sound_loop fadeout 1
    play sound sfx_close_water_sink 
    "Умывшись как всегда холодной водой, я направился в домик занести принадлежности и подумать, как скоротать время до завтрака."  
    $ renpy.pause (1)
    stop ambience fadeout 1
    window hide
    
    show bg int_house_of_mt_day with dissolve
    window show
    play sound sfx_close_door_1
    play ambience ambience_int_cabin_day fadein 1
    "Что ж. Самое время поразмыслить над новой-старой запиской."
    "Я сел на кровать, извлёк из кармана бумажку и поднёс её к глазам."
    play sound paper
    show note_03_3 at cright with dspr
    "Да, мои старые очки сейчас бы не повредили. Но до них как до Китая…"
    "Хотя почему же «как»?"
    $ renpy.pause (1,hard=True)
    "Ладно, не важно."
    "Придётся обходиться так."
    $ renpy.pause (1.5,hard=True)
    play music tricky_code fadein 2
    "Бумага как бумага, ничего необычного."
    "Белая. {w} Хорошего качества."
    "Новый текст написан той же ручкой, что и предыдущий.{w} Так же неаккуратно."
    "Скорее всего, писал один и тот же человек. {w}Наверняка мужчина, но нельзя сказать наверняка."
    "Почему?{w} Во-первых, не у всех девушек красивый почерк."
    "Во-вторых, потренировавшись с полчаса, можно до неузнаваемости изменить свой стиль и одно-два предложения написать совершенно по-другому."
    "В общем, тупик."
    $ renpy.pause (1)
    me "Так, а она пахнет вообще?"
    hide note_03_3 with dspr
    $ renpy.pause (1)
    play sound sniff
    $ volume(1.4,'sound')
    $ renpy.pause (1.3)
    "Ничего."
    me "Жаль, как жаль! Эх, почему я не парфюмер… {w} Хотя, как известно, он плохо кончил, так что всё в порядке."
    "Поняв, что больше ничего нового из бумажки выведать не получится, я аккуратно сложил её и положил в карман."
    "Теперь лучше всегда носить её с собой, чтобы не появилось каких-то новых надписей."
    $ renpy.pause (1)
    me "Буду относиться к этому как к шутке. {w} К неудачной такой шутке."
    me "На завтраке надо быть повнимательнее — вдруг кто-то проявит себя пристальным вниманием."
    me "Тут-то мы голубчика и накроем."
    "План утверждён. Выдвигаемся!"
    window hide
    $ renpy.pause (2,hard=True)
    scene black with dissolve
    stop music fadeout 2
    stop ambience fadeout 1
    jump day_two_canting

label day_two_canting:
    show bg ext_dining_hall_near_day with dissolve
    play ambience ambience_camp_center_day fadein 1
    show sl normal pioneer with dspr
    window show
    $ renpy.pause (1.5,hard=True)
    "По скромным подсчётам, в столовую я должен был прийти раньше остальных.{w} Но скромные подсчёты в каком-то месте дали сбой — на скамейке уже сидела Славя и с задумчивым видом смотрела перед собой."
    "Странно."
    show sl smile2 pioneer with dspr
    sl "О, Семён, доброе утро. Ты чего это так рано?"
    me "Привет! Привык просыпаться в одно и то же время под будильник Ольги Дмитриевны, теперь отвыкнуть вот не могу."
    me "Оказалось, что сбивать режим дня почти так же сложно, как восстанавливать."
    me "А ты почему уже здесь?"
    sl "А я всегда рано встаю. В комнате делать особо нечего, поэтому время от времени хожу сюда — вдруг на кухне нужно чем-то помочь."
    sl "Хотя почему-то за всё время помощь им так ни разу и не понадобилась…"
    me "Хм, странно, и почему…"
    show sl serious pioneer with dspr
    $ renpy.pause (1)
    "Улыбаться явно не стоило."
    show sl normal pioneer with dspr
    sl "Как у тебя дела? Разобрался с запиской?"
    me "Да ты знаешь…"
    th "Я бы на её месте уже давно забыл."
    "Да и забыл ведь на самом деле!"
    "И не вспомнил бы, если бы не инцидент сегодня утром."
    me "Да не бери в голову, ерунда это всё. Мелким нечем заняться, вот и бесятся."
    me "Честно говоря, не удивлюсь, если узнаю, что это всё Ульяна затеяла, чтобы меня позлить."
    "Славя покачала головой."
    show sl serious pioneer with dspr
    sl "Не думаю, что это Ульяна. Помнишь, что там было написано?"
    sl "Что-то про «восход», «уверенное существование»…"
    sl "Что там ещё…."
    "Славя задумалась."
    $ renpy.pause (1)
    show sl normal pioneer with dspr
    sl "Слушай, а она у тебя с собой?"
    sl "Дай ещё раз взглянуть, пожалуйста."
    "Я потянулся к карману, как вдруг вспомнил о строках, появившихся ночью.{w} Рука повисла в воздухе."
    me "А-а-а…" 
    me "Знаешь, её у меня нет. Я её это… {w}Выбросил!"
    me "К чему всякий мусор с собой таскать?"
    show sl serious pioneer with dspr
    "Славя с сомнением посмотрела на меня."
    sl "Выбросил, говоришь?{w} Ну ладно."
    hide sl with dspr
    "Она поднялась и пошла прочь от столовой."
    me "Странно… Куда это она?"
    window hide
    $ renpy.pause (1,hard=True)
    scene black with dissolve
    jump day_two_canting_2

label day_two_canting_2:
    show bg ext_dining_hall_near_day with dissolve
    play sound sfx_dinner_horn_processed
    $ renpy.pause (1,hard=True)
    "Через пару минут откуда ни возьмись налетела целая стая голодных пионеров, которые, толкаясь и ругаясь нехорошими словами, принялись протискиваться в двери столовой."
    th "Порядок? Какой порядок?"
    stop ambience fadeout 1
    hide bg ext_dining_hall_near_day with dissolve
    show bg int_dining_hall_people_day with dissolve
    play ambience ambience_dining_hall_full fadein 1 
    $ renpy.pause (1)
    "Завтракал (равно как обедал и ужинал) я обычно в одиночестве."
    "Не потому что никого не знал или не хотел общаться, а потому что ко мне попросту никто не садился. {w}Ну не знаю, это как в автобусе, когда рядом с тобой есть свободное место, но люди предпочитают стоять."
    "Совиная аура?.."
    $ renpy.pause (1.5)
    "Тем не менее, в этот раз отшельником мне себя почувствовать не довелось. Все столы были заняты."
    "С подносом в руках я рыскал глазами во всех направлениях, стараясь найти кого-нибудь из своего отряда."
    show sl normal pioneer far at center with dspr
    show un normal pioneer far at left with dspr
    "Ага. Вон Лена и Славя. Сидят за четырёхместным столиком."
    th "И когда только Славя успела зайти?"
    $ renpy.pause (1)
    "Я нахмурил брови и принялся обдумывать, реально ли незаметно унести поднос в комнату, как вдруг Славя поднялась, посмотрела на меня и громко сказала:"
    show sl smile2 pioneer far at center with dspr
    sl "Семён! Место ищешь? Садись сюда, здесь не занято!"
    "А Лена-то не против, интересно?"
    "Изобразив на лице радость, я медленно побрёл к их столу."
    show sl smile2 pioneer at center with dspr
    show un normal pioneer at left with dspr
    me "Доброе утро, девочки!"
    me "Спасибо, Славя, я-то уж решил, что позавтракать сегодня не доведётся. Надеюсь, я вам не помешаю."
    sl "Да что ты. «Пионер всегда и при любых обстоятельствах должен помогать товарищу», помнишь?"
    "Я не помнил, но на всякий случай покивал."
    me "Лена, ты как? Что нового?"
    show un smile pioneer at left with dspr
    un "Хорошо. Сегодня письмо от родителей должно прийти."
    un "Интересно узнать, что там дома происходит."
    "Ух ты, а сюда даже письма доставляют! Здорово."
    un "А у тебя как дела, Семён?"
    me "Хорошо вроде… Ничего нового со вчерашнего дня!"
    un "Вот и хорошо."
    un "Приятного аппетита."
    "— пожелала Лена и принялась за еду. Я последовал её примеру."
    "…"
    $ renpy.pause (1)
    stop ambience fadeout 1
    scene black with dspr
    hide un with dspr
    $ renpy.pause (1)
    show bg int_dining_hall_people_day with dissolve
    play ambience ambience_dining_hall_full fadein 1
    show sl normal pioneer at center with dspr
    "Через пару минут Лена расправилась со своей порцией и покинула наше общество."
    "Я ж ел не спеша, скрытно и внимательно оглядывая пионеров вокруг."
    "Но тщетно — на меня никто не смотрел и не косился."
    "Не подглядывал из-за огромной вазы с фикусом."
    "Не щёлкал тайком на советский «Горизонт»."
    "А жаль."
    $ renpy.pause (1)
    "Прикончив свою порцию, я элегантнейшим образом вытер губы салфеткой и стал было приподниматься, как вдруг меня осторожно взяла за рукав Славя."
    sl "Семён… Ты ведь пионер?"
    "Я опешил."
    me "Э-э-э… Конечно!"
    me "Что за вопрос?"
    sl "И ты мне товарищ?"
    me "Разумеется! А что такое-то?"
    show sl serious pioneer at center with dspr
    sl "У настоящего пионера не должно быть секретов от своих товарищей.{w} Особенно если эти секреты способны повлечь за собой вред окружающим."
    me "Господи, да что за секреты-то? Славя, не тяни резину, говори уже!"
    sl "Семён, я же знаю, что записка при тебе.{w} И даже знаю где — в правом кармане брюк."
    sl "Не умеешь ты врать."
    sl "Дай мне взглянуть на неё, пожалуйста. Почему-то я чувствую, что с ней не всё так просто."
    "Славя протянула руку. Посмотрела на меня серьёзным, прямым взглядом. Прямо в глаза."
    "Я поёжился и, покраснев, извлёк из кармана на свет божий записку."
    sl "Спасибо."
    "Я стал внимательно наблюдать за её реакцией."
    "Она развернула записку…{w} Зрачки её глаз расширились. Бумажка упала на стол."
    show sl surprise pioneer at center with dspr
    sl "Что это… Этого ведь вчера не было!"
    hide sl with dspr
    "Я отвёл глаза и дал ей время всё прочитать и осмыслить."
    "Мне бы не хотелось втягивать Славю во всё это. Всё-таки записка предназначалась в первую очередь мне."
    "Мы ведь до сих пор не знаем, что таит в себе этот клочок бумаги — шутку или реальную угрозу."
    show sl serious pioneer at center with dspr
    "Когда я вновь украдкой посмотрел на Славю, то увидел, что её взгляд направлен прямо на меня."
    "Мне стало не по себе.{w} В очередной раз за этот день, который едва начался."
    sl "Семён, так не пойдёт. Обязательно нужно рассказать остальным."
    me "Да будто есть о чём рассказывать. Ну «восход», ну и что? Обычный приём, чтобы заставить человека поволноваться."
    me "Это же банальщина — написать без задней мысли кучу непонятных слов и заставить кого-то другого ломать голову над их «глубинным смыслом»."
    sl "Нет, здесь что-то другое… Но я не могу понять что!"
    sl "На банальщину это не похоже, скорее на какое-то…{w} послание?{w} Предупреждение?"
    $ renpy.pause (1)
    "Славя ещё раз взглянула на бумажку."
    sl "Смотри, тут написано «восхода ещё можно избежать». Так его можно остановить?"
    me "Не знаю… А есть какие-то идеи?"
    sl "Нет…"
    sl "Кстати, а как эти строки вообще появились?"
    "Я рассказал ей о том, как обнаружил смятую записку утром в шезлонге."
    show sl scared pioneer at center with dspr
    "Услышав, что в комнату ночью кто-то заходил, Славя испугалась."
    sl "Обязательно закрывай дверь, слышишь! Не хватало ещё, чтобы с тобой что-нибудь случилось!"
    me "Да что со мной случится…"
    show sl angry pioneer at center with dspr
    me "Хорошо-хорошо! Буду закрываться!"
    show sl normal pioneer at center with dspr
    me "Да не переживай ты так, всё будет в порядке. Мы ж всё-таки в Советском Союзе живём, а не в Америке какой-нибудь."
    me "У нас люди с когтями не ходят и девушек не похищают!"
    sl "А что, в Америке похищают?"
    "Я кратенько пересказал Славе сюжет «Кошмара на улице Вязов», случайно спутав несколько сюжетных линий и, в общем, проспойлерив фильм, который в этом мире ещё даже не вышел."
    "Ну, хотя бы обстановку разрядил."
    me "Ну вот, тем всё и кончилось. Все умерли.{w} Ну, почти все."
    show sl serious pioneer at center with dspr
    sl "Да не может быть. Таких сумасшедших людей не существует."
    "Ох, как же ты ошибаешься, Славя. Существуют, да ещё и не такие."
    "Расскажи в Советском Союзе, что творится в мире в 21-м веке, — не поверят. Покрутят пальцем у виска и назовут дураком."
    "Ходит дурачок по лесу…"
    $ renpy.pause (1)
    me "Ну ладно, я пойду. Спасибо ещё раз за компанию!"
    sl "Не за что… Ещё раз. Будь осторожен."
    sl "И если будут новые записки — непременно сообщай!"
    me "Ну… Хорошо. У нас вроде как с тобой союз теперь. Островок здравого смысла в море сумасшествия."
    me "Маяк мудрости в океане невежества!"
    show sl smile2 pioneer at center with dspr
    sl "Горазд на сравнения. Тебе бы литератором быть, а ты за китайский сел свой…"
    "И это она помнит!"
    "Не удивлюсь, если и в шахматы может без доски играть."
    hide sl with dissolve
    "Встав из-за стола и кивнув Славе на прощание, я вышел."
    $ renpy.pause (1)
    scene black with dissolve
    window hide 
    jump day_two_canting_3

label day_two_canting_3:
    show bg ext_dining_hall_near_day with dissolve
    play ambience ambience_camp_center_day fadein 1
    window show
    $ renpy.pause (1)
    me "Ай!" with hpunch
    play music gangster fadein 1
    "Едва выйдя из столовой, я получил крепкого тумака."
    "Побелев от злости, я заозирался вокруг и принялся водить руками влево и вправо, словно в поисках перчатки для объявления немедленной дуэли."
    "Не сходя с места! С шести шагов!"
    show us laugh pioneer at left with dspr
    "Но перчатка не понадобилась: рядом стояла и хохотала Ульяна, а с девочками, как известно, дуэлей не бывает."
    show us laugh pioneer at center with dspr
    me "В чём дело, мелкая? Жить надоело?"
    show us grin pioneer at center with dspr
    us "А о чем это вы со Славей разговаривали, а? Небось, в любви признавался? Или внимания заискивал, чтобы от работы отлынивать?"
    me "А хоть бы и так! Тебе-то что?"
    show us angry pioneer with dspr
    "Ульяна надулась."
    us "Ну нет."
    us "Так неинтересно."
    us "Ты должен был начать оправдываться, краснеть или что-то придумывать."
    us "Давай ещё раз, а?"
    me "Ну давай."
    stop music fadeout 1
    hide us with dspr
    stop ambience fadeout 1
    $ renpy.pause (2,hard=True)
    window hide
    scene black with dissolve
    show bg ext_dining_hall_near_day with dspr
    play ambience ambience_camp_center_day fadein 1
    window show
    $ renpy.pause (2,hard=True)
    me "Ай!" with hpunch
    me "Эй, ну это можно было и не повторять!"
    play music music_list["i_want_to_play"] fadein 1
    show us laugh pioneer at center with dissolve
    $ renpy.pause (1)
    me "Весело ей…"
    me "А тебе чего надо-то, собственно?"
    show us shy pioneer with dspr
    us "Ну ладно, ладно."
    us "Теперь серьёзно."
    us "Ты мне нужен…"
    hide us with dspr
    show us shy pioneer with dspr
    "Ульяна посмотрела по сторонам и убедилась, что нас никто не подслушивает."
    us "Ты мне нужен для одного дела.{w} Очень секретного."
    us "И очень чёрного."
    me "Очень?"
    us "Очень."
    me "Как уголь?"
    show us angry pioneer with dspr
    us "Нет, блин, как дельфин!"
    us "Так ты в деле?"
    me "Смотря что за дело. Принимать участие в сомнительных акциях мне начальство не разрешает."
    show us laugh pioneer with dspr
    us "Славя, что ли?"
    "Я улыбнулся. Карл Иванович бы очень удивился."
    "А что, если косы приделать — будет один в один Славя!"
    with hpunch
    us "Алло, приём!"
    me "Да-да."
    me "И хватит меня тыкать!"
    me "Так что за дело?"
    hide us with dissolve
    window hide
    $ renpy.pause (1)
    $ set_mode_nvl()
    "И Ульяна поведала мне свою историю."
    "Сегодня утром Алиса её очень обидела. Прямо смертельно."
    "Как именно?"
    "Не разбудила на завтрак. Из-за этого Ульяна проспала и утреннюю порцию питательных веществ не получила."
    nvl clear
    "На вопрос «Зачем же ты так поступила? Ты была мне как… сестра!» обладательница самых рыжих волос в лагере заявила, что будила её на протяжении получаса, но цели своей не достигла."
    "Ульяна посчитала эти слова «глупой отговоркой» и придумала план."
    "Праведная месть должна свершиться."
    "Но одной ей было не справиться, так что пришлось искать помощника — в меру не в себе (чтобы помог), в меру умного (чтобы понял, что именно нужно делать) и в меру благородного (чтобы не проболтался)."
    nvl clear
    "Сэр Симон по всем параметрам подходил идеально (особенно по первому)."
    $ renpy.pause (0.5)
    $ set_mode_adv()
    window show
    $ renpy.pause (0.5)
    "Услышав такую характеристику, я принялся было возникать, но потом вспомнил про «благородного» и довольно выпрямился."
    th "Ах, мелкая, знает, чем меня взять!"
    show us normal pioneer with dspr
    me "Ну хорошо, я согласен. Только у меня два вопроса:"
    me "а) Что нужно делать?"
    me "б) Какое вознаграждение?"
    us "О вознаграждении не переживай, будешь доволен."
    us "А что делать — это совсем просто."
    us "Твоя задача — раздобыть ведро, наполнить его водой и помочь мне его поднять."
    us "На этом твоя роль завершится, да и моя, в принципе, тоже."
    "Облить Алису водой? Глупо, банально, но…{w} Интересно!"
    "И безболезненно."
    th "Вот она обрадуется!"
    me "По рукам."
    "Ульяна плюнула себе на руку и протянула её мне."
    me "Эй, ну не в прямом смысле же!"
    me "И да, где мне ведро достать?"
    show us angry pioneer with dspr
    us "М-да, такой ты и есть… Ведро в сарае на складе. Если что, спроси там кого-нибудь."
    me "Хорошо. Тогда через пятнадцать минут у вашего домика."
    show us laugh pioneer with dspr
    us "Не опаздывай!"
    "…"
    window hide
    stop music fadeout 2
    stop ambience fadeout 1
    $ renpy.pause (1)
    scene black with dissolve
    $ renpy.pause (1)
    jump day_two_joke
    
label day_two_joke:
    scene bg ext_warehouse_day_7dl
    window show
    "Достать ведро оказалось несложной задачей — склад действительно был открыт."
    "Завхоз, правда, поинтересовался, зачем оно мне вдруг понадобилось, — пришлось соврать, что возникла срочная необходимость полить цветы у домика."
    "Вроде как Ольги Дмитриевны нет и никто их не поливает…"
    "В общем, ведро мне дали."
    "…"
    $ renpy.pause (1)
    scene bg ext_boathouse_day with dissolve
    "За водой пришлось сходить к реке. Тоже без приключений."
    "…"
    $ renpy.pause (1)
    scene bg ext_house_of_dv_day with dissolve
    play ambience ambience_camp_center_day fadein 1
    "В общем, уже через десять минут мы с ведром были в кустах в назначенном месте. Мелкой, правда, ещё не было."
    "…"
    $ renpy.pause (2)
    "Не появилась она и ещё через десять минут."
    "Я начал терять терпение."
    "Под палящим солнцем вода нагрелась и уже не была холодной."
    "Ох и повезёт же Алисе! В такой жаркий день подобный «душ» явно её обрадует."
    "Наверное."
    "Меня бы обрадовал."
    $ renpy.pause (1)
    show us normal sport far at right with dissolve
    "Наконец-то появилась Ульяна."
    play music music_list["eat_some_trouble"] fadein 2
    $ volume(0.85, "music")
    "На плече она тащила огромный моток толстой верёвки. Для инженерной конструкции?"
    "Она подошла к домику и оглянулась. Я зашипел из кустов, привлекая её внимание."
    show us smile sport far at right with dspr
    "Она повернула голову, кивнула и прогулочным шагом направилась в мою сторону."
    show us laugh2 sport at center with dspr
    "Поравнявшись со мной, Ульяна сделала резкий прыжок и в мгновение ока оказалась в кустах, едва не перевернув ведро."
    with hpunch
    me "Тише ты! Конспирация на то и конспирация, что с ней лучше не перебарщивать!"
    me "Чего так долго?"
    show us smile sport at center with dspr
    us "Да верёвку искала, а она только у кибернетиков! Заставили про свою кошкодевочку слушать."
    us "Шурик этот!.."
    us "«Какие у неё красивые ушки»!"
    us "Еле ноги унесла!"
    me "А что, кошкодевочки — это сила."
    me "Даёшь биоинженерию! Каждому рабочему — по личной кошкодевочке!"
    show us angry sport at center with dspr
    "Ульяна посмотрела на меня с сомнением и резюмировала, что «в меру не в себе» — это точно про меня."
    me "Ну так что? Как ведро вешать будем?"
    show us normal sport at center with dspr
    us "Я всё продумала. Значит, так…"
    hide us with dspr
    "Ульяна в общих чертах объяснила принцип работы этого «механизма»."
    $ renpy.pause (1,hard=True)
    $ set_mode_nvl()
    "По крыше домика пускалась верёвка — один её конец привязывался к ведру, а другой — к железной трубе за домиком."
    "Затем ведро поднималось на требуемую высоту. Учитывая, что тащить его на веревке придётся через всю длину домика, становится интересно, почему Ульяна не выбрала кого-нибудь помощнее."
    "Открываясь, дверь толкает ведро, и то, наклоняясь, выливает всё содержимое на голову выходящего."
    "Потом ведро нужно будет снять."
    "Всё просто!"
    "И гениально."
    $ renpy.pause (1,hard=True)
    $ set_mode_adv()
    $ renpy.pause (1,hard=True)
    show us normal sport with dspr
    me "А тебе не кажется, что тянуть ведро по такой пологой крыше вряд ли удастся? Веревка будет скатываться вниз."
    us "Не, на крыше довольно прочная черепица."
    us "Если закинешь верёвку как надо, она даже шевелиться не будет."
    me "А верёвки-то хватит?"
    us "Да, тут метров двадцать, не меньше."
    us "Ну как тебе план вообще?"
    me "Круто. Если всё получится, будешь Чудо-инженером."
    show us laugh sport with dspr
    us "Йе-ей!"
    "Ульяна довольно улыбнулась и сняла с плеча верёвку."
    us "Ну что, выдвигаемся?"
    me "Пошли."
    hide us with dspr
    "Мы тихо-тихо начали пробираться в сторону домика, то и дело поглядывая на окна."
    "Алиса ходила по комнате, отчаянно жестикулировала и что-то говорила, словно рассказывая кому-то очень интересную историю."
    "А мне-то казалось, что только я в этом месте настолько странный, что разговариваю сам с собой. Видимо, нас в этом мире чуть больше."
    "Встав перед дверью, Ульяна привязала один конец верёвки к ведру, а второй дала мне."
    "С десятого раза мне удалось перекинуть его как надо."
    "Успех!"
    $ renpy.pause (1)
    "За домом действительно оказалась старая железная труба. Я привязал к ней конец верёвки и стал осторожно поднимать ведро."
    $ renpy.pause (1)
    with vpunch
    th "А-а-а! Тяжело!"
    "Я тяну до тех пор, пока не слышу свист Ульяны."
    $ renpy.pause (1)
    "Кое-как дотянувшись до трубы, я обматываю веревку вокруг неё, делаю ещё один узел и возвращаюсь на крыльцо."
    scene bg house_dv_bucket with dspr
    $ renpy.pause (1,hard=True)
    show us smile sport with dspr
    $ renpy.pause (0.5,hard=True)
    us "Ну что, по позициям?"
    hide us with dspr
    "Мы кидаемся к кустам, забыв о всякой конспирации."
    scene cg us_sneakpeak_7dl with dspr
    "Ульяна откуда-то извлекает маленький театральный бинокль и, явно довольная собой, смотрит в направлении домика."
    me "Зачем тебе бинокль? Тут и десяти метров-то нет."
    us "Тише, салага. Чем лучше обзор, тем меньше шансов пропустить всё представление."
    "Салага?.."
    "Я собираюсь в понятных выражениях растолковать, кто здесь салага: откашливаюсь, открываю рот и…"
    me "Ульяна, смотри! Павлин летит!"
    us "Где?!"
    "Пользуясь моментом, я выхватываю бинокль у неё из рук."
    me "Ой. Улетел уже."
    me "Ну что поделать, увидишь ещё!"
    scene bg house_dv_bucket with dspr
    show us angry sport with dspr
    us "Ах ты, контра…"
    us "А ну, отдай обратно!"
    "Тем временем в домике наметилось какое-то движение."
    me "Внимание!"
    me "Операция «Рыжеещастье» подходит к концу!"
    show us laugh sport with dspr
    "Ульяна в тот же момент забыла о бинокле и захихикала."
    "Я затаил дыхание и приковал взгляд к двери."
    me "Три…"
    me "Два…"
    stop music fadeout 1
    $ renpy.pause (1)
    show us surp2 sport with dspr
    "Вдруг Ульяна подаётся вперёд."
    us "…Эй! Это же…"
    "Я резко приставляю к глазам бинокль. Ульяна тем временем бросается к домику."
    hide us with dspr
    us "Стой! Не открывай!"
    "Но уже слишком поздно. Дверь открывается."
    stop music fadeout 1
    "Ведро переворачивается, окатывая водой ничего не подозревающую…{w} Лену."
    play sound water_bucket
    $ renpy.pause (1,hard=True)
    "Я пулей выскакиваю из кустов и подбегаю к ним."
    $ renpy.pause (0.5)
    show un shocked pioneer with dspr
    show us surp2 sport at left with dspr
    me "Лена, это недора…"
    us "Мы хотели облить Алису, честно-честно! Кто ж знал, что ты будешь в домике! Мы правда не хотели!"
    "Ульяна сбивчиво извинялась, поняв, какой неприятный оборот приняла шутка — казалось, идеально спланированная и подготовленная."
    "Сейчас она наверняка могла бы посоревноваться с Мику по количеству произносимых слов в минуту…"
    "Я же стоял потупившись, не зная, что сказать."
    "Ульяна трясла за рукав Лену, которая словно застыла. Вода стекала по её волосам и рукавам, и рядом уже образовалась небольшая лужица."
    un "Не… подходи…"
    "— выдохнула Лена, внезапно переведя взгляд на меня."
    me "А?.."
    hide un
    show un_rage_wet_1
    with dspr
    un "Не подходи ко мне! Видеть тебя не могу!"
    hide un_rage_wet_1 with dspr
    "— закричала она и бросилась бежать, спрятав лицо в ладонях."
    "Я метнулся было за ней, но меня кто-то крепко схватил за руку."
    "Слишком сильно для Ульяны, но в самый раз для…"
    play music gangster fadein 1
    show dv grin pioneer2 with dspr
    dv "Что, удалась шутка?"
    "— насмешливо проговорила Алиса. Мы с Ульяной недоумённо посмотрели на неё."
    dv "Думаете, я вас не заметила?"
    dv "Тебя, дылда с ведром, с того конца лагеря можно было увидеть, не то что из окна."
    dv "Если уж пошли на дело, то старайтесь не так сильно светиться, в конце концов."
    dv "Какой нормальный человек будет под окном с верёвкой слоняться?"
    show dv laugh pioneer2 with dspr
    dv "Эх вы, разведчики."
    show us sad sport with dspr
    "Ульяна насупилась, а я уткнул взгляд в землю."
    "Оказывается, план был обречён с самого начала. С самого начала!"
    us "Так если ты нас видела, почему ничего не сделала?"
    dv "Стало интересно, действительно ли вы обольете бедную Леночку."
    dv "За инженерные старания плюс — это вы заслужили."
    dv "Семён, ты, небось, придумал?"
    show us angry sport with dspr
    us "И ничего не Семён! Это всё я!"
    us "Ещё в школе хотела что-нибудь подобное провернуть, но всё повода не было."
    show us sad sport with dspr
    us "До сегодняшнего дня…"
    "М-да. Испытание прошло успешно."
    us "Удивительно, что Лена нас не заметила. Она ведь тоже в комнате была…"
    show dv grin pioneer2 with dspr
    "Алиса хитро улыбнулась, и я понял причину её громкого монолога и активных жестикуляций."
    dv "Ну ладно, горе-шпионы, идите уже. И ведро своё не забудьте!"
    hide dv with dspr
    stop music fadeout 1
    "И с довольным видом удалилась обратно в домик."
    show us sad sport at center with dspr
    "Ульяна проводила её взглядом и посмотрела на меня."
    us "Перед Леной надо извиниться. Это раз."
    us "Как отомстить Алисе, я ещё придумаю. Это два."
    us "Ведро я отнесу сама. Это три."
    me "И четыре — где моя награда?"
    me "Хоть месть и не удалась, но уговор есть уговор!"
    show us angry sport with dspr
    "Ульяна посмотрела на меня долгим взглядом, вздохнула и сунула руку в карман.{w} Быстро извлекла какой-то небольшой тонкий предмет, который немедленно вручила мне."
    show us sad sport with dspr
    me "Что… это?"
    us "А не видишь? Ручка."
    me "А на кой она мне? У меня что, своих ручек нет?"
    us "Не знаю. Пригодится. Честно говоря, я её у твоего домика нашла."
    us "Подумала, что ты потерял и ходишь такой грустный, а я тебе верну, и ты — оп! — и снова радостный!"
    "— сказала Ульяна с кислым видом."
    "Да уж, сейчас только ходить и радоваться."
    me "Хм… Ну спасибо. Может, и пригодится."
    "— сказал я и сунул ручку в карман."
    me "Ну ладно, за ужином увидимся."
    show us normal sport with dspr
    "Ульяна кивнула."
    me "Ещё перед Леной извиняться. Ох уж эта…{w} невыносимая легкость бытия!"
    window hide
    $ renpy.pause (1)
    hide us with dissolve
    stop ambience fadeout 1
    scene black with dissolve
    $ renpy.pause (1)

    scene bg int_house_of_mt_day with dissolve2
    window show
    $ renpy.pause (1)
    play music otl_lolor fadein 1
    "Время обеда."
    "Встречаться с Леной так скоро мне не хотелось, так что я принял решение истинного стоика — пропустить приём пищи и придумать за это время монолог, в котором удастся уместить сразу три вещи:"
    "а) искреннее раскаяние;"
    "б) что-то весёлое (чтобы поднять Лене настроение);"
    "в) цитатку какого-нибудь великого человека (чтобы смотрелось круто)."
    "В задумчивости я уселся на кровать и принялся крутить в руках ручку Ульяны."
    "Около домика нашла? Хм…"
    "Стоит рассмотреть её поближе."
    show red_pen with dspr
    "Обычная ручка."
    "Тонкий стержень."
    "Мне такие по душе."
    "Я взял со стола листок и написал иероглиф «вселенная»."
    hide red_pen with dspr
    show red_pen_2 with dspr
    "Посмотрел с разных сторон."
    th "Хм, красная."
    $ renpy.pause (1)
    "Но вроде неплохо."
    $ renpy.pause (1)
    hide red_pen_2 with dspr
    "Усевшись на кровать, я принялся размышлять о том, как бы начать свою извинительную речь."
    me "Так… Наверное, лучше начать с чего-то отстранённого. Хм…"
    me "Как вообще в подобных обстоятельствах разговор начинают?"
    me "«Привет, Лена, я хочу извиниться»?"
    $ renpy.pause (1,hard=True)
    me "Нет, это как-то просто."
    "Я откинул покрывало, поставил подушку поудобнее и прилёг. Принялся думать, стуча ручкой по лбу."
    $ renpy.pause (1)
    me "А может, о погоде заговорить?"
    me "«Как сегодня жарко, самое то для… "
    $ renpy.pause (0.5)
    play sound badumtssch
    extend "{i}ХОЛОДНОГО ДУША!{/i}»"
    $ renpy.pause (1,hard=True)
    "…"
    $ renpy.pause (1)
    "Лежать было неудобно. Одеяло сложилось складками и упиралось прямо в лопатки."
    $ renpy.pause (1)
    stop music fadeout 3
    "Поёрзав, я вздохнул и понял, что лучше не станет."
    "Поднялся, положил ручку на стол и обратил взор на кровать."
    "Однако вместо складок или чего бы то ни было увидел небольшой лист, сложенный вчетверо."
    $ renpy.pause (2,hard=True)
    play music music_list["torture"] fadein 1
    window hide
    play sound paper
    show note_04 with dspr
    $ renpy.pause (3,hard=True)
    window show
    "«Why is my verse so barren of new pride,"
    "So far from variation or quick change?"
    "Why with the time do I not glance aside"
    "To new-found methods, and to compounds strange?"
    "16; 34 + 1; 10; 4; 1»"
    "…"
    $ renpy.pause (1)
    stop music fadeout 2
    hide note_04 with dspr
    $ renpy.pause (2,hard=True)
    "Самое удивительное, что я не испугался."
    "Да, это новая записка."
    "Да, она обнаружилась у меня в кровати."
    "Да, она снова полна загадок."
    "Но мне уже было не столько страшно, сколько интересно."
    "Страх словно отступил, дав место боевому задору."
    "Тем более здесь по крайней мере не какая-то несусветная чушь, а нечто вроде… {w} шифра?"
    "Кто-то задумал сыграть со мной в игру?"
    "Ну что ж…"
    me "Будь по-вашему!"
    play music tricky_code fadein 2
    play sound paper
    show note_04_4 with dspr
    "Стих был мне известен, на первом курсе нас его даже учить заставляли… На русском, правда. {w} Но при чём здесь Шекспир?"
    "И почему некоторые слова подчёркнуты?"
    "Why… {w} change? {w} Why… {w}do I not glance aside… {w}to new-found methods?"
    "«Зачем… меняться? {w}Зачем… я не смотрю… на новые методы?»"
    $ renpy.pause (1)
    "Превосходно."
    $ renpy.pause (1)
    hide note_04_4 with dspr
    me "Поздравляю, сэр, вы пробили дно."
    "А что, если…"
    play sound paper
    show note_04_4 with dspr
    "«Стоит ли меняться? К чему изобретать новые методы?»"
    "Хм."
    $ renpy.pause (1)
    "Смотрелось уже лучше, но смысла так и не прояснило. {w}Может быть, цифры снизу помогут?"
    me "«16; 34 + 1; 10; 4; 1». Наверняка у них есть что-то общее со словами."
    "М-м-м…"
    me "16-е слово, 34-е слово и так далее?"
    "change…{w} А 34-го слова нет. strange — 33-е и последнее."
    "Так что версия со словами, скорее всего, отпадает."
    $ renpy.pause (1)
    "А если начать отсчёт заново?"
    "change…{w} why…{w} why…{w} so…{w} verse…{w} why."
    $ renpy.pause (1)
    "Нет, в этом нет смысла. Даже если поменять слова местами."
    "…"
    "…"
    stop music fadeout 2
    $ renpy.pause (0.5)
    "Ничего не могу придумать."
    $ renpy.pause (1)
    "Я достал первую записку."
    play sound paper
    show note_05_5 with dspr
    "Почерк вроде тот же самый, хотя нельзя сказать точно."
    $ renpy.pause (1)
    "Но в одном я уверен на все сто процентов:"
    hide note_04_4 with dspr
    hide note_05_5 with dspr
    me "Ручка одна и та же. Красная."
    $ renpy.pause (1,hard=True)
    "Мне ведь тоже недавно довелось обзавестись красной ручкой. И я почти на сто процентов уверен, что…"
    $ renpy.pause (0.5)
    "Я подошёл к столу и взял листок, на котором был написан иероглиф «вселенная».{w} Поднёс к глазам оба листка."
    play sound paper
    show note_06_6 with dspr
    "Сомнений не осталось. {w} Слова на обеих записках точно были написаны {i}одной и той же ручкой{/i}."
    "{i}Той самой, что сейчас лежала у меня на столе.{/i}"
    hide note_06_6 with dspr
    $ renpy.pause (0.5)
    stop music fadeout 2
    play ambience ambience_int_cabin_day fadein 2
    "Я судорожно вытер пот со лба."
    "Да уж, «не испугался»!"
    me "Ульяна ведь нашла её около моего домика, верно?"
    me "Утром я весьма глупо прыгал у шезлонга и наверняка заметил бы её, так что появилась она точно {i}ПОСЛЕ{/i} завтрака."
    me "Ну или до завтрака, но после того, как я ушёл. {w} Странно…"
    $ renpy.pause (1.5)
    me "Итак, что мы имеем."
    me "В домике обнаруживается новая записка, которая ни черта не проясняет смысла происходящего, а лишь всё запутывает. {w}Красная ручка, которая, судя по всему, принадлежит господину Х."
    me "Господин Х дожидается моего ухода, преспокойно заходит, подбрасывает записку и линяет, на выходе теряя ручку."
    "Выглядит складно, но что-то всё равно не даёт мне успокоиться."
    me "Но как он в домик-то вошёл?{w} Дверь ведь была заперта, это я точно помню!"
    "Ключи есть только у меня и у Ольги Дмитриевны."
    "Ольги Дмитриевны нет, ключ она наверняка никому не давала."
    "Мой ключ всегда находился при мне."
    "Кстати говоря, у ОД есть ключи от всех помещений лагеря.{w} Также они есть у Виолы, завхоза и Слави."
    me "Ну, Виоле-то от меня вряд ли что-то нужно. Не такой она человек, чтобы оставлять какие-то странные записки."
    "Я потянул носом воздух."
    me "Да и завхоза здесь точно не было. Везде, где бывает Петрович, остаётся сильный запах перегара. Склад насквозь пропах!"
    "Остаётся Славя."
    $ renpy.pause (0.5)
    "Славя…"
    "Нет, эта версия тоже отпадает. {w}Наличие ключа не может считаться доказательством."
    "Хотя…"
    $ renpy.pause (0.5)
    "Интерес к запискам. {w}Загадочное исчезновение прямо перед завтраком.{w} Свободное место за столом (учитывая, что народу было пруд пруди)."
    "Наконец, просьба впредь делиться всеми мыслями относительно {i}этих странных дел{/i}."
    "К тому же у Слави в деревне есть обширная библиотека и чуть ли не круглосуточный доступ к ней, из чего следует, что в теории знать английский она может."
    $ renpy.pause (2,hard=True)
    "Ну нет…"
    "Я с силой потёр виски."
    "Если это действительно была Славя, то единственный шанс подбросить записку был в пятиминутном промежутке между её «отступлением» и горном для завтрака.{w} Она должна была действовать очень быстро, чтобы успеть ко времени и ещё раз поговорить со мной."
    "Неудивительно, что ручка оказалась на земле, — закрывая дверь, она, вероятно, очень торопилась."
    "Ульяна нашла ручку после начала завтрака, что, в принципе, подтверждает эту теорию."
    $ renpy.pause (0.5)
    "Я вновь опустился на кровать и обхватил голову руками."
    "Это всё Славя? И ночью тоже была она?"
    $ renpy.pause (1,hard=True)
    me "Сколько тайн."
    "От размышлений голова шла кругом. Необходимо было построить план дальнейших действий."
    "Со Славей обязательно нужно будет поговорить. За ужином."
    me "Решено. Я покажу ей ручку и записку и посмотрю, как она отреагирует."
    me "Поддельное удивление легко вычислить, так что очень скоро я всё узнаю."
    "Додумав эту мысль до конца, я немного успокоился."
    "Больше ничего сделать было нельзя, так что я засунул ручку в карман и отправился прогуляться."
    "…"
    window hide
    stop ambience fadeout 1
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg ext_square_day with dissolve
    window show
    $ renpy.pause (1,hard=True)
    "Придя на площадь, я уселся на скамейку и заставил себя думать о чём-то приятном."
    th "Имбирное печенье…"
    th "Пицца с ананасами…"
    th "Полароид как у Максин из Аркадии Бэй…"
    "Внезапно меня кто-то окликнул."
    show el smile pioneer with dspr
    el "А! Семён! Ты-то мне и нужен."
    me "A?.."
    "Не успел я опомниться, как меня схватили за руку и куда-то потащили."
    hide el with dspr
    $ renpy.pause (0.5)
    scene bg ext_clubs_day with dissolve
    show el smile pioneer with dspr
    me "А в чём дело-то?"
    me "Меня в чём-то обвиняют?"
    el "Да не бойся ты."
    el "Сейчас войдём внутрь, и всё расскажу."
    "Блин!"
    "Будто приключений с Ульяной было мало."
    hide el with dspr
    $ renpy.pause (0.5)
    scene bg int_clubs_male_day with dissolve
    "Пропустив меня вперёд, Электроник вошёл следом и запер дверь на ключ."
    show el normal pioneer with dspr
    el "Ну всё. Тут не услышат."
    el "Короче, в чём дело."
    el "Мы тут с Шуриком недавно обнаружили, что эта куча железок…"
    "Он показал на старый магнитофон, стоявший на столе."
    el "…куча железок может ловить зарубежные радиостанции."
    me "Да как может-то? Тут же расстояние даже до Финляндии…"
    show el angry pioneer with dspr
    el "Тихо, не перебивай!"
    el "В инструкции написано, что порог приёма антенны до 2 тысяч километров. Мы сможем поймать европейскую станцию, если настроим магнитофон по принципу вертикальной поляризации, закрепив магнитный штырь с одной стороны, а с другой поставим изолятор."
    el "Но ещё надо закрепить согласующую…"
    me "Ладно-ладно, понял!"
    me "Давай дальше."
    show el normal pioneer with dspr
    el "Так вот. Ловить может, но только надо, как я уже сказал, добавить некоторые детали и перепаять несколько схем."
    el "Изолятор и магнитный штырь мы с Шуриком раздобыли."
    el "Начали делать, как Сашка внезапно включил заднюю."
    show el angry pioneer with dspr
    el "«Слушать зарубежное непатриотично!»"
    el "«Холодная война, железный занавес!»"
    show el normal pioneer with dspr
    el "И ушёл на этом."
    el "Согласующую коробку и рефлектор я достал из старого телевизора."
    el "Но чтобы завершить работу, мне нужен помощник."
    show el smile pioneer with dspr
    el "Семён, ты же прогрессивный человек? Парень из новой эпохи?"
    th "Ещё из какой новой."
    el "Не отступишься перед идеологическим барьером?"
    $ renpy.pause (1)
    me "И всё?"
    show el upset pioneer with dspr
    el "Что «и всё?»"
    me "Ты меня только ради этого сюда с самой площади тащил?"
    me "Провода тебе подержать?"
    me "Тьфу ты!" with hpunch
    me "Я уж думал, что-то серьёзное."
    hide el with dspr
    "Я развернулся, чтобы уйти, но услышал жалобный голос Электроника."
    show el upset pioneer with dspr
    el "Ну Семён, ну чего тебе стоит…"
    show el smile pioneer with dspr
    el "Тем более ключ-то всё равно у меня…"
    "Чёрт! Надо было сразу убегать!"
    $ renpy.pause (1)
    "Ну ладно, не гоняться же за ним по всему клубу."
    me "А это всё много времени займёт?"
    el "Нет, минут пятнадцать.{w} Полчаса максимум."
    $ renpy.pause (1)
    me "Эх…"
    me "Ну давай, показывай свою {i}электронику{/i}."
    window hide
    hide el with dspr
    $ renpy.pause (1)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg int_clubs_male_day with dspr
    window show
    $ renpy.pause (1)
    "Уже через пять минут я убедился, что внутренности советского магнитофона — поистине непроходимый лес."
    "Все микросхемы и платы выглядели одинаково. По меньшей мере дюжина проводов была спутана в узел размером с мой кулак."
    "Хотя, казалось, Электроник во всём этом неплохо разбирался."
    "Поначалу он подробно объяснял, какую деталь куда крепить, и одобрительно кивал, когда я делал как надо."
    "Но потом я случайно капнул ему смолой на палец…"
    $ renpy.pause (1)
    "В общем, он отобрал у меня паяльник и велел держать провода под определённым углом, пока он будет их крепить."
    $ renpy.pause (1)
    "…"
    scene black with dspr
    scene bg int_clubs_male_day with dspr
    "На исходе 70-й минуты работ Электроник выпрямился и объявил, что «в принципе, наверное, всё»."
    "Я издал вздох облегчения и с удовольствием размял затекшие пальцы."
    me "Слушай, Эл, а почему это нельзя было в открытую сделать? КГБ приедет и в ГУЛАГ увезёт?"
    show el upset pioneer with dspr
    el "КГБ?"
    show el normal pioneer with dspr
    el "Нет, никто не приедет."
    el "Это не то что бы незаконно…"
    el "Скажем так: порицаемо непросвещёнными народными массами."
    el "«Зачем слушать иностранщину, когда у нас есть своё, советское?»"
    me "А ты борец с режимом?"
    show el upset pioneer with dspr
    el "Я? Нет!"
    show el normal pioneer with dspr
    el "Но послушать-то хочется. Хотя бы для того, чтобы было с чем сравнивать."
    me "Здравая мысль."
    me "Давай слушать уже."
    hide el with dspr
    "Электроник воткнул вилку в сеть и повернул выключатель."
    $ renpy.pause (1)
    play music radio_on fadein 1
    $ renpy.pause (1)
    me "Работает!"
    show el serious pioneer with dspr
    el "Погоди, надо ещё поймать что-нибудь."
    stop music fadeout 1
    play music radio_tuning fadein 1
    el "Ну давай же, работай, ящик…"
    "Электроник вовсю крутил небольшое колесико, стараясь поймать какую-нибудь станцию."
    $ renpy.pause (2,hard=True)
    stop music 
    play music erika fadein 1
    $ renpy.pause (6,hard=True)
    show el shocked pioneer with dspr
    el "Это что ещё такое?.."
    $ renpy.pause (4,hard=True)
    me "Выключай! Выключай скорее!"
    me "Ща чекисты набегут!"
    hide el with dspr
    stop music
    show el upset pioneer with dspr
    el "М-да… Не на такую «европейскую музыку» я рассчитывал…"
    me "Первый блин комом?"
    el "Да уж."
    el "Ну, давай ещё что-нибудь поищем."
    hide el with dspr
    $ renpy.pause (1,hard=True)
    play music radio_tuning fadein 1
    $ renpy.pause (3,hard=True)
    stop music
    play music roulette fadein 1
    $ renpy.pause (4,hard=True)
    show el normal pioneer with dspr
    $renpy.pause (4,hard=True)
    show el smile pioneer with dspr
    el "Ну, это ещё куда ни шло."
    me "А по-моему, довольно неплохо."
    el "Судя по всему, мы сейчас где-то в зоне ФРГ. Вся музыка немецкая."
    me "Ещё покрути, может, ещё что-нибудь поймаем."
    me "Хотя результат уже есть."
    hide el with dspr
    "Электроник кивнул."
    stop music fadeout 1
    $ renpy.pause (2,hard=True)
    play music radio_tuning fadein 1
    $ renpy.pause (4,hard=True)
    stop music
    play music texas fadein 1
    $ renpy.pause (3,hard=True)
    me "О, а вот это уже дело."
    show el smile pioneer with dspr
    el "Что-то американское?"
    me "Да, скорее всего."
    $ renpy.pause (1,hard=True)
    el "Смотри, умеют же люди музыку сочинять!"
    el "Надеюсь, и к нам такое скоро придёт."
    me "Вместе с револьверами, сигарами и Диким Западом?"
    show el grin pioneer with dspr
    el "А почему бы и нет?"
    me "Тогда будем грабить корованы. Ты и я."
    hide el with dspr
    "Электроник расхохотался."
    "…"
    $ renpy.pause (2,hard=True)
    "Мы ещё немного послушали веселой музыки Североамериканских Соединённых Штатов и отправились дальше."
    stop music fadeout 2
    play music radio_tuning fadein 1
    $ renpy.pause (3,hard=True)
    stop music
    play music tsoy fadein 1
    $ renpy.pause (4,hard=True)
    "Услышав знакомый мотив, Электроник расплылся в улыбке."
    show el smile pioneer with dspr
    el "Во-о-от!"
    el "Всё-таки нет ничего лучше старого доброго Цоя."
    el "Семён, подтверди."
    me "Подтверждаю.{w} Цой жив."
    show el grin pioneer with dspr
    el "Рад, что мы с тобой сошлись во мнениях!"
    $ renpy.pause (2,hard=True)
    "…"
    stop music fadeout 1
    window hide
    scene black with dissolve
    $ renpy.pause (1)
    $ persistent.sprite_time = 'sunset'
    $ sunset_time()
    scene bg int_clubs_male_sunset with dissolve
    $ renpy.pause (1)
    play music zuruck_zu_dir fadein 2
    window show
    $ renpy.pause (2)
    "Я кинул взгляд на часы и осознал, что провёл в клубе чуть больше времени, чем планировал."
    me "Серёжа, я, пожалуй, пойду. Дела не ждут."
    show el smile pioneer with dspr
    el "Конечно, брат, иди."
    el "Спасибо большое, что помог с магнитофоном! Смотри, сколько веселья!"
    "«Спотифай» — начало."
    me "Да не за что, брат. Ты на ужин не опоздай."
    show el grin pioneer with dspr
    el "Не опоздаю, не волнуйся."
    show el smile pioneer with dspr
    $ renpy.pause (1)
    "Я подошёл к двери и вопросительно посмотрел на Электроника."
    show el upset pioneer with dspr
    "Электроник вопросительно посмотрел на меня."
    $ renpy.pause (2)
    show el surprise pioneer with dspr
    el "А, точно! Тебя же выпустить надо!"
    hide el with dspr
    "Электроник вскочил, едва не уронив магнитофон, и открыл дверь."
    "Я похлопал его по плечу и вышел."
    stop music fadeout 2
    window hide
    scene black with dissolve
    $ renpy.pause (1)
    
    scene bg ext_clubs_sunset with dissolve
    play ambience ambience_camp_center_evening fadein 2
    window show
    $ renpy.pause (1)
    "Солнце уже садилось."
    "Немного подумав, я решил направить свои стопы прямиком в столовую."
    $ renpy.pause (0.5)
    scene bg ext_houses_sunset with dissolve
    "Я шёл, смотря по сторонам и раздумывая, чем займусь после ужина."
    th "Может, снова в лес пойти?"
    $ renpy.pause (0.5)
    "За спиной у меня раздался какой-то шорох. Я резко обернулся."
    show un normal pioneer far with dissolve
    play music music_list["dance_of_fireflies"] fadein 2
    "Хм.{w} Лена."
    "И долго она уже меня так преследует, интересно?"
    $ renpy.pause (1)
    "Чёрт! Я забыл придумать, что скажу ей!"
    "Придётся импровизировать."
    show un normal pioneer with dspr
    me "Эм… Привет, Лена. Ты как?"
    show un smile pioneer with dspr
    un "Добрый вечер, Семён. Всё хорошо, а ты как?"
    me "Тоже неплохо… {w}Слушай, я вот хотел извиниться…"
    show un surprise pioneer with dspr
    un "Извиниться? За что?"
    me "Ну как же…{w} Сегодня мы с Ульяной этсамое…{w} неудачно пошутили… {w} и тебя это…{w} Облили, в общем."
    un "Облили? {w}Когда?"
    un "Я ничего такого не помню."
    me "Ну как же! Неужели не помнишь? Это три часа назад было!"
    un "Извини, конечно, Семён, но ты, вероятно, меня с кем-то путаешь. Сегодня после завтрака я пошла в библиотеку и читала Драйзера."
    show un smile pioneer with dspr
    un "«Финансист». Интересная книга, кстати. Ты не читал?"
    me "Нет, но…"
    "Задержав взгляд на лице Лены, я понял, что она либо действительно ничего не помнит, либо скрывает это с мастерством профессионального актёра."
    $ renpy.pause (1)
    "Чертовщина какая-то. В дополнение к запискам."
    me "Ладно. Может, действительно с кем-то перепутал.{w} А может, и с ума сошёл."
    me "С кем не бывает, в конце концов!"
    show un serious pioneer with dspr
    "Лена остановилась."
    un "Слушай, Семён, ты какой-то красный. У тебя, часом, температуры нет?"
    me "Нет, я комму…"
    "Она взяла меня за руку."
    with hpunch
    "От её легкого прикосновения у меня внутри всё словно застыло."
    th "Я должен, должен ей обо всем рассказать! Всю правду!"
    "Но я понимал, что не могу. Это невозможно."
    un "Да нет, вроде не горячий. В чём же дело?"
    me "Да я… {w}на солнце п-перегрелся…"
    "Лена почувствовала дрожь в моей ладони и отпустила руку."
    show un surprise pioneer with dspr
    un "Всё в порядке?"
    me "Да… В полном…"
    hide un with dspr
    "Оставшуюся часть пути до столовой Лена рассказывала про Драйзера, но я слушал вполуха."
    "Во-первых, я Гамлет, а во-вторых, мне всё не давала успокоиться странная «забывчивость» Лены."
    "…"
    $ renpy.pause (1)
    stop music fadeout 2
    stop ambience fadeout 1
    scene black with dissolve
    window hide
    $ renpy.pause (1)
    play ambience ambience_camp_center_evening fadein 2
    scene bg ext_dining_hall_near_sunset with dissolve2
    window show
    $ renpy.pause (0.5)
    "Подойдя к столовой, мы увидели Славю в окружении толпы пионеров."
    show sl normal pioneer far with dissolve
    sl "Ребята, без паники! Горн дадут через пять минут!"
    sl "У поваров возникли какие-то трудности, и они просят подождать. Сохраняйте спокойствие!"
    "Ага! Помощь Слави всё-таки пригодилась!"
    "Но, судя по недовольному гулу, сохранять спокойствие голодные пионеры не хотели."
    "Славя увидела нас с Леной и помахала. Я помахал в ответ."
    "Видимо, до конца ужина поговорить с ней не выйдет."
    scene black with dissolve
    $ renpy.pause (0.1)
    scene bg ext_dining_hall_near_sunset with dissolve
    play sound sfx_dinner_horn_processed
    "Горн наконец прозвучал, и пионеры всей толпой ринулись к дверям."
    "Я вздохнул. Каждый вечер одно и то же."
    stop ambience fadeout 1
    scene bg int_dining_hall_sunset with dissolve
    play ambience ambience_dining_hall_full
    $ renpy.pause (0.5)
    "В этот раз мой любимый стол был не занят."
    "Я уселся и с удовольствием запустил вилку в салат."
    th "Ах, оливье!"
    $ renpy.pause (1)
    "Надо мной кто-то вежливо покашлял."
    show sl shy pioneer with dissolve
    sl "Можно?"
    "Это было неожиданно."
    me "Разумеется, Славя, садись."
    show sl normal pioneer close with dissolve
    "Славя села напротив и пожелала приятного аппетита. Я ответил тем же."
    $ renpy.pause (2)
    show sl smile pioneer close with dissolve
    "Жуя салат, я бросал на неё косые взгляды."
    "Но она ничем себя не выдавала: не дрожала, не смотрела по сторонам и на меня.{w} В общем, вела себя как самый обычный человек в столовой — без лишних движений поглощала пищу.{w} Хм."
    me "Славя, тут это…{w} Новости есть."
    show sl surprise close pioneer with dspr
    "Славя оторвалась от еды и внимательно посмотрела на меня."
    sl "Говори."
    "Я отдал ей бумажку и рассказал, как обнаружил её под одеялом."
    "Славя покачала головой."
    show sl serious close pioneer with dspr
    sl "Значит, у него есть ключ. Интересно…"
    stop ambience fadeout 4
    hide sl with dspr
    "И склонилась над запиской."
    "Я изо всех сил всматривался ей в лицо, но не смог заметить ни тени сомнения."
    "Славя действительно думала."
    show sl serious close pioneer with dspr
    sl "Это английский?"
    me "Да, стихотворение Шекспира."
    me "Из подчёркнутых слов составляется выражение «Зачем меняться? К чему изобретать новые методы?». Но вот с шифром я не разобрался…"
    show sl surprise close pioneer with dspr
    sl "Хм… А ты знаешь английский?"
    "Не каждый школьник способен без словаря перевести Шекспира. Подозрительно."
    me "Знаю, но очень плохо. Еле перевёл."
    "Славя продолжала разглядывать записку."
    hide sl with dspr
    show sl serious pioneer close at right with dspr
    play sound paper
    show note_07_7 with dspr
    sl "Слушай, а может это быть словом? Ну, для каждой буквы свой номер."
    "И, не дожидаясь ответа, принялась отсчитывать буквы."
    "Я внимательно следил, чтобы она не ошиблась."
    "…"
    $ renpy.pause (2,hard=True)
    show note_07_8 with dspr
    $ renpy.pause (1.5,hard=True)
    me "Нет, сомневаюсь, что это слово. Даже если буквы местами поменять."
    sl "Хм…"
    show note_07_9 with dspr
    $ renpy.pause (2)
    me "А может…"
    sl "А может, нужно вести отсчёт от подчёркнутых слов?"
    me "Думаешь, их для этой цели подчеркнули?"
    me "Мне казалось, из них какое-то предложение нужно составить…"
    "Но Славя уже не слушала."
    "Она снова отсчитывала буквы."
    $ renpy.pause (3,hard=True)
    show note_07_10 with dspr
    $ renpy.pause (2,hard=True)
    sl "{i}«ascent»…{/i} Есть такое слово?"
    play music strange fadeout 1
    "Мир замер."
    hide note_07_7
    hide note_07_8
    hide note_07_9
    hide note_07_10 with dspr
    hide sl with dspr
    show sl serious pioneer close at center with dspr
    me "Восхождение…"
    show sl scared pioneer close with dspr
    sl "Восхождение?"
    sl "Восход?"
    "Я сидел без движения. Судя по всему, это таинственное «восхождение» является ключом к происходящему.{w} Но как его использовать? Как найти замок к этому ключу?"
    "Я посмотрел на взволнованную Славю."
    "Нет. Играть так подлинно и честно невозможно…"
    $ renpy.pause (0.5)
    "Но если это не Славя, то кто?"
    $ renpy.pause (1)
    me "Славя, я должен показать тебе ещё кое-что."
    me "Тебе знаком этот предмет?"
    show red_pen_1 with dspr
    show sl normal pioneer close with dspr
    "Я положил на стол ручку. Славя взяла её и покрутила в руках."
    sl "Нет."
    me "Ну вот… Я её около домика нашёл и хотел хозяину вернуть."
    sl "Красные ручки обычно бывают у учителей и во всяких учреждениях, где много пишут. В библиотеке точно должны быть."
    sl "Ты спроси у Жени. Если у кого-то в лагере может быть такая ручка, то только у неё."
    "К Жене идти?"
    "Ну уж нет, благодарю покорно…"
    me "Хорошо, спасибо…"
    show sl surprise pioneer close with dspr
    sl "Около домика нашёл, говоришь?"
    me "Да."
    sl "Чьего домика?"
    me "Около домика…{w} кибернетиков!"
    show sl normal pioneer close with dspr
    "Мне кажется или я вижу облегчение во взгляде Слави?"
    sl "А, понятно…{w} Ну, держи."
    hide red_pen_1 with dspr
    "Славя протягивает мне ручку. Я забираю её, попутно касаясь Славиной ладони.{w} Она холодная…"
    "Странно, ведь на улице градусов двадцать пять, не меньше…"
    show sl serious pioneer close with dspr
    "Славя задумалась. Я тронул её за плечо и показал на тарелку с пюре."
    stop music fadeout 2
    me "Ты со всеми этими тайнами совершенно забыла про еду.{w} Да и я тоже."
    show sl shy pioneer close with dspr
    sl "Извини, Семён, мне что-то не хочется. Я, пожалуй, пойду."
    "— с виноватым видом пробормотала Славя."
    show sl shy pioneer with dspr
    me "Конечно, иди…{w} Может, тебе булочек взять?"
    sl "Спасибо, Семён, не стоит. Я правда не хочу есть."
    me "Ну ладно…{w} Не забивай себе голову чепухой, ладно? Всё будет нормально!"
    "Славя кивнула и улыбнулась. Вышло у неё это не очень."
    sl "Конечно. Доброй ночи, Семён!"
    me "Доброй ночи!"
    hide sl with dspr
    $ renpy.pause (1,hard=True)
    "Хм."
    "Это так на неё непохоже."
    $ renpy.pause (0.5)
    "Я для приличия с минуту посидел за столом, а потом бросился за ней, забыв убрать поднос."
    "…"
    stop ambience fadeout 1
    window hide
    scene black with dissolve
    scene bg ext_dining_hall_near_sunset with dissolve
    play ambience ambience_camp_center_evening
    window show
    $ renpy.pause (0.3)
    "В мгновение ока слетев по ступенькам, я выбежал на крыльцо и принялся вертеть головой влево и вправо, отчаянно пытаясь увидеть Славю или хотя бы её очертания."
    "Но всё тщетно — её нигде не было."
    "Зато была Алиса, которая с явным интересом наблюдала за моими странными действиями."
    show dv normal pioneer with dspr
    me "Алиса! Ты видела Славю?"
    show dv grin pioneer with dspr
    dv "Видела, конечно. А тебе на что?"
    "Алиса стояла облокотившись на заборчик и с хитрой улыбочкой поглядывала на меня."
    me "Мне очень-очень нужно!{w} Я ей денег должен."
    show dv laugh pioneer with dspr
    dv "Денег? Так давай мне, я ей передам. Рядом живём всё-таки."
    me "Да я сам хотел передать. Там сумма очень уж…{w} приличная!"
    show dv normal pioneer with dspr
    dv "Приличная, говоришь… А мне ты не доверяешь?"
    "Чёрт! И почему мне в голову пришло именно это?"
    show us smile pioneer at right with dspr
    us "Кончай путать его, Алиса."
    us "Вон туда Славя ушла, к домикам. Если поторопишься, то догонишь."
    "Взявшаяся словно из ниоткуда Ульяна спасает мне жизнь. Я поворачиваюсь к ней и благодарно киваю."
    $ renpy.pause (0.3)
    "И ещё у меня в голове внезапно вспыхивает идея!"
    me "Ульяна, слушай! А у тебя бинокль с собой?"
    us "Какой бинокль?"
    me "Ну с которым ты сегодня в окна смотрела. Маленький такой!"
    show us grin pioneer at right with dspr
    us "А! С собой. Сейчас, погоди."
    hide us with dspr
    "Ульяна пошарила по карманам и извлекла из широких штанин (нет) бинокль."
    show us grin pioneer at right with dspr
    me "Завтра верну, обещаю!"
    show us laugh pioneer at right with dspr
    us "Хорошо! Только за девочками не подглядывай!"
    show dv laugh pioneer with dspr
    dv "А он может!"
    dv "Не забудь шторы сегодня задёрнуть!"
    hide dv with dspr
    hide us with dspr
    "Но я уже не слышал их."
    "Я со всех ног бежал в направлении, указанном Ульяной."
    th "Хоть бы догнать…"
    "…"
    stop ambience fadeout 1
    window hide
    scene black with dissolve

    scene bg ext_houses_sunset with dissolve
    play ambience ambience_camp_center_evening
    window show
    show sl serious pioneer far with dspr
    "Спустя пару минут перед моим взором предстают две белые косы Слави."
    "Я держусь на почтительной дистанции и отмечаю деревья, за которыми можно будет в случае чего спрятаться."
    "Таких, к сожалению, очень немного."
    "Но благо Славя не оборачивается, так что слежку можно вести с комфортом, напевая песенки и считая шаги."
    "…"
    hide sl with dspr
    scene black with dspr
    $ renpy.pause (1,hard=True)
    scene bg ext_house_of_un_day with dspr
    $ renpy.pause (1,hard=True)
    "Подобным образом я провожаю Славю до её домика. Она скрывается в его дверях, а я отхожу подальше, чтобы не привлекать к себе внимания."
    "Заняв наблюдательную позицию на скамейке, я сложил руки в замок и принялся делать вид, что «тупа чиллю-отдыхаю»."
    "Из домика меня не заметить, разве что специально повернуться и сделать несколько шагов. Зато я всё вижу как на ладони."
    "Я заставляю себя расслабиться и наслаждаться летним вечером, при этом не отводя глаз от дверей домика."
    "Придётся ждать."
    "…"
    $ renpy.pause (1,hard=True)
    stop ambience fadeout 1
    scene black with dspr
    $ renpy.pause (1,hard=True)
    scene bg ext_house_of_un_day with dspr
    play ambience ambience_camp_center_evening
    show sl normal pioneer far with dspr
    $ renpy.pause (1,hard=True)
    "Спустя минут двадцать Славя выходит."
    "Я медленно встаю и захожу за ближайшее дерево, стараясь не выдать своего присутствия."
    "Но заметить меня сложно — я ступаю неслышно, а за стволом дуба могут спрятаться минимум двое."
    $ renpy.pause (1,hard=True)
    "Славя, кстати, вышла с увесистой сумкой на плече."
    "Куда это она, интересно, на ночь глядя?"
    "Дождавшись, пока она отойдёт достаточно далеко, я выхожу из укрытия и направляюсь за ней."
    $ renpy.pause (1,hard=True)
    th "Только бы не заметила!"
    "Попасться сейчас — значит облажаться в самом начале и не приблизиться к правде."
    "Провал неприемлем."
    $ renpy.pause (1,hard=True)
    scene bg ext_square_sunset with dissolve
    "Мы проходим площадь…"
    scene bg ext_house_of_mt_sunset with dissolve
    "Наш с ОД домик…"
    scene bg ext_dining_hall_away_sunset with dissolve
    "Столовую…"
    scene bg ext_clubs_sunset with dissolve
    "Клубы…"
    "Электроника, кстати, на ужине не было."
    "Так и слушает {i}голос Америки{/i}, наверное."
    scene bg ext_path_sunset with dissolve
    "Лес…"
    "А что она в лесу-то забыла?"
    "Сомневаюсь, что сейчас лучший момент собирать грибы."
    $ renpy.pause (0.3,hard=True)
    "Хотя…{w} Сумку-то она для чего-то взяла?"
    scene bg ext_banya_something with dissolve
    "В конце концов мы приходим к бане.{w} Славя скрывается внутри."
    "И всё?"
    "Чёрт!"
    "Девушка просто отправилась помыться. А я навыдумывал себе не пойми чего!"
    $ renpy.pause (0.3)
    "Я отхожу чуть назад и располагаюсь…{w} Где бы вы думали?"
    "В ближайших кустах!"
    "М-да."
    "Уже второй раз за день мне приходится прятаться от девушек в зелёных зарослях.{w} Главное, чтобы в привычку не вошло."
    "Если бы Ольга Дмитриевна узнала, наверняка заперла бы меня в комнате до конца смены.{w} Или чего похуже придумала…"
    "…"
    stop ambience fadeout 1
    window hide
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    $ persistent.sprite_time = 'night'
    $ night_time()
    play ambience ambience_camp_center_night
    $ volume(1.1,'ambience')
    scene bg ext_bathhouse_night_1 with dissolve
    window show
    $ renpy.pause (1)
    "Дело шло к вечеру."
    "В надежде, что Славя пробудет там недолго, я поудобнее устроился на еловых ветках."
    "Горько будет признавать, что слежка не удалась."
    "К тому же мне точно надо узнать, замешана Славя во всём этом или нет."
    "Ничего, после высплюсь."
    "Необходимо было придумать себе занятие."
    "Сначала я немного погрустил, повспоминав дом и Карла Ивановича."
    "От дома перешёл к работе. От работы — к любимому китайскому."
    "Какой бы чэнъюй подошёл к этой ситуации?"
    "«И сы пу гоу»?"
    "«Не упускать ни одной мелочи»?"
    $ renpy.pause (1)
    "А что, подходит!"
    me "А, кстати! У меня же бинокль есть!"
    "Я извлекаю чудо-инструмент и приставляю его к глазам."
    scene bg ext_bathhouse_night_1_close with dissolve
    "Подкрутив колёсико, я настраиваю кратность так, что кажется, будто окно в двух шагах. Свет горит — значит, Славя там."
    "Хотя стекло запотело и ничего не видно. Да и куст этот дурацкий половину окна закрывает!"
    $ renpy.pause (1,hard=True)
    "Ладно, не очень-то и хотелось…"
    "…"
    $ renpy.pause (0.3)
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg ext_bathhouse_night_1_close with dissolve
    $ renpy.pause (1)
    "Прошёл по меньшей мере час."
    "Всё это время я смотрел в бинокль и обкладывал себя ветками. Теперь, даже если кто-то пройдёт мимо в трёх шагах, то вряд ли меня заметит."
    "Я с гордостью подумал, что из меня вышел бы превосходный снайпер."
    th "Ах, Вьетнам…"
    me "ДЖОННИ, ОНИ НА ДЕРЕВЬЯХ!"
    show vietnam with dissolve
    play music vietnam_1 fadein 1
    $ renpy.pause (5,hard=True)
    hide vietnam with dspr
    stop music fadeout 1
    $ renpy.pause (2,hard=True)
    "Так, я это вслух сейчас сказал?"
    "…"
    $ renpy.pause (1,hard=True)
    "Не отвлекаемся!" with hpunch
    $ renpy.pause (1,hard=True)
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg ext_bathhouse_night_1 with dissolve
    me "Нет, ну сколько можно уже? Она там всю ночь сидеть будет?"
    "Стрекочут кузнечики. Каркает ворона.{w} Аромат еловых ветвей странно клонит в сон…"
    me "Н-нет…{w} Я не могу уснуть!"
    me "Бр-р!" with hpunch
    "Я встряхиваюсь и продолжаю смотреть в бинокль.{w} Смотрю."
    "Смотрю."
    "Смотрю."
    "Смотрю…"
    $ renpy.pause (2,hard=True)
    scene bg ext_bathhouse_night_1_blur with dissolve2
    "И вдруг понимаю, что куда-то проваливаюсь.{w} Но не во Вьетнам, нет."
    "Я тяну брови вверх, изо всех сил стараясь удержать ускользающее сознание."
    "Но запах — не еловый, а будто какой-то мятный — такой приятный, и сопротивляться нет сил…"
    "Я закрываю глаза и опускаю голову на мягкую подушку из еловых ветвей."
    window hide
    stop ambience fadeout 2
    show blink
    $ renpy.pause (3,hard=True)
    play ambience ambience_camp_entrance_night fadein 3
    $ volume(0.5, "ambience")
    window show
    $ renpy.pause (2,hard=True)
    "Как холодно…{w} Окно, что ли, открыто?"
    hide blink
    show unblink
    scene bg ext_bathhouse_night_2 with dissolve
    "Я открываю глаза и понимаю, что нахожусь не в домике, а в своей засаде.{w} И что уже глубокая ночь."
    "Чёрт, и из-за этого тумана не видно ничего…{w} Или это не туман?"
    "Какой-то металлический привкус во рту…"
    "И почему-то сильно болит голова."
    hide unblink
    "Я перевожу взгляд на баню и даже без бинокля понимаю, что там никого нет: свет не горит и вокруг стоит какая-то странная тишина, нарушаемая только негромким стрекотом кузнечиков."
    "Сам бинокль лежит чуть левее. Я тянусь за ним и осознаю, что мой правый кулак холодный и как-то странно сжат.{w} Отлежал, что ли?"
    "Я трясу рукой."
    stop ambience fadeout 1
    "Она разжимается…"
    "…и из ладони вылетает скомканный клочок бумаги."
    play music music_list["torture"] fadein 1
    $ volume(0.8, "music")
    $ renpy.pause (1,hard=True)
    "Я чувствую, как лоб покрывается испариной."
    "Странно, мне же всего две минуты назад было холодно…"
    $ renpy.pause (0.8,hard=True)
    window hide
    play sound paper
    show note_08_11 with dissolve
    $ renpy.pause (10,hard=True)
    hide note_08_11 with dspr
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (1)
    scene black with dissolve
   
    $ renpy.pause (2,hard=True)
    $ persistent.sprite_time = 'day'
    $ day_time()
    play ambience ambience_int_cabin_day fadein 4
    $ volume(0.3, "ambience")
    scene bg int_house_of_mt_day with dissolve
    window show
    $ renpy.pause (1,hard=True)
    "Наступило утро."
    "За всю ночь мне так и не удалось хотя бы на минуту сомкнуть глаз."
    "Я не помню, как дошёл до домика. {w}Я и сейчас будто в бреду."
    "Мне не удалось перехитрить Славю. Это {i}она{/i} перехитрила меня."
    "Должно быть, заметила, как я иду по пятам.{w} Через окно увидела, куда я прячусь.{w} Дождалась, пока я усну."
    "И нанесла удар."
    window hide
    stop music fadeout 2
    $ renpy.pause (2,hard=True)
    $ set_mode_nvl()
    $ renpy.pause (1,hard=True)
    play music music_list["into_the_unknown"] fadein 1
    "Я лежал на кровати и чувствовал внутри себя страх.{w} Сердце билось так быстро, будто я только что бегом поднялся на двенадцатый этаж."
    "За всю свою жизнь я ничего так не боялся. Ни экзаменов, ни важных встреч."
    "Даже в тот момент, когда на нас с Карлом Ивановичем ночью в Харбине напали грабители, я не чувствовал себя так, как сейчас."
    "Тогда нам (точнее, мне) повезло. В трости у Карла Ивановича был спрятан стилет — тонкий и острый."
    "Одному грабителю он рассёк плечо, второму — артерию на шее (Truncus Brahiocephalicus, как я потом выяснил), и они бросились наутёк."
    "Но тогда мы знали, как бороться с соперниками. А сейчас…"
    $ renpy.pause (1,hard=True)
    nvl clear
    "Страх перед неизвестным — самый неприятный. Ты не знаешь, чего ожидать, и это гнетёт больше всего."
    "И что хуже всего, я ведь не могу даже точно сказать, Славя ли это была. Я ведь не видел, как она ко мне подходила, не видел её лица."
    "А вдруг это не она?"
    $ renpy.pause (1)
    nvl clear
    stop music fadeout 1
    $ set_mode_adv()
    window show
    $ renpy.pause (1,hard=True)
    "Я откинул голову на подушку и заставил себя перестать думать об этом."
    th "Хорошо бы поспать хоть немного…"
    $ renpy.pause (0.5)
    play sound sfx_knock_door3_dull
    $ volume(1.8,'sound')
    "Но тут внезапно раздался стук в дверь.{w} Довольно громкий."
    "Я вздохнул, еле как поднялся с кровати и пошёл открывать."
    $ renpy.pause (1,hard=True)
    play sound sfx_open_door_1 
    show sl scared pioneer with dspr
    $ renpy.pause (0.7,hard=True)
    play music what_you_can fadein 2
    "На пороге стояла Славя."
    "Я отшатнулся, но она будто не заметила."
    sl "Семён! У нас ЧП!"
    "Пока что лучше всего будет подыграть ей. Чтобы предъявлять обвинения, нужны либо улики, либо веские доказательства, а у меня нет ни того, ни другого."
    me "Д-доброе утро… или день. А что такое?"
    "Лицо Слави было белее мела. Я заметил это только сейчас."
    sl "Я не увидела тебя на завтраке и решила, что ты проспал. Подошла к домику, а здесь…{w} Выйди и сам посмотри!"
    "Она судорожно вздохнула и замолчала."
    "Я прямо в носках вышел на крыльцо."
    stop ambience fadeout 1
    scene bg ext_house_of_mt_day with dissolve
    play ambience ambience_camp_entrance_day
    "Подошёл к шезлонгу."
    $ renpy.pause (1,hard=True)
    "{i}Там лежало тело Ольги Дмитриевны.{/i}"
    "…"
    $ renpy.pause (2,hard=True)
    "В голове вертелись лишь слова {i}«восхождение моё будет ночи черней»{/i}."
    "Так вот что за «восхождение»…"
    me "Она…{w} М-мертва?"
    show sl scared pioneer with dspr
    sl "Нет, она дышит. Но очень медленно."
    sl "Посмотри, она же почти синяя!"
    sl "Будто в ней совсем не осталось жизни…"
    "Я подошёл и проверил пульс."
    "Тело Ольги Дмитриевны было как лёд, но пульс действительно был. 15–20 ударов в минуту, не более."
    me "Что же с ней такое? Это какой-то… летаргический сон?"
    sl "Может быть."
    show sl serious pioneer with dspr
    sl "Но как она здесь оказалась?"
    sl "Ты же говорил, что она уехала в город!"
    me "Так она и уехала…{w} или не уезжала?"
    "В конце концов, не сама же она до шезлонга дошла."
    $ renpy.pause (2,hard=True)
    "Дело принимало очень скверный оборот."
    $ renpy.pause (0.5)
    play sound sfx_face_slap
    with hpunch
    "Я побил себя по щекам. Успокоился."
    "В моменты крайнего стресса человеческий мозг активируется и начинает работать очень точно и быстро."
    "Судя по всему, я окончательно «пробил дно» и достиг именно такого состояния."
    "В голове у меня одна за другой рождались идеи, и я сразу озвучивал их, боясь, что они ускользнут так же легко, как и пришли."
    me "Первое. Судя по всему, Ольга Дмитриевна всё время была в лагере. Кто-то ещё три дня назад что-то с ней сделал и спрятал, а теперь принёс сюда, чтобы мы нашли. Но главное, что она жива."
    me "Второе. Одна бы ты провернуть это вряд ли смогла, так что вопрос о твоей причастности временно снимается."
    me "Либо же у тебя были сообщники, но это уже совсем другое дело."
    show sl surprise pioneer with dspr
    "Славя слушала меня с открытым ртом."
    sl "Моей…{w} причастности? "
    show sl angry pioneer with dspr
    extend "Ты подозревал меня? Что я подбрасываю тебе записки?"
    "Славино удивление постепенно перерастало в гнев."
    sl "Ну знаешь ли, Семён!"
    sl "У меня есть дела и поважнее, кроме как бумажки тебе писать!"
    sl "Да и Ольгу Дмитриевну!.."
    sl "Это как ты должен был до этого…"
    show blink
    "Я закрыл глаза."
    "Мне казалось, что я нахожусь в пустой стеклянной комнате, где нет никого, кроме меня и моих мыслей."
    hide sl with dspr
    me "И третье. Теперь мы знаем, что записки — это дело рук не какого-то шутника, а самого настоящего психопата, которого нужно остановить."
    me "Ибо если это действительно «восхождение»…"
    hide blink
    show unblink
    me "То он не остановится, пока не дойдёт до самой вершины."
    "Открыв глаза, я имел перед собой чёткую картинку."
    "Взяв Славю за руку, я повёл её в домик — объяснить план дальнейших действий и показать записку, найденную сегодня ночью."
    "Нельзя было терять ни минуты."
    window hide
    $ renpy.pause (0.5)
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    window show
    $ renpy.pause (1,hard=True)
    scene bg ext_house_of_mt_day with dissolve
    "Едва дверь закрылась, от дерева отделился силуэт."
    show kw ser with dissolve
    "Высокий человек, стоявший там всё это время, медленно зашагал к домику, стараясь всё время оставаться в тени."
    hide kw ser with dspr
    "Встав около шезлонга, он наклонился над Ольгой Дмитриевной и с интересом взглянул на её лицо."
    "Кивнув самому себе, он аккуратно повернул её голову и пристально рассмотрел затылочную артерию.{w} Цокнул языком."
    "Кивнул ещё раз.{w} Вздохнул."
    show kw ser with dspr
    waeber_unknown "Опасную игру ты затеял, мой дорогой друг…"
    hide kw ser with dspr
    "Выпрямившись, он провёл рукой по лицу и сделал шаг по направлению к домику."
    window hide
    $ renpy.pause (0.5)
    scene black with dissolve
    play ambience ambience_int_cabin_day
    scene bg int_house_of_mt_day with dissolve
    show sl serious pioneer with dspr
    $ renpy.pause (0.5)
    window show
    $ renpy.pause (0.5)
    me "…и, проснувшись, обнаружил в руке эту записку."
    "Славя ещё раз скользнула взглядом по красным строкам и вновь задумалась."
    me "Говорю тебе, она и Ольга Дмитриевна как-то связаны!"
    sl "Может быть."
    sl "А ещё может быть, что шутник и Ольга Дмитриевна — совершенно разные дела."
    sl "Вдруг этот неизвестный играет на нашей стороне? Пытается предупредить нас?"
    me "Очень глубоко в этом сомневаюсь."
    play sound sfx_knocking_door_outside
    $ volume(0.5,'sound')
    $ renpy.pause (1)
    "Раздался негромкий стук в дверь."
    me "Ну кого там ещё чёрт принёс?"
    play sound sfx_open_door_1
    $ volume(0.5,'sound')
    "Я открыл дверь… {w}И не поверил своим глазам."
    stop ambience fadeout 2
    me "Не может быть…{w} Карл?!"
    scene black with dissolve
    jump label_mist_2


label label_mist_2:
    $ save_name = ('Схватка с неизвестностью.\nЧасть 2: Триумф.')
    $ new_chapter(3, u"Схватка с неизвестностью.\nЧасть 2: Триумф.")
    stop ambience fadeout 2
    stop music fadeout 2
    $ renpy.pause (2,hard=True)
    show parttwo with dissolve
    $ renpy.pause (2,hard=True)
    hide parttwo with dissolve
    scene anim prolog_1 with dissolve 
    $ renpy.pause (1)
    $ prolog_time()
    $ set_mode_nvl()
    play music endless_rain fadein 2
    "Что есть сон?"
    "Любой из нас хотя бы раз в жизни задавал себе этот вопрос."
    "В давние времена считалось, что сон — скрытое послание богов. Толковать сны правителей доверяли только самым мудрым из людей, и работа эта считалась очень престижной.{w} И очень ответственной."
    "Приснился королю ворон — к чему это?"
    "К засухе? К гибели?"
    "Процветанию?"
    "Провидцы часто ошибались и, как следствие… часто менялись."
    nvl clear
    "Сегодня же мы считаем, что сны — всего лишь проблески сознания в темной пучине мыслей."
    "За исключением некоторых «особенных» людей, мы не можем контролировать то, что видим, сомкнув глаза ночью."
    "Сны неподвластны нашему пониманию и контролю. Мы можем лишь наблюдать, как привычный мир меняется на глазах."
    $ renpy.pause (0.5,hard=True)
    $ set_mode_adv()
    $ day_time()
    $ persistent.sprite_time = 'day'
    scene cg sl_mirror_1 with dissolve2
    $ renpy.pause (1,hard=True)
    window show
    "Так думала Славя, смотря на своё отражение в зеркале."
    th "Всё произошло так быстро…"
    th "Всё шло своим чередом…{w} и в один момент полетело вверх дном."
    th "Сначала загадочное исчезновение Ольги Дмитриевны…"
    th "Потом записки…"
    th "Теперь этот странный человек, стоящий в центре комнаты."
    $ renpy.pause (1,hard=True)
    th "Кто он?"
    "Славя пару раз видела его в лагере раньше, но не думала, что им предстоит познакомиться…{w} Да ещё и при таких обстоятельствах."
    scene bg int_house_of_mt_day with dspr
    hide sl_mirror_1 with dspr
    "Словно внезапно вспомнив о её присутствии, Семён обернулся."
    show pi2 smile at left with dspr
    me "Точно!"
    me "Славя, ты ведь не знаешь господина Вебера?"
    me "Вас надо срочно познакомить!"
    "Славя попыталась изобразить робкую улыбку."
    "Парень же сделал к ней пару шагов и наклонил голову в приветствии."
    show kw with dspr
    "Его голубые глаза смотрели смотрели холодно и изучающе."
    kw "Карл Иванович Вебер."
    kw "Лучше просто Вебер."
    kw "А вы, я так понимаю, Славя?"
    sl "Да. Вообще Славяна, но лучше Славя."
    kw "Замечательно."
    hide kw with dspr
    $ renpy.pause (0.3)
    show kw with dspr
    "Вебер поклонился и обратил свой колючий взгляд обратно к Семёну."
    "Но Семён словно не чувствовал никакой неловкости."
    "Наоборот: Славе ещё не доводилось видеть его таким приветливым и улыбчивым."
    th "Кто он для него?{w} Друг, который потерялся десять лет назад, а сейчас волшебным образом нашёлся?"
    show kw smile with dspr
    kw "Ну что же…{w} Мне кажется, нам обоим есть что поведать друг другу?" 
    "— проговорил Вебер, глядя на Семёна с полуулыбкой."
    me "Да, только ты первый начинай!"
    show kw ser with dspr
    kw "Вот так, значит?{w} Ну хорошо."
    kw "Начнём с того, как я попал сюда. Это было весьма…{w} М-м-м… "
    show kw smirk with dspr
    extend "необычно."
    show pi2 laugh with dspr
    "При этих словах Карл чуть улыбнулся. Семён же едва не расхохотался."
    me "Ай-ай! Неужто заснул на работе?"
    show kw smile_2 with dspr
    kw "Откуда знаешь?"
    "Семён рассмеялся и махнул рукой, позволяя Веберу продолжить."
    show pi2 smile with dspr
    show kw with dspr
    kw "Ладно, если серьёзно, я в этой «ловушке» уже пять дней."
    kw "Провожу лето с пользой: подрабатываю вожатым."
    show kw disapp with dspr
    kw "Поистине незабываемый опыт."
    th "Вожатым?"
    th "Невозможно!"
    "Славя знала всех вожатых в лагере, благо их было не так уж и много."
    "На первой линейке было общее знакомство, и Вебера там точно не было."
    th "Но это было в первый день…"
    th "К тому же как он попал сюда?"
    th "«Заснул на работе»?"
    $ renpy.pause (0.5,hard=True)
    th "Бред какой-то."
    th "Нет, это точно сон.{w} Или уравнение, в котором слишком много неизвестных."
    show kw with dspr
    kw "…Отбросив на второй день все сомнения относительно реальности лагеря и всех людей вокруг, я принялся изучать окрестности."
    kw "Пытался понять, что же всё-таки произошло."
    show pi2 smile with dspr
    me "И как?"
    show kw sad with dspr
    "Вебер вздохнул."
    kw "Если коротко — никак."
    kw "Вопросов действительно больше, чем ответов."
    show kw ser with dspr
    kw "Есть ещё кое-что."
    kw "Здесь происходит ещё нечто более странное, не так ли?"
    show pi2 ser with dspr
    "Семён нахмурился."
    me "С чего ты так решил?"
    th "Он ведь ещё не знает…"
    show kw surp with dspr
    kw "У вас девушка без сознания лежит в шезлонге, вы в курсе?"
    show pi2 upset with dspr 
    "Семён виновато кашлянул."
    me "Да…{w} В последнее время здесь творится очень много непонятного."
    me "Лучше присядь. Это будет долгая история."
    hide pi2 with dspr
    hide kw with dspr
    window hide

    $ renpy.pause (0.5)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg int_house_of_mt_day with dspr
    window show
    $ renpy.pause (0.5)
    "Выслушав рассказ Семёна и покрутив в руках записки, Вебер поднялся… "
    show kw ser with dspr
    extend "И обратил свой холодный взгляд к Славе."
    kw "Вы же осознаёте, что дело совсем не шуточное?"
    kw "Из всего лагеря записки приходили только Семёну."
    kw "Если принять за условие, что это действительно маньяк или, скорее, скрытый психопат, то вы вполне можете стать его следующей жертвой."
    "Славя замерла."
    sl "Я?"
    sl "Но ведь Семён тоже…"
    show kw disapp with dspr
    "Вебер устало потёр переносицу."
    kw "С Семёном ничего не случится.{w} Скорее всего."
    kw "Он проводник.{w} Или оппонент."
    show pi2 surp at left with dspr
    me "«Проводник»?"
    show kw thinking with dspr
    kw "Насколько я понял из этих записок, наш милый друг хочет, чтобы с ним сразились."
    kw "Только что это за игра, знает лишь он сам."
    show pi2 with dspr
    show kw ser with dspr
    "Славя вновь ощутила на себе его холодный взгляд."
    kw "Вы можете уйти. Это будет безопаснее."
    kw "За Семёном наверняка следят, так что вы подвергаете себя опасности, просто находясь рядом."
    kw "Причём опасности совершенно неоправданной и ненужной."
    sl "А как же вы?"
    show pi2 ser with dspr
    me "Это другое дело."
    "— внезапно произнёс Семён."
    "Он не отводил взгляда от Вебера."
    me "Я уверен, Карл здесь не просто так."
    me "Он поможет мне понять, в чём тут дело."
    me "Не может быть, что мы оба оказались здесь совершенно случайно."
    show pi2 upset with dspr
    me "Это всё для чего-то нужно…"
    "— добавил Семён нерешительно и опустил глаза."
    $ renpy.pause (0.5)
    sl "Нет."
    "— уверенно сказала Славя."
    show kw surp with dspr
    kw "Что «нет»?"
    sl "Я не отступлю."
    sl "Не брошу Семёна."
    show pi2 surp with dspr
    me "Но подумай…"
    "— начал Семён. Вебер жестом руки остановил его."
    show kw smile_2 with dspr
    kw "Не стоит."
    "Он стоял, склонив голову, и смотрел на Славю."
    kw "Её ты вряд ли переубедишь."
    me "Почему?"
    kw "А ты разве не видишь?"
    me "Чего не вижу?"
    $ renpy.pause (0.5,hard=True)
    with hpunch
    sl "Довольно!"
    sl "Нам что, больше заняться нечем, кроме как болтать?"
    "— неожиданно раздражённо произнесла Славя."
    "Семён с непониманием переводил взгляд с Вебера на Славю и обратно."
    show kw ser with dspr
    kw "Да, действительно…{w} У вас есть какие-нибудь мысли насчёт наших дальнейших действий?"
    show pi2 with dspr
    me "Мы тут как раз думали, что делать, когда ты постучал."
    kw "И что придумали?"
    show pi2 upset with dspr
    me "Ничего…"
    show kw sad with dspr
    "Вебер вздохнул."
    show kw with dspr
    kw "Первым делом предлагаю занести девушку в дом."
    kw "Это ваша вожатая, да?"
    kw "Судя по её состоянию, два-три дня она ещё точно продержится. Дальше — неизвестно."
    sl "А вы встречались с такого рода… травмами?"
    kw "Не совсем, но видел что-то очень похожее."
    show kw thinking with dspr
    kw "Скорее всего, просто совпадение, ибо получить подобную, как вы сказали, травму здесь практически невозможно."
    sl "Что вы имеете в виду?"
    show kw ser with dspr
    kw "Здесь нет и не может быть людей, которые могли бы это сделать."
    "— коротко ответил Вебер."
    th "Яснее не стало…"
    show pi2 smile with dspr
    me "Кстати, а ты где живёшь-то?"
    kw "В домике напротив административного корпуса. Рядом с первым отрядом."
    kw "А к чему вопрос? Навестить хочешь?"
    show pi2 laugh with dspr
    me "Именно! Составляю вот себе план на ближайшую ночь."
    show kw thinking with dspr
    kw "Нет уж, потрудись найти себе другое занятие на вечер."
    "— рассердился Карл. Семён улыбнулся ещё шире."
    me "Не очень-то и хотелось…"
    th "Не очень, говоришь?"
    show kw with dspr
    kw "Так или иначе, нам всем следует быть настороже."
    show pi2 with dspr
    kw "Семён, пошли."
    "Вебер кивнул головой в сторону двери."
    kw "Славя, постоите настороже? Лучше избегать лишнего шума. Поднимать панику совершенно ни к чему."
    "Славя кивнула."
    th "И долго он ещё ко мне на вы будет?"
    hide pi2 with dspr
    hide kw with dspr
    window hide
    $ renpy.pause (0.5)
    scene black with dissolve
    $ renpy.pause (0.5)
    scene bg int_house_of_mt_day with dspr
    window show
    $ renpy.pause (0.5)
    "Аккуратно положив Ольгу Дмитриевну на кровать, Вебер ненадолго задержался и дотронулся до её шеи."
    "Выпрямившись, он на мгновение прикрыл глаза, словно собираясь с мыслями."
    show kw sad with dspr
    kw "Наведаюсь в библиотеку. Попробую отыскать какие-либо упоминания о подобном «стазисе»."
    show kw thinking with dspr
    kw "Хотя сомневаюсь, что в медицинском справочнике пишут о чём-то подобном."
    kw "Да и откуда тут такой справочник вообще…"
    show pi2 smile at left with dspr
    me "Я с тобой!"
    show kw ser with dspr
    "Карл недоуменно посмотрел на Семёна."
    kw "Думаешь, библиотеку не найду?"
    kw "Нет, спасибо, я вполне способен справиться самостоятельно."
    me "Но ведь там Женя…"
    kw "Разберусь."
    hide kw with dissolve
    "Вебер кивнул Славе и вышел."
    hide pi2 with dspr
    show pi2 upset at center with dspr
    "Семён вздохнул и сел на кровать, закрыв лицо руками."
    me "Его не поймёшь… То он нормальный, то вот такой.{w} Холодный."
    "Славя присела рядом."
    sl "А кто он вообще? Друг?"
    me "Коллега…"
    sl "А ты, значит, Шеф?"
    show pi2 smile with dspr
    "Семён усмехнулся."
    me "Да нет, скорее наоборот."
    sl "Так ты работаешь где-то?"
    show pi2 ser with dspr
    me "Славя, пожалуйста…{w} Не спрашивай ни о чём."
    me "К чему создавать ещё больше тайн?"
    "Семён выглядел уставшим."
    sl "Тебе стоит отдохнуть."
    sl "Встретимся после обеда, хорошо?"
    me "Ладно…"
    show pi2 with dspr
    me "Всё-таки хорошо, что Вебер здесь, да?"
    me "Он поможет нам распутать этот клубок."
    me "Он умный, но не всегда говорит всё, что знает."
    me "Готов поспорить, что он уже наверняка напал на след нашего… «друга»."
    "Славя не могла похвастаться наличием такого же мнения относительно человека, которого узнала лишь пару минут назад."
    th "Но Семён так уверен в нём…"
    th "Только «коллега», значит?"
    "Славя похлопала Семёна по плечу и вышла."
    stop music fadeout 3
    window hide
    $ renpy.pause (0.5)
    scene bg ext_house_of_mt_day with dissolve
    play ambience ambience_camp_entrance_day
    window show
    $ renpy.pause (0.5)
    "Отойдя на пару шагов от домика, Славя остановилась и взглянула на пустой шезлонг."
    th "Ольга Дмитриевна… {w}Надеюсь, всё будет хорошо."
    "Даже находясь в подобном круговороте не совсем приятных событий, Славя не могла забывать о долге."
    "И должности помощницы вожатой."
    "Учитывая обстоятельства, все заботы о руководстве отрядом внезапно перешли на её плечи."
    "Предаваться унынию было нельзя."
    $ renpy.pause (0.5)
    scene bg ext_square_day with dissolve
    $ renpy.pause (0.5,hard=True)
    "Но даже дойдя до площади, Славя не могла отделаться от мысли, что что-то во всей этой истории не сходится."
    th "Появление этого Вебера было слишком уж неожиданным."
    th "Будто он специально подбирал правильный момент, когда его «представление» будет выглядеть наиболее эффектно."
    th "Может ли быть, что автор этих записок — он?"
    th "Но какие у него могут быть мотивы?"
    th "Он ведь и знать не знает Ольгу Дмитриевну…{w} наверное."
    th "А вдруг знает? Что тогда?"
    th "Меняет ли это что-нибудь?"
    "Славя огляделась по сторонам и с радостью заметила несколько желтых листьев, лежащих на площади."
    "Она потянулась за метлой, как вдруг её кто-то окликнул."
    play music music_list["sweet_darkness"] fadein 1
    show mi smile pioneer with dspr
    mi "Славя, привет!~"
    mi "Как дела?"
    sl "Привет, Мику. Вроде неплохо."
    sl "У тебя как?"
    "— проговорила Славя, стараясь выглядеть как обычно."
    "Даже учитывая говорливость Мику, они никогда особо не было близки и знали друг друга на уровне «привет-пока» в столовой."
    "Тот факт, что Мику сама подошла поздороваться, да ещё так бодро, привёл Славю в лёгкое замешательство."
    show mi happy pioneer with dspr
    mi "Отлично!"
    show mi smile pioneer with dspr
    mi "Тут Лена не проходила, нет?"
    sl "В последние пару минут — нет."
    sl "У тебя к ней дело какое-то?"
    mi "Нет-нет, ничего такого! Просто хотела рассказать ей кое-что крайне интересное!"
    sl "Интересное?"
    show mi grin pioneer with dspr
    mi "Да! Но только ей, и никому больше!"
    show mi smile pioneer with dspr
    "Славя пожала плечами."
    sl "Посмотри в домике. Она наверняка там."
    show mi upset pioneer with dspr
    mi "Да нет её там, я только что оттуда!"
    "Мику переминалась с ноги на ногу и смотрела на Славю, словно раздумывая, можно ли доверить ей страшную тайну."
    show mi happy pioneer with dspr
    mi "Ладно, тебе тоже расскажу!"
    mi "Но если увидишь Лену, обязательно передай, слышишь?"
    "Вообще, у Слави не было особого желания выслушивать трескотню Мику."
    th "Тем более, что интересного она может рассказать?"
    show mi smile pioneer with dspr
    mi "В общем, слушай!"
    mi "Выхожу я, значит, минут десять назад из своего клуба."
    mi "Иду себе, иду, как вдруг!"
    mi "Мне навстречу — парень!"
    "Мику сделала большую паузу в явном ожидании, что Славя схватится за голову, громко ахнет и упадёт на землю."
    th "Парень?"
    th "Боже мой, на что я трачу своё время…"
    sl "Эм…{w} И?"
    show mi grin pioneer with dspr
    mi "Парень такой, знаешь, высокий! Держится очень прямо! И весь такой…{w} Ну знаешь…{w} Импозантный!"
    "Было видно, что вспомнив такое сложное слово, Мику была крайне довольна собой."
    sl "А ты что, в первый раз за две недели его увидела?"
    show mi normal pioneer with dspr
    mi "В том-то и дело, что да!"
    mi "Раньше я его не видела, это точно!"
    th "Хм…"
    sl "А у него, часом, глаза не голубые? И рубашка не чёрная?"
    show mi surprise pioneer with dspr
    mi "Про глаза не знаю, но рубашка…"
    show mi dontlike pioneer with dspr
    mi "Эй, а откуда ты знаешь?"
    mi "Уже видела его, да? И мне не сказала!"
    th "Да уж, сейчас бы только бегать по лагерю и рассказывать всем, что дружок Семёна нашелся…"
    sl "Да уж, довелось…"
    sl "А что такое с ним?"
    show mi smile pioneer with dspr
    mi "Да ничего! Карл (странное имя, да?) показался мне очень интеллигентным!"
    sl "В самом деле? Вы уже познакомились?"
    mi "Да, я подошла к нему, и он…{w} Ты не поверишь!"
    show mi surprise pioneer with dspr
    mi "Поклонился! Мне!"
    th "Нет, он точно застрял в 19-м веке, если кланяется каждой девушке."
    show mi normal pioneer with dspr
    mi "Мы немного поговорили, и он сказал, что идёт в библиотеку."
    mi "Вот я и хотела сказать Лене, чтобы она тоже туда пошла!"
    show mi smile pioneer with dspr
    mi "Ведь если он идёт в библиотеку, то любит читать, так?"
    mi "Лена тоже любит читать, так что у них будет о чём поговорить!"
    show mi happy pioneer with dspr
    mi "Надо найти её, пока он не ушёл!"
    sl "А зачем тебе это?"
    show mi surprise pioneer with dspr
    mi "Что?"
    sl "Ну знаешь…{w} Бегать туда-сюда. Знакомить их."
    sl "Выглядит странновато."
    th "Что я говорю…"
    th "Нет, точно надо будет немного отдохнуть."
    th "Отойти от событий этого утра."
    show mi normal pioneer with dspr
    mi "Странновато? А чего же здесь странного?"
    mi "Лена всегда одна. Ей наверняка не с кем поговорить, и в лагере никого нет, кто бы хотел с ней общаться и проводить время."
    mi "Я подумала, что с ним ей будет не так одиноко."
    show mi shy pioneer with dspr
    mi "Они бы обсуждали книги… Делились эмоциями… "
    show mi grin pioneer with dspr
    extend "Ну ты знаешь, как это бывает!"
    th "Действительно…"
    th "И какое я имею право судить о человеке, которого совсем не знаю?"
    th "Может быть, он не такой сноб, каким выглядит?"
    th "Уверена, ему есть что скрывать, но…"
    th "Могу ли я считать его тем самым психопатом лишь потому, что он мне не нравится?"
    th "А, и он, оказывается, мне не нравится."
    th "И почему, интересно?"
    sl "Я понимаю, Мику."
    sl "Извини, сегодня я что-то не совсем быстро соображаю…"
    show mi smile pioneer with dspr
    "Мику улыбнулась."
    mi "Бывает!"
    mi "Полежи немного, и всё станет путём."
    mi "Я побегу дальше Лену искать, а ты, если увидишь её, не забудь передать!"
    sl "Хорошо."
    hide mi with dspr
    "Мику убежала в сторону столовой."
    th "Может быть, действительно пойти полежать?"
    "…"
    $ renpy.pause (0.5)
    with hpunch
    th "Бр-р, нет. Кто же тогда площадь подметёт?"
    th "Вдруг приедет кто, а здесь такой бардак?"
    "Славя нахмурила брови и принялась тщательно подметать три желтых листа, опавшие за ночь."
    stop music fadeout 2
    window hide
    $ renpy.pause (0.5)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg ext_square_day with dissolve
    play ambience ambience_camp_entrance_day
    window show
    $ renpy.pause (0.5)
    "Незаметно подошло время обеда."
    "Славя поставила метлу за памятник и отправилась к столовой."
    $ renpy.pause (0.5)
    scene bg ext_dining_hall_near_day with dissolve
    show pi2 smile with dissolve
    show un smile pioneer at left with dissolve
    $ renpy.pause (0.5)
    "Возле входа в столовую Славя увидела Семёна и Лену."
    "Они стояли рядом с лавочкой и о чём-то увлечённо разговаривали."
    "Лена улыбалась."
    "Славя подошла к ним."
    show pi2 smile with dspr
    show un normal pioneer at left with dspr
    me "О, привет, Славя!"
    me "А мы тут как раз о тебе вспоминали."
    sl "Вот как?"
    sl "Приятно слышать!"
    sl "Надеюсь, хорошее вспоминали?"
    show un smile2 pioneer with dspr
    "Лена хихикнула."
    un "Разумеется!"
    sl "Кстати. Лена, тебя Мику искала, знаешь?"
    show un surprise pioneer with dspr
    un "Мику?"
    un "А зачем?"
    sl "Ну…{w} Вот она сама тебе и расскажет."
    sl "Она ещё не пришла, значит?"
    show pi2 with dspr
    me "Нет, нигде не видно."
    show pi2 laugh with dspr
    me "А что такое? Мне тоже интересно!"
    sl "Ну обедать-то она придёт же, да?"
    me "Придёт, наверное, куда денется."
    play sound sfx_dinner_horn_processed
    "В этот момент раздался горн."
    $ renpy.pause (0.5)
    scene bg int_dining_hall_people_day with dissolve
    play ambience ambience_dining_hall_full
    $ renpy.pause (0.5)
    "Лена предложила занять стол, пока Семён и Славя берут еду."
    hide un with dspr
    "Славя кивнула и потянула Семёна за рукав."
    sl "Ну пошли, чего стоишь?"
    show pi2 with dspr
    "Взгляд Семёна был обращен ко входу."
    me "Интересно, Вебер придёт?"
    me "Или, быть может, Мику?"
    sl "Послушай, если мы будем так стоять, то еды не достанется!"
    sl "Пошли!"
    show pi2 upset with dspr
    "Семён неуверенно посмотрел на неё."
    me "Ну пошли…"
    hide pi2 with dspr
    $ renpy.pause (0.5)
    scene black with dissolve
    $ renpy.pause (0.2)
    scene bg int_dining_hall_people_day with dissolve
    $ renpy.pause (0.2)
    "Взяв еды, они направились к столику."
    "У Семёна в каждой руке было по подносу, и ещё он беспрестанно вертел головой в разные стороны, так что Славе приходилось поддерживать его за локоть."
    "Дойдя наконец до стола, Семён с облегчением поставил оба подноса и спросил у Лены:"
    show un normal pioneer with dspr
    show pi2 serious at left with dspr
    me "Лена, ты не заметила Мику или…{w} Мику, в общем, не было?"
    un "Нет."
    un "Хотя я не то что бы смотрела."
    me "Да? А что ты делала?"
    show un serious pioneer with dspr
    un "Мечтала."
    "— с серьёзным видом произнесла Лена.{w} Очень серьёзным."
    show pi2 surp with dspr
    me "Мечтала?.."
    un "Ну да. А что в этом такого?"
    show un smile3 pioneer with dspr
    un "Ты разве никогда не мечтаешь?"
    "Семён растерялся."
    me "Я…{w} Ну…"
    th "Я и раньше думала, что Лена немного странная. {w}Но не до такой же степени?"
    th "Видимо, люди вокруг, да и жизнь вообще, никогда не перестанут меня удивлять."
    dv "Эй, народ! У вас не занято?"
    show dv smile pioneer at right with moveinright
    $ renpy.pause (0.5)
    th "Алиса!{w} Словно из ниоткуда."
    "Славя хотела предложить ей сесть, но…"
    show pi2 serious with dspr
    show un serious pioneer with dspr
    me "Извини, у нас мест нет."
    dv "Как это нет? Стула тут четыре, а вас трое."
    dv "Кстати, знаете, как втроем на один стул сесть?"
    dv "Не знаете?"
    show dv grin pioneer with dspr
    dv "Нужно всего лишь его перевернуть…"
    me "Очень смешно, Алиса, но мест действительно нет."
    me "Человек должен прийти, вот мы и караулим."
    dv "Да? И что же за человек?"
    show un angry pioneer with dspr
    un "Слушай, а какое тебе дело?"
    "— внезапно громко произнесла Лена."
    stop ambience fadeout 2
    show pi2 surp with dspr
    show dv surprise pioneer with dspr
    "Все резко посмотрели на неё."
    "На лице Лены читалось раздражение, совсем для неё не свойственное."
    show dv scared pioneer with dspr
    dv "Да никакого…{w} В общем…"
    hide dv with moveoutright
    "— смущённо пробормотала Алиса и отошла."
    show un normal pioneer with dspr
    "За столом повисла неловкая пауза."
    show pi2 ser with dspr
    me "Лена…"
    me "Зачем же так грубо?"
    show un serious pioneer with dspr
    un "Грубо?"
    un "Чего же грубого?"
    show pi2 surp with dspr
    me "Ну…"
    "Семён запнулся. Лена выжидающе смотрела на него."
    sl "Семён имеет в виду, что это было несколько неожиданно с твоей стороны."
    show pi2 with dspr
    me "Да.{w} Ты обычно так себя не ведёшь."
    show un smile3 pioneer with dspr
    un "А как же я себя веду?"
    "Славя вздохнула и махнула на это рукой."
    "День становился всё интереснее и интереснее."
    window hide
    $ renpy.pause (0.5)
    stop ambience fadeout 2
    scene black with dissolve
    hide un
    hide pi2
    $ renpy.pause (0.5)
    scene bg ext_dining_hall_near_day with dissolve
    window show
    $ renpy.pause (0.5)
    play ambience ambience_camp_entrance_day

    "Покончив с едой, Славя и Семён вышли из столовой."
    show pi2 with dissolve
    me "Они так и не появились… И это странно."
    me "По крайней мере, для Мику."
    "Семён остановился и посмотрел на Славю."
    show pi2 ser with dspr
    me "Ну колись, что такое Мику хотела Лене передать?"
    me "Что-то важное, да?"
    "Славя взглянула на Семёна."
    th "Боже, он такой серьёзный…"
    $ renpy.pause (0.3,hard=True)
    with vpunch
    $ renpy.pause (0.3,hard=True)
    show pi2 surp with dspr
    me "Эй, чего смеёшься?"
    sl "Ничего-ничего, извини!"
    sl "Мику просто видела твоего друга и подумала, что они с Леной были бы превосходной парой!"
    sl "Нет, ну ты представь!"
    sl "Они же часами молчать будут!"
    show pi2 ser with dspr
    "Славя рассмеялась."
    "Подняв глаза на Семёна, она увидела, что тот даже не улыбнулся."
    me "Ты его совсем не знаешь."
    me "И Лену, скорее всего, тоже."
    me "Славя, я не думаю, что это здравая идея — судить людей лишь по поверхности, которую они показывают миру."
    me "У каждого своя история."
    me "У каждого свои тайны."
    me "Я считаю, стоит узнать человека получше, прежде чем делать какие-то поспешные выводы."
    me "Разве не так?"
    "Славя опустила глаза и вдруг вспомнила, что ещё совсем недавно, на площади, думала о том же самом."
    th "Неужели мы мыслим одинаково?.."
    sl "Хорошо, извини."
    sl "Но неужто у Вебера есть другая сторона кроме той, что он показал сегодня утром?"
    show pi2 with dspr
    me "Есть…{w} И не одна."
    sl "А разве так бывает?"
    show pi2 sad with dspr
    me "Видимо, бывает."
    "Семён вздохнул. Судя по всему, для него это было не совсем лёгкой темой."
    me "Это нелегко объяснить. Я…"
    el "Ребята!"
    show el surprise pioneer at right with dissolve
    "Откуда ни возьмись появился Электроник."
    "Вид у него был крайне взволнованный."
    me "Серёжа? Что случилось?"
    show pi2 smile with dspr
    me "«Deutsche Welle» поймал?"
    show el smile pioneer with dspr
    el "Не. Недооцениваешь ты меня, Семён!"
    el "Ещё вчера вечером поймал! Правда, ничего кроме «Дойчланд» не смог разобрать…"
    show el shocked pioneer with dspr
    "Вдруг Электроник снова заволновался."
    el "Да не в этом дело вообще!"
    el "В общем, хотел сегодня добавить небольшую модификацию, чтобы ловить ещё дальше, но ПРЕДСТАВЬ СЕБЕ!"
    el "Магнитофона нигде нет!{w} Его украли!"
    show pi2 ser with dspr
    me "Украли? Может, Шурик забрал?"
    show el normal pioneer with dspr
    el "Да зачем он ему?"
    el "Понятное дело, что он контрреволюция и всё такое, но {i}мне{/i} так вредить он бы точно не стал."
    el "Нет, это явно не он."
    show el serious pioneer with dspr
    el "В общем, если вам на глаза случайно попадется старый красный магнитофон, то сообщите мне, ладно?"
    el "И вообще…"
    play music neboslavyan fadein 2
    $ volume(0.6, "music")
    "Его слова внезапно прервала громкая музыка откуда-то сбоку."
    th "Со сцены?"
    show pi2 surp with dspr
    show el surprise pioneer with dspr
    el "Это что?"
    me "Кто-то демонов призывает?"
    sl "Скорее всего, Алиса снова песни своих диссидентов ставит."
    sl "Вовсю пользуется, что Ольги Дмитриевны нет."
    sl "Хотя…{w} Может быть, ты и не далёк от истины."
    show pi2 smile with dspr
    me "Я бы тоже отрывался, если бы…"
    show pi2 upset with dspr
    "Славя вспомнила о записках."
    th "Да уж…{w} Ему наверняка куда сложнее, чем мне.{w} Чем всем нам."
    th "Носить их постоянно с собой, думать о них…"
    show el serious pioneer with dspr
    el "Точно!{w} Алиса!"
    el "Как же я сразу про неё не подумал?!"
    el "Кому ещё может понадобиться этот старый ящик?"
    el "Я побежал!"
    hide el with dspr
    "Электроник унёсся вдаль."
    "Семён смотрел себе под ноги с тем же выражением лица и о чём-то думал."
    th "Надо его чем-то отвлечь."
    sl "Семён, не хочешь прогуляться?"
    show pi2 with dspr
    me "Прогуляться?{w} Куда?"
    sl "Ну… Да хоть в лес вон."
    show pi2 surp with dspr
    me "В лес?"
    show pi2 with dspr
    "Семён будто что-то прикинул в голове и кивнул."
    me "Ну пошли."
    me "Всё одно лучше, чем вот это вот слушать."
    stop music fadeout 2
    window hide
    $ renpy.pause (0.5)
    scene black with dissolve
    hide pi2
    $ renpy.pause (1,hard=True)
    scene bg ext_path_day with dissolve
    window show
    $ renpy.pause (0.5)
    "По мере удаления от столовой музыка становилась тише."
    "Наконец исчезла вовсе."
    show pi2 ser with dspr
    "Славя время от времени посматривала на Семёна, думая, как начать разговор."
    "Он же казался погружённым в мысли и словно не замечал её присутствия рядом."
    th "Ну и ладно."
    th "Сам заговорит, когда захочет."
    hide pi2 with dspr
    "Тропинка, по которой они шли, была знакома Славе вдоль и поперёк."
    "Если идти всё время прямо, выйдешь к опушке, с которой открывается замечательный вид на небольшое лесное озеро."
    th "Собственное тайное место…"
    "В том, что оно тайное, Славя была уверена на сто процентов."
    "Иначе там бы каждый вечер кто-нибудь сидел и смотрел на звёзды."
    "Идеальное место для свиданий."
    "С одной стороны, спрятанное от всех, с другой — совершенно открытое."
    "А как там красиво ночью при свете луны…{w} Вид поистине живописный."
    th "А может…"
    th "Хотя нет. Какое свидание при нынешних обстоятельствах?"
    th "Ну а вообще…{w} Он бы согласился?"
    "Славя осторожно взглянула на Семёна."
    show pi2 with dspr
    "Его взгляд был направлен прямо на неё."
    sl "Ай!" with hpunch
    "Славя споткнулась и наверняка бы упала, не подхвати Семён её за руку."
    show pi2 surp with dspr
    me "Эй, ты чего?"
    me "Всё в порядке?"
    sl "Да. Отвлеклась немного."
    show pi2 smile with dspr
    "Семён улыбнулся."
    me "Тоже любишь помечтать?"
    me "Не думал, что вы с Леной так похожи."
    "Славя почувствовала, что краснеет."
    sl "И вовсе не похожи!"
    sl "Лена вообще какая-то стран…{w} О, смотри, что это?"
    show stain with dspr
    "Славя указала на небольшое тёмно-красное пятно чуть сбоку от тропинки."
    sl "Как это могло здесь появиться?"
    show pi2 with dspr
    me "Хм…"
    me "Действительно, пятно какое-то…"
    show pi2 ser with dspr
    me "Ох, только не говори мне, что это кровь!"
    hide pi2 with dspr
    scene bg stained_close with dissolve
    "Славя наклонилась и потёрла пальцем странную субстанцию."
    me "Ты что? А вдруг это сибирская язва?"
    sl "Перестань, мы не в Сибири."
    sl "Да и не кровь это."
    sl "Больше похоже на краску."
    scene bg rocambole with dissolve
    show pi2 ser with dspr
    me "Краску?"
    me "Откуда ей взяться в лесу?"
    show pi2 smile with dspr
    me "Или у вас тут как в армии — перед приездом начальства красят траву?"
    show pi2 laugh with dspr
    me "Хотя что-то мне подсказывает, что трава явно не красного цвета…"
    th "Ох уж эти его шутки…"
    th "Ну ладно, у него хотя бы настроение получше стало."
    me "Пойдём дальше?"
    sl "Пойдём."
    window hide
    $ renpy.pause (1)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg ext_path_day with dissolve
    window show
    $ renpy.pause (0.4,hard=True)
    sl "Чем вообще планируешь заниматься?"
    show pi2 with dspr
    me "Сейчас? Гулять."
    me "А что, есть иные предложения?"
    sl "Да нет, просто интересуюсь. А после ужина что делать будешь?"
    me "После ужина мы договорились встретиться с Вебером и обсудить, что делать дальше."
    sl "А вы виделись?"
    me "Да, я всё-таки навестил его в библиотеке."
    show pi2 smile with dspr
    me "Видела бы ты его лицо, когда я вошёл!"
    sl "Да уж, подозреваю, что он {i}очень{/i} был рад тебя видеть."
    sl "А вообще, что мы собираемся дальше делать?{w} Есть у нас стратегический план?"
    show pi2 ser with dspr
    "Семён стал серьёзным."
    me "Господина Шутника найти надо обязательно. Это даже не обсуждается."
    me "Однако вот тебе вопрос: как мы собираемся это сделать?"
    me "Ответ очевиден: будем дожидаться его следующего выхода. Карл более чем уверен, что наш «друг» не заставит себя долго ждать."
    me "До конца смены остаётся совсем немного, а у него ещё «восхождение» или что там…"
    sl "И всё? Мы будем просто ждать, когда он доберётся до следующей жертвы?"
    me "А с чего ты решила, что это будет жертва?"
    me "Что мешает ему дальше развлекаться при помощи записок?"
    sl "Ну… Разве нет возможности нанести упреждающий удар?"
    me "Упреждающий удар, говоришь…"
    hide pi2 with dspr
    $ renpy.pause (1,hard=True)
    show pi2 ser with dspr
    "Семён оглянулся, словно проверяя, может ли их кто-то подслушать."
    me "Были у меня по этому поводу мысли."
    me "Но мысли не совсем приятные, знаешь ли."
    me "Уверена, что хочешь их услышать?"
    sl "Учитывая твои прошлые подозрения, что маньяк — это я, хуже уже быть точно не может."
    show pi2 ser close with dspr
    "Семён придвинулся к ней поближе и тихо заговорил:"
    me "Тебе не показалось странным поведение Лены в последнее время?"
    me "Мне довелось пересечься с ней несколько раз за последние дни, и знаешь, что я заметил?"
    me "Каждый раз она какая-то…{w} другая."
    me "Каждый раз, разговаривая с ней, мне казалось, что я общаюсь с совершенно разными людьми."
    me "Когда я встретил её в лесу вечером, она вела себя как обычно.{w} Как та Лена, которую мы все знаем."
    me "Когда на следующий день я облил её водой, она словно с цепи сорвалась."
    me "Но через несколько часов после этого уверяла меня, что весь день читала книгу и вовсе не выходила из комнаты."
    me "И эта странная сцена в столовой…"
    "Славя остановилась и серьёзно посмотрела на Семёна."
    sl "Семён."
    me "Что?"
    sl "Это всё, конечно, очень занятно, но у меня к тебе один вопрос."
    me "Да, что такое?"
    sl "Зачем ты облил Лену водой?"
    show pi2 upset with dspr
    "Семён смутился и отодвинулся."
    me "А, это долгая история…"
    show pi2 ser with dspr
    me "Да и вообще, это не имеет значения!"
    me "Самое главное то, что Лена ведёт себя очень и очень странно!"
    me "Ты как вообще меня слушаешь?"
    sl "Да внимательно я тебя слушаю, успокойся!"
    sl "Просто твоя идея, что Лена — маньяк, немного…{w} непродуманная."
    sl "Сам посуди: как Лена могла дотащить Ольгу Дмитриевну до шезлонга?"
    sl "Твой Вебер-то еле её поднял, а тут худенькая Леночка."
    me "Во-первых, Вебер не мой."
    me "Во-вторых, я не говорил, что Лена и есть маньяк."
    me "Я хотел сказать, что между её странным поведением и странными вещами, которые творятся в лагере, можно провести очевидные странные параллели!"
    me "Может быть, она и не при делах, действительно."
    show pi2 smile with dspr
    me "А провалы в памяти — это так, небольшие психические отклонения."
    show pi2 ser with dspr
    me "А может, что она с кем-то заодно, и у них в планах превратить «Совёнок» в местный филиал Питерского мясокомбината."
    me "В конце концов, кто тебе сказал, что автор записок — одно лицо?"
    me "Чтобы провернуть такое в одиночку, нужно очень постараться…"
    hide pi2 with dspr
    scene bg ext_old_building_day_7dl with dissolve
    $ renpy.pause (0.5)
    "За разговором они незаметно дошли до старого лагеря."
    "Славя слышала про него много историй, в большинстве своём не очень приятных."
    "Будто бы раньше там действительно был лагерь, но во время войны немецкие доктора и учёные ставили на пленных какие-то ужасные опыты…"
    th "Бр-р…"
    "Хотя в энциклопедии пишут, что именно войнам мы обязаны целым рядом важных открытий в области хирургии и психологии."
    "Недаром эта энциклопедия запрещена к прочтению Коммунистической партией."
    "Но в деревне ещё и не такое можно найти. Там проверять некому."
    show pi2 ser with dspr
    "Семён, видимо, тоже слышал пару неприятных историй об этом месте."
    "Он остановился и как-то ощутимо занервничал."
    th "Неужто испугался?"
    me "Хм…{w} Может, обратно пойдём?"
    me "Здесь как-то неуютно."
    hide pi2 with dspr
    "Славя бросила взгляд на старое, покосившееся здание."
    th "Странное чувство."
    play music heartbeat fadein 4
    th "Будто за нами кто-то наблюдает."
    th "Но кому здесь быть?"
    stop ambience fadeout 2
    th "Здесь же никто не бывает…{w} По крайней мере, из «Совёнка»."
    "Истории о войне и немецких докторах стали всё отчётливее проступать в памяти Слави."
    "Здесь ставили опыты на головном мозге…"
    "Например, сколько сможет прожить человек, если поражать током определённые участки его серого вещества."
    th "Если это можно назвать жизнью…"
    show redscreen with dissolve
    "Это странное чувство не отпускало Славю. Оно словно становилось всё сильнее с каждой секундой."
    th "Неудивительно, что лагерь решили перенести…{w} Но почему так близко?"
    th "Почему вообще не снести это страшное место и не уничтожить все воспоминания о нём?"
    th "Зачем оно нужно, даже если сама атмосфера рядом с ним настолько гнетёт и давит на сознание?"
    th "И мы ведь ещё даже не внутри…"
    show redscreen2 with dissolve
    play sound sob_sl fadein 2
    $ volume(0.7, "sound")
    "Славя почувствовала, что начинает задыхаться."
    show pi2 shocked with dspr
    me "Славя?.."
    me "Славя, ты в порядке?"
    th "Семён… Он тоже это чувствует?"
    sl "Пошли быстрее…"
    "Славя взяла Семёна за руку, и они едва не бегом бросились прочь от проклятого места."
    stop sound fadeout 2
    hide pi with dspr
    stop music fadeout 3
    stop ambience fadeout 2
    window hide
    scene black with dissolve
    hide redscreen
    hide redscreen2

    $ renpy.pause (0.5)
    scene bg ext_dining_hall_near_day with dissolve
    play ambience ambience_camp_center_day fadein 2
    $ volume(0.8,'ambience')
    window show
    $ renpy.pause (0.5)
    "Только вернувшись обратно к столовой, Славя вновь почувствовала себя в безопасности."
    th "Нет, со старым лагерем точно что-то не так."
    th "Ольга Дмитриевна наверняка что-то знает, но у неё ведь не спросишь…"
    show pi2 ser with dissolve
    "Семён стоял рядом и взволнованно смотрел на неё."
    me "Это что такое было?{w} Ты едва не упала там на месте!"
    sl "Я что-то почувствовала…"
    sl "Опасность."
    show pi2 surp with dspr
    me "Опасность?"
    me "Согласен, место старое и довольно неприятное, но опасность…"
    show pi2 ser with dspr
    me "На тебе лица нет. Тебе явно стоит прилечь."
    "Семён машинально дотронулся до её щеки."
    "Но через мгновение вздрогнул и тут же отдёрнул руку."
    sl "Да, наверное."
    sl "Ты как, справишься тут без меня до ужина?"
    show pi2 with dspr
    me "Конечно."
    me "Поищу Карла, спрошу, есть ли у него новости."
    "Короткое имя резануло слух Слави, словно острый нож."
    th "Почему, почему он так часто говорит о нём?"
    th "Кто он такой, чтобы так сильно занимать внимание Семёна?"
    sl "Хорошо…{w} Береги себя."
    show pi2 smile with dspr
    me "Ты тоже."
    me "Увидимся на ужине!"
    hide pi2 with dspr
    "Семён махнул рукой и ушёл. Славя смотрела ему вслед."
    th "Ну ничего…{w} Мы же совсем скоро снова с ним увидимся, да?"
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    play ambience ambience_camp_entrance_day fadein 2
    scene bg ext_square_day with dissolve
    "Славя направилась к своему домику самым коротким путём — через площадь."
    "Вокруг не было ни души, что было довольно непривычно."
    "Свободные от бесконечных заданий главной вожатой, пионеры предпочитали проводить большую часть времени в домиках."
    "Кто-то читал, кто-то рисовал. Некоторые вообще спали."
    th "И какой в этом интерес?"
    th "Будто дома нельзя почитать, в самом деле."
    th "Ехать за столько километров и спать полдня…{w} Некоторых людей просто невозможно понять."
    th "Надо пользоваться свободным временем, пока оно есть."
    th "За школой последует институт, а за ним — работа. Когда ещё удастся провести неделю-другую в таком замечательном месте?"
    play sound inhale fadein 1
    $ volume(0.6, "sound")
    "Славя сделала глубокий вдох."
    th "Вот бы можно было вовсе отсюда не уезжать…"
    uv_voice "Пс-с!"
    with hpunch
    sl "A?"
    sl "Кто здесь?"
    show uv smile with dissolve
    play music otl_lolor fadein 2
    $ volume(0.6,'ambience')
    uv "Всего лишь я."
    sl "Юля!"
    "Славя огляделась."
    sl "Ты что? Вдруг кто-нибудь увидит?"
    show uv normal with dspr
    uv "Да перестань."
    uv "Нет же никого."
    sl "Нет, так всё равно не пойдёт."
    "Славя взяла Юлю за руку и потянула в сторону."
    $ renpy.pause (0.5,hard=True)
    scene bg ext_houses_day with dspr
    $ renpy.pause (0.5,hard=True)
    show uv normal with dspr
    sl "Что такое? Что-то срочное?"
    uv "Нет."
    uv "Просто захотелось увидеться."
    show uv smile with dspr
    "Юля двинула ушками и смущённо продолжила."
    uv "Видела вас с Семёном около старого лагеря."
    uv "Вы чего-то испугались?"
    sl "Да, мне почему-то стало…{w} страшно."
    uv "Почему? Просто от старого лагеря?"
    sl "Не знаю. Наверное."
    sl "А что с ним такое?"
    uv "Не могу сказать."
    sl "Вечно ты так…"
    sl "Всё знаешь, а сказать не можешь."
    uv "Таковы правила. Не я ведь их создавала."
    sl "Думаешь, нам действительно грозила опасность?"
    show uv normal with dspr
    uv "Ну…{w} Нет, вряд ли."
    uv "Во всяком случае, напрямую вам ничего не угрожало."
    show uv smile with dspr
    uv "Просто вы подошли слишком близко к тому, что знать вам пока рано."
    sl "Это как-то связано с Ольгой Дмитриевной?"
    uv "Здесь всё со всем связано."
    show uv upset with dspr
    uv "Не поверишь, но даже я иногда не могу понять, что к чему."
    sl "Действительно не поверю."
    sl "Юля…{w} Какие-нибудь подсказки будут?"
    show uv smile with dspr
    uv "Ты до сих пор называешь это подсказками?"
    uv "И главное — от кого?"
    uv "От духа?"
    uv "Вдруг меня вообще не существует, а ты выдумала себе воображаемого друга?"
    sl "Было бы очень обидно…"
    uv "И всё?"
    sl "…что два мешка сахара ушли в никуда…"
    show uv rage with dspr
    uv "!!!"
    "Славя рассмеялась."
    sl "Ладно, не злись. Я же шучу."
    show uv upset with dspr
    uv "Не смешно, знаешь!"
    uv "С чем с чем, а с сахаром точно не шутят."
    uv "Мне явно стоит проводить больше времени со смертными, чтобы понять их {i}юмор{/i}…"
    sl "Тебе стоит познакомиться с Семёном."
    sl "Он хороший парень.{w} И шутит просто отменно."
    show uv smile with dspr
    uv "Да уж."
    show uv normal with dspr
    uv "В прошлый раз его очень позабавили мои уши…"
    sl "В прошлый раз?"
    show uv smile with dspr
    uv "Даже не пытайся говорить про меня — он всё равно не вспомнит."
    uv "Но я его помню.{w} И очень хорошо."
    uv "Ещё бы я его не помнила…"
    sl "Как так?"
    uv "А что до подсказок — не волнуйся насчёт него.{w} Семён сильнее, чем кажется."
    sl "Да я и не волнуюсь…"
    show uv laugh with dspr
    "Юля широко улыбнулась."
    uv "Правда?"
    $ renpy.pause (1,hard=True)
    sl "Ну ладно…"
    sl "Немного, может быть."
    show uv smile with dspr
    uv "Старайся проводить с ним как можно больше времени.{w} Кто знает, когда придёт пора расставания?"
    sl "Ты знаешь."
    uv "…к тому же ты хорошо на него влияешь.{w} Успокаивающе."
    sl "В самом деле?"
    sl "А Вебер, наоборот, посоветовал мне держаться от него подальше."
    sl "Знаешь его? Это друг Семёна, как оказалось."
    show uv normal with dspr
    uv "Знаю. Видела со стороны."
    sl "Кто он такой вообще?"
    uv "Не последний человек в этой истории. Это всё, что я могу сказать."
    sl "Но он такой…{w} странный."
    show uv smile with dspr
    uv "А кто из нас не странный?"
    show bg ext_house_of_sl_day with dissolve
    show uv smile with dspr
    sl "Зайдешь? Есть хороший чёрный чай."
    sl "С сахаром."
    show uv laugh with dspr
    uv "Соблазн велик, но я удержусь."
    uv "Спасибо."
    show uv smile with dspr
    uv "В общем, с тобой всё в порядке.{w} Я рада."
    sl "А могло быть и не в порядке?"
    uv "Ну…{w} В любом случае, мне запрещено вмешиваться, ты же знаешь."
    uv "Свобода выбора всегда за человеком."
    sl "И поэтому ты не скажешь, кто на самом деле этот маньяк…"
    uv "Не скажу."
    uv "Вы должны сами во всём разобраться."
    sl "Какой в этом смысл, если могут погибнуть люди?"
    show uv normal with dspr
    uv "А что, уже кто-то погиб?"
    sl "Нет, но…"
    uv "Тогда к чему загадывать плохое?"
    show uv smile with dspr
    uv "В общем, будь внимательна."
    uv "Ещё ничего не определено."
    uv "Всё может поменяться."
    uv "До скорого!"
    hide uv with dissolve
    $ renpy.pause (1,hard=True)
    "Юля махнула рукой и исчезла."
    th "Интересно, она может хоть раз сказать что-нибудь без загадок?"
    th "И ведь не расскажешь никому про неё…"
    th "Удивительное существо."
    "Славя стояла и смотрела на свой домик."
    th "Вот зачем, по сути, она приходила сейчас?"
    th "Проверила, всё ли со мной в порядке."
    th "Сказала, чтобы я была настороже."
    th "И всё."
    th "Какой в этом смысл?"
    th "Она знает всё.{w} Она и есть хозяин этого лагеря."
    th "Ничто не может скрыться от её внимания: листок упадёт с дерева раньше времени — она будет знать почему."
    th "Разумеется, она знает, кто стоит за всеми делами, которые сейчас здесь творятся."
    th "Но она не может сказать, потому что это «нарушило бы баланс»."
    th "Но к чему нужен этот «баланс», если жизни людей висят на волоске?"
    th "…"
    $ renpy.pause (1.5,hard=True)
    th "Может быть, когда-нибудь я пойму."
    th "Но точно не сегодня."
    stop music fadeout 2
    stop ambience fadeout 2
    scene black with dissolve
    $ sunset_time()
    $ persistent.sprite_time = 'sunset'
    $ renpy.pause (3,hard=True)
    show blink
    hide blink
    play sound sfx_dinner_horn_processed
    $ volume(0.25, "sound")
    $ renpy.pause (3,hard=True)
    show unblink
    scene bg int_house_of_sl_sunset with dissolve
    window show
    "Славя вздрогнула и открыла глаза."
    th "Уже закат…{w} Неужто я проспала так долго?"
    th "Как же не хочется никуда идти…"
    play music fineoutside fadein 2
    "Потянувшись, Славя окончательно проснулась и поняла, что вставать с кровати у неё совершенно нет желания."
    th "Ничего ведь не случится, если я разок пропущу ужин? Да и есть-то не особо хочется."
    show blink
    "Зевнув, она перевернулась на бок, закрыла глаза и ещё глубже укуталась в одеяло."
    th "Вот бы заснуть и проснуться, когда всё уже закончится."
    th "Когда Ольга Дмитриевна будет с нами…{w} Семён снова станет весёлым…{w} и научится шутить…"
    th "Наверное, об этом можно только мечтать."
    th "Мечтать…"
    th "Неужели Лена всё-таки чихнула на меня?!"
    $ renpy.pause (0.5)
    play sound sfx_knock_door3_dull
    $ volume(1.4, "sound")
    stop music fadeout 2
    $ renpy.pause (0.5)
    "Вдруг в дверь постучали."
    "Славя вздрогнула."
    th "Я сплю.{w} И ничего не слышу."
    play sound sfx_knock_door3_dull
    "Постучали ещё раз."
    th "Нет-нет. Здесь никого нет."
    $ renpy.pause (0.5)
    me "Славя! Ты здесь?"
    me "Славя!"
    show unblink
    scene bg int_house_of_sl_sunset with dissolve
    play music fineoutside2 fadein 3
    th "Это Семён!"
    sl "Да-да, заходи!"
    "И что это он здесь делает? Не положено ли ему сейчас быть на ужине?"
    show pi2 smile with dissolve
    "Семён вошёл."
    "В руках он держал поднос с двумя тарелками: на одной лежали булочки, на второй — котлеты."
    show pi2 ser with dspr
    me "Куда бы это поставить…"
    "— замялся Семён, видя, что на столе нет места."
    sl "Прямо сюда ставь!"
    "— указала Славя рядом с собой."
    "Она всё ещё лежала в кровати, укрытая одеялом с пяток до подбородка."
    show pi2 surp with dspr
    "Семён захлопал глазами и, кажется, немного покраснел."
    show pi2 smile with dspr
    me "Тебе лучше?"
    me "Всё-таки правильно подумал, что ты на ужин не придёшь."
    th "Он такой заботливый. {w}И ответственный."
    "Славя ощутила, что тоже краснеет."
    sl "Спасибо. Это очень мило с твоей стороны."
    me "Да было бы за что."
    show pi2 ser with dspr
    me "Давай не будем больше ходить к старому лагерю."
    me "Там какая-то энергетика странная…{w} Ты тоже это почувствовала, да?"
    "Славя кивнула."
    th "Да кому вообще может прийти в голову специально туда ходить?"
    th "И всё же…"
    th "Не могу отделаться от чувства, что за нами кто-то наблюдал."
    th "Это было очень странно."
    show pi2 smile with dspr
    me "Ну ладно, не буду тебе мешать."
    me "Набирайся сил."
    me "Завтра будет интересный день."
    sl "Интересный день?"
    "Семён улыбнулся."
    me "Ну да. Разве не все дни здесь интересные?"
    sl "Наверное, так.{w} Даже, пожалуй, слишком интересные."
    sl "И ты мне не мешаешь, честно!"
    "Но Семён уже поднялся."
    me "Встретимся завтра утром, хорошо?"
    me "Запирай дверь на ночь."
    sl "Хорошо."
    sl "Хотя… Хотела бы я посмотреть на храбреца, который осмелится ворваться в комнату, где спит Женя…"
    show pi2 laugh with dspr
    "Семён расхохотался."
    "Славя посмотрела на Семёна и внезапно для самой себя проговорила:"
    sl "Семён…{w} Подойди сюда."
    show pi2 surp with dspr
    me "Зачем?"
    "— спросил Семён с недоумением, но подошёл."
    "Славя приподнялась на локтях и дотронулась до его щеки."
    sl "Спасибо тебе."
    sl "Ты единственный в лагере так заботишься обо мне…"
    "Семён вздрогнул."
    me "Да что ты…{w} Пустяки."
    show pi2 smile with dspr
    me "Ну всё, до завтра!"
    hide pi2 with dissolve
    "Семён ободряюще улыбнулся и вышел."
    th "Мне столько нужно ему сказать…{w} Надеюсь, шанс когда-нибудь представится."
    stop music fadeout 3
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ night_time()
    $ persistent.sprite_time = "night"
    $ renpy.pause (2,hard=True)
    scene bg ext_house_of_sl_night with dissolve
    play ambience ambience_camp_entrance_night fadein 2
    window show
    $ renpy.pause (1,hard=True)
    "На лагерь постепенно навалился вечер."
    play sound sfx_wind_gust
    $ volume(0.6, "sound")
    "Подул прохладный ветер. Славя поёжилась."
    "Хотя после жаркого дня это самый лучший способ освежиться."
    th "А в лесу, наверное, совсем хорошо…"
    "Славя понимала, что гулять так поздно одной в лесу может быть опасно."
    "Понимала, что неизвестно, кто может быть там в такое время."
    "Но в глубине души она знала, что всё это не может остановить её."
    th "И что теперь, за дверь носа не казать?"
    th "Глупость. Нельзя постоянно жить в страхе."
    th "В лесу никого нет.{w} Наверное."
    th "Главное — не приближаться к старому лагерю."
    play sound inhale fadein 1
    "Славя сделала глубокий вдох, пытаясь загнать все страхи куда подальше."
    "Выдох."
    th "Не вечно же прятаться."
    $ renpy.pause (1,hard=True)
    scene bg ext_path_night with dissolve2
    $ renpy.pause (0.5)
    "Славя немного побродила и вышла на тропинку, по которой они днём гуляли с Семёном."
    th "Поляна уже должна быть где-то рядом."
    "Больше всего Славя хотела лечь на траву и смотреть на звёзды."
    "Чтобы лёгкий ветер шевелил листья на деревьях и приносил с собой прохладу."
    "Чтобы всё было как раньше."
    "Чтобы в этом мире не было зла и страха, а были только любовь и доброта."
    "Чтобы сон уступил место реальности."
    scene bg ext_polyana_night with dissolve
    $ renpy.pause (0.5)
    "Славя дошла до опушки, как вдруг её внимание привлекло какое-то движение впереди."
    show man_sitting with dspr
    play music heartbeat fadein 2
    $ volume(0.4,'ambience')
    th "Человек?{w} Не может быть!"
    "Славя мгновенно спряталась за дерево."
    th "Неужто…{w} Неужто это он?"
    th "Маньяк?.."
    "Сердце стучало так быстро, что, казалось, ещё чуть-чуть — и оно вырвется из груди."
    "Неизвестный сидел спиной к Славе."
    th "Он пока меня не заметил.{w} Может, получится…"
    waeber_unknown "Хм."
    waeber_unknown "И долго вы ещё там будете стоять?"
    waeber_unknown "Не бойтесь, подойдите."
    "Голос показался Славе странно знакомым."
    th "Ну что ж, если уж он меня заметил…"
    th "Прятаться больше нет смысла."
    "Славя выдохнула, сжала кулаки и двинулась вперёд."
    "Подойдя, она увидела, что сидящий человек — не кто иной, как…"
    stop music fadeout 2
    $ volume(1.0,'ambience')
    hide man_sitting
    show kw with dissolve
    "Карл Иванович Вебер, «коллега» Семёна."
    "Сам Вебер не выглядел хоть сколько-нибудь удивлённым."
    kw "Добрый вечер, Славя."
    kw "Перестаньте хлопать глазами. Кроме меня, здесь больше никого нет."
    th "Этого-то я и боюсь!"
    show kw ser with dspr
    kw "Вы удивлены? Думали увидеть здесь кого-то другого?"
    sl "Нет… Я вообще не думала здесь никого увидеть."
    show kw thinking with dspr
    kw "В самом деле? Тогда зачем же вы здесь?"
    sl "Просто так. Я гуляла."
    th "А почему я вообще отвечаю на его вопросы?"
    th "Разве его поведение менее странно?"
    "Славя взглянула на небольшую стопку книг, лежащую рядом с ним."
    th "Кто будет сидеть ночью в лесу со свечой и читать?"
    sl "А вы сами-то что здесь делаете? В комнате пропало электричество?"
    show kw disapp with dspr
    "Карл скривился."
    kw "Нет. Дело гораздо серьёзнее."
    kw "Я прячусь."
    "Подобного ответа Славя не ожидала."
    sl "Прячетесь? От кого?"
    show kw with dspr
    kw "От Семёна."
    kw "Он обещал вечером зайти…"
    sl "И что?"
    show kw thinking with dspr
    kw "Ну нет уж, благодарю покорно! Мне его общества с лихвой хватает и днём."
    kw "Надеюсь, что он сейчас придёт, подумает, что я уже сплю, и уйдёт."
    kw "А я смогу вернуться и продолжить свои «литературные изыскания» в чуть более удобной обстановке."
    sl "Но он был так рад вас видеть…{w} Я никогда не замечала его в таком настроении."
    sl "Он неделю почти ни с кем не говорил, а как вы появились…"
    show kw sad with dspr
    "Вебер вздохнул и прикрыл глаза."
    show kw with dspr
    kw "Послушай, как ты смотришь на то, чтобы перейти на ты?"
    kw "Честно говоря, все эти церемонии и официальность страшно надоели ещё на работе."
    kw "Раз уж мы застряли здесь, можно и отдохнуть немного…"
    "Славя кивнула."
    sl "Хорошо. Так действительно будет лучше."
    sl "Тем более судя по всему, ты ненамного меня старше."
    "Карл мельком оглядел Славю и пробормотал:"
    kw "Думаю, лет пять всё-таки будет."
    kw "Ну да не важно."
    kw "К сожалению, ни рюмок, ни вина у меня с собой нет, так что на брудершафт выпить не удастся."
    kw "Но так тоже сойдёт."
    "Карл протянул ей свою руку."
    "Славя чуть подумала и пожала её."
    show kw smile with dspr
    play music quiet_night fadein 2
    $ volume(0.6,'ambience')
    "Она была очень тёплой… Что удивительно для такого холодного человека, каким Славя его себе представляла."
    "Но сейчас Вебер смотрел прямо на неё, и в его глазах не было ни холода, ни тени."
    sl "Карл… Какое странное имя…"
    "— произнесла Славя, садясь рядом."
    show kw with dspr
    kw "Странное? Ну, может быть, для Советского Союза и да."
    sl "А ты разве не отсюда?"
    show kw smile with dspr
    kw "И да, и нет."
    kw "Я из России, правда, немного из другой, новой России. Но да ладно, разница небольшая."
    kw "А насчёт имени: у нас в роду всем дают немецкие имена, даже если ребёнок рождается не в Германии. Такая уж традиция."
    sl "А почему?"
    show kw with dspr
    kw "Ну, как ты уже могла догадаться по фамилии, у меня немецкие корни."
    sl "В самом деле? Ты говоришь на русском как на родном."
    show kw smile with dspr
    kw "Он и есть для меня родной."
    kw "Закончив университет, мой отец женился и уехал работать в Россию, где я и родился…"
    kw "Где, в принципе, и жил, и учился, и работал до недавнего времени."
    kw "Но потом родители уехали назад в Германию и предоставили меня собственной судьбе.{w} А судьба, соответственно, закинула меня в Поднебесную."
    show kw with dspr
    kw "Куда через несколько лет закинула и Семёна."
    show kw disapp with dspr
    kw "Странная эта штука, судьба…{w} Связывает людей."
    "Славя слушала и удивлялась."
    th "Немец, живущий в Союзе?"
    th "И Семён…"
    th "Но как такое возможно?!"
    show kw smile_2_2 with dspr
    "Карл бросил взгляд на Славю и улыбнулся."
    kw "Да-да…{w} Небольшой разрыв шаблона?"
    kw "Информация одного мира наслаивается на информацию из другого, и в результате получается нечто не совсем понятное?"
    sl "Да…"
    show kw disapp with dspr
    "Вебер вздохнул."
    kw "Значит, мои догадки по поводу этого места были верны."
    kw "Только вот непонятно, как вернуться назад."
    sl "Куда вернуться?"
    show kw with dspr
    kw "Эх…{w} Я бы всё рассказал, но не могу."
    kw "Ты вряд ли поверишь, да и неизвестно, как отреагирует система…{w} Не хотелось бы экспериментировать."
    "Его слова ввели Славю в ещё больший ступор."
    sl "Хорошо…"
    sl "А что ты вообще тут делаешь?"
    show kw ser with dspr
    kw "Тут — это где?"
    kw "В лагере?{w} На поляне?{w} В этой вселенной?"
    sl "Предположим, на поляне."
    kw "Читаю."
    "Вебер указал рукой в сторону, где лежали книги."
    kw "Удивительное место."
    kw "Почему-то именно здесь мысли приобретают формы и направления, не свойственные им где-то ещё."
    kw "Плюс полное отсутствие людей…"
    "Карл покосился на Славю."
    show kw smile_2_2 with dspr
    kw "Ну, почти полное."
    sl "А что читаешь?"
    "Вместо ответа Карл потянулся и передал ей пару книг."
    "На первой значилось «Общая психология под ред. Махлакова»."
    "На второй — какой-то непонятный текст."
    show kw ser with dspr
    kw "«Die Leiden des jungen Werther». «Страдания юного Вертера»."
    kw "Давно хотел прочитать, да всё некогда было.{w} Надеюсь, воспоминания сохранятся."
    sl "Ого! Книга на немецком!"
    sl "Их у нас немного, и то не в свободном доступе. И как только Женя тебе её дала…"
    show kw smile with dspr
    kw "Порой всё, что нужно, — это дипломатично спросить. Такт и вежливость открывают любые двери."
    sl "А ты, значит, ещё и немецким владеешь? Здорово."
    "Вебер чуть покраснел."
    kw "Ну так… Немного."
    th "А он умеет смущаться?.."
    hide kw with dspr
    "Разговаривая с Вебером, Славя постепенно избавлялась от волнения."
    th "А ведь он не такой чёрствый и непробиваемо серьёзный, как показалось на первый взгляд…"
    th "Разве что немного непонятный."
    th "Действительно, иногда мы слишком поспешно формируем мнение о людях, которых толком не знаем."
    th "Первое впечатление, как выяснилось, не всегда верное…"
    show kw with dspr
    sl "А «Психология…»?"
    kw "Психология…"
    show kw ser with dspr
    stop music fadeout 2
    "Вебер в мгновение ока посерьёзнел. Температура вокруг словно упала на пару градусов."
    kw "Психология имеет непосредственное отношение к делу, которое сейчас стоит перед нами."
    kw "Хочу напомнить, что, несмотря на всю видимую иллюзорность происходящего, всё это совсем не шутка."
    kw "То, что мы с Семёном появились в этом лагере, значит, что от нас что-то зависит."
    kw "Видимо, именно нам придётся остановить этого…{w} злодея."
    kw "К сожалению, я не так силён в психологии, как мне бы хотелось. Однако одно могу сказать более-менее точно:"
    kw "Скорее всего, мы имеем дело с маниакально-компульсивным синдромом, перетекающим из первой фазы во вторую."
    "Славе стало не по себе."
    th "Вряд ли «маниакально-компульсивный синдром» может быть чем-то хорошим…"
    sl "Что это значит?"
    "Карл забрал у неё книгу и открыл где-то посередине."
    show kw thinking with dspr
    kw "Первая фаза — фаза становления личности.{w} Так называемая фаза «Восхождения»."
    show kw with dspr
    kw "Если сказать просто, то человек примеряет себе различные маски и решает, кем хочет стать."
    kw "В этот момент все изменения ещё обратимы — с индивидом нужно как следует поговорить и убедить, что единственная настоящая личность в его теле — это он сам."
    kw "Если сделать всё правильно, то последствия будут сравнимы с…{w} ну, лёгкой депрессией, например."
    sl "А если нет?"
    show kw ser with dspr
    kw "Если нет, то первая фаза постепенно переходит во вторую.{w} Фаза «Триумфа»."
    kw "Человек окончательно срастается со своей новой личностью."
    kw "Его восприятие изменяет то, как он видит реальность. Меняются и выводы, которые он делает."
    sl "Это как?"
    "— удивилась Славя."
    kw "Тебе приходилось слышать выражение, что каждый из нас смотрит на мир сквозь призму своих «линз»?"
    kw "Для того, кто {i}хочет{/i} видеть мир хорошим, всё вокруг полнится красками. Всё замечательно, и даже внезапно начавшийся дождь — не досадная прихоть природы, а повод погулять по лужам босиком."
    kw "Пессимистам же кажется, что мир вокруг тёмный, страшный и враждебный. Возможно, для них это так и есть."
    kw "Так вот, все эти записки — прямое подтверждение тому, что мировоззрение нашего злого гения уже не похоже на наше."
    kw "Мы находимся в одном мире, только он видит в нём знаки, неведомые нам. Пишет записки, в которых прячет какие-то намёки."
    kw "Для него они имеют смысл. Для нас — нет."
    kw "Просто потому, что его мышление уже совершенно иное."
    kw "Вообще, самое страшное то, что он взялся за живых людей."
    kw "Если бы он просто сидел в комнате, изливал свои фантазии на бумагу и никого не трогал, всё было бы хорошо.{w} Для его окружения, по крайней мере."
    kw "Но в нашем случае всё очень и очень серьёзно."
    kw "Он доведёт своё дело до конца.{w} Если его не остановить, разумеется."
    "Славя хлопала глазами и пыталась понять масштабы угрозы."
    sl "А что идёт после второй фазы?"
    show kw thinking with dspr
    kw "Обычно считается, что фаза «Триумфа» вторая и последняя, так как большинство маньяков не могут остановиться сами и их останавливают разнообразными силовыми методами."
    kw "Но я как раз читал, что в исключительных случаях наблюдается и третья фаза."
    kw "Так называемая фаза «Падения»."
    kw "Про неё не так много написано, а вся имеющаяся информация довольно абстрактная."
    sl "Странно."
    sl "Но ведь он не убивает!"
    show kw ser with dspr
    kw "Технически да. Но он вводит своих жертв в некое состояние, схожее с трансом."
    kw "Я не знаю точно, как он это делает, но у меня есть пара предположений…"
    "Славя выжидающе посмотрела на него."
    show kw smile_2 with dspr
    kw "…которые до поры до времени я придержу у себя. Не хочется быть источником ложной информации."
    th "Не доверяет?"
    th "Думает, что я проговорюсь кому-нибудь и в лагере поднимется шум…"
    "Карл словно прочитал её мысли."
    show kw with dspr
    kw "Кстати, в лагере ещё никто не в курсе происходящего?"
    sl "Нет."
    kw "Это хорошо."
    kw "Возможно, наш милый психопат хочет огласки, паники и шума."
    show kw smile_2_2 with dspr
    kw "Такого удовольствия мы ему не доставим."
    "Вебер улыбнулся."
    "Славя, конечно, и раньше понимала, что он знает больше, чем говорит."
    "Но настолько больше!.."
    th "Эта его психологическая теория была странной.{w} Да что уж там говорить — страшной!"
    th "Но если предположить, что он прав и всё действительно обстоит так, как он описал…"
    th "Надо смотреть в оба."
    "Тем временем стало совсем темно. {w}И холодно."
    show kw with dspr
    kw "Тебя проводить?"
    hide kw with dspr
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1)
    scene bg ext_house_of_sl_night with dissolve
    window show
    $ renpy.pause (0.3)
    "Дойдя до домика Слави, Вебер остановился."
    show kw with dissolve
    kw "Послушай…"
    kw "Помнишь, я днём говорил, чтобы ты проводила меньше времени рядом с Семёном?"
    kw "Я говорил это серьёзно."
    kw "Никто не знает, как и по кому маньяк нанесёт следующий удар."
    kw "Очень маловероятно, что по Семёну, но всё же…"
    kw "К чему рисковать?"
    "Славя вспомнила слова Юли и серьёзно посмотрела на него."
    sl "Думаешь, за день моё мнение изменилось?"
    sl "Мы доведём с ним эту историю до конца.{w} Вместе."
    "Вебер взмахнул рукой, словно хотел что-то сказать, но остановился.{w} Провёл по лицу."
    show kw disapp with dspr
    kw "Хорошо…"
    kw "Надеюсь, ничего не случится…"
    sl "А что может случиться?"
    show kw with dspr
    kw "Многое."
    kw "Будьте осторожны."
    "Славя взглянула на его серьёзное лицо и улыбнулась."
    sl "Доброй ночи, Карл."
    sl "Всё будет хорошо."
    kw "Завтра узнаем."
    kw "Доброй ночи."
    "Вебер склонил голову в прощании."
    hide kw with dissolve
    "Славя стояла на крыльце и всё думала о его последних словах."
    th "«Завтра узнаем»…"
    th "Намёк?{w} На что?"
    th "Хм…"
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1)

    $ prolog_time()
    play music music_list["sunny_day"] fadein 1
    scene bg darkroom with dissolve2
    window show
    $ renpy.pause (2,hard=True)
    "Как много дел.{w} И как мало времени."
    "Сколько ещё нужно успеть!"
    "Основная часть уже завершена, осталось несколько последних шагов."
    "Неизвестный похрустел пальцами и покосился в сторону."
    "На кровати лежала длинноволосая девушка."
    "В конце концов, это всё не так уж и важно."
    "Тренировка, не более того."
    "Подготовка к финальному акту представления."
    "Все герои уже на сцене."
    "Сценаристу осталось выбрать, кому жить, а кому умереть."
    "Единственный вопрос — кто здесь сценарист?"
    "Надеюсь, что я.{w} Ну а кто же ещё?"
    "Ведь не он же?"
    "Хотя…{w} Он главный герой. Тоже честно."
    "И пусть потом только скажет, что его роль была мизерной или неважной!"
    "Девушка шевельнулась."
    voice_unknown "Доброе утро.{w} Или вечер?"
    show mi shocked pioneer with dissolve
    mi "Это шутка?"
    show mi upset pioneer with dspr
    mi "Скажи, что это шутка!"
    voice_unknown "Разумеется! Конечно шутка!"
    voice_unknown "Сейчас я тебя развяжу, и мы пойдём обратно в лагерь. Подожди минуту."
    $ renpy.pause (2,hard=True)
    voice_unknown "Стало легче?"
    show mi cry pioneer with dspr
    voice_unknown "Не волнуйся.{w} Больно не будет."
    voice_unknown "Или будет?"
    "Да уж."
    "Будто мне это нравится."
    "Но что поделать — план есть план."
    "Ничего."
    "Завтра.{w} Завтра всё решится."
    hide mi with dspr
    stop music fadeout 2
    window hide
    scene black with dissolve
    $ renpy.pause (1,hard=True)

    $ day_time()
    $ persistent.sprite_time = "day"
    $ renpy.pause (2,hard=True)
    scene bg int_house_of_sl_day with dissolve
    play ambience ambience_int_cabin_day fadein 1
    $ renpy.pause (1)
    window show
    $ renpy.pause (0.5)
    th "Уже утро?.."
    th "А казалось, что сон окончательно меня покинул."
    "Славя поднялась с кровати и подошла к окну."
    "Солнце было ещё не высоко, но пригревало ощутимо."
    "Мир выглядел как обычно. Зелёная трава и чистое небо."
    th "Сегодня явно будет жарко."
    th "Надеюсь, Семёна не похитили…{w} Хотя это было бы сложно провернуть — он бы наверняка голосил на весь лагерь."
    "Славя улыбнулась и перевела взгляд на безмятежно спящую Женю."
    th "Здорово, наверное, просто наслаждаться жизнью и ни о чём не думать."
    th "Правду говорят: «Знание — сила, а незнание — счастье»."
    window hide
    $ renpy.pause (2,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg int_house_of_sl_day with dissolve
    window show
    "Проделав все утренние процедуры, Славя вернулась в комнату и взглянула на часы."
    "До завтрака оставалось ещё немного времени."
    th "Почему бы не пойти проведать Семёна?"
    stop ambience fadeout 2
    play sound sfx_close_door_1
    scene bg ext_house_of_mt_day with dissolve
    play ambience ambience_camp_center_day fadein 2
    "Дойдя до его домика, она подумала, что заходить за ним по утрам постепенно входит для неё в привычку."
    th "Да ничего такого!"
    th "Просто проверяю, жив он там или нет."
    "Успокоив себя этой мыслью, Славя подошла к двери…"
    play sound sfx_open_door_2
    show pi2 with dissolve
    "…как та внезапно открылась."
    "На пороге стоял Семён, полностью одетый, в чистой рубашке и с красиво повязанным галстуком."
    "В общем, готовый к новому дню."
    show pi2 smile with dspr
    "Увидев Славю, он широко улыбнулся."
    me "Ага, кто это тут у нас?{w} Доброе утро!"
    sl "Доброе утро! А ты куда уже собрался?"
    me "Да никуда, в принципе. Хотел в шезлонге на свежем воздухе посидеть."
    show pi2 with dspr
    me "А что?"
    sl "Ничего. Ночью всё нормально было?"
    show pi2 smile with dspr
    me "Если ты спрашиваешь, приставала ли ко мне Ольга Дмитриевна, то отвечу:{w} нет, не приставала."
    sl "Балбес!"
    show pi2 laugh with dspr
    "Семён улыбнулся ещё шире."
    "И как с ним вообще можно серьёзно говорить?"
    sl "Ладно, иди уже на свой шезлонг."
    hide pi2 with dspr
    $ renpy.pause (0.5,hard=True)
    show pi2 smile with dspr
    "Семён с разбегу запрыгнул в шезлонг и как следует уселся."
    th "Надо будет проследить, чтобы он его с собой не увёз в конце смены."
    th "А то он может!"
    me "А ты как поживаешь, кстати? Ничего нового с вечера?"
    sl "Нет. Всё тихо и спокойно.{w} Хотя…"
    show pi2 with dspr
    me "Хотя?"
    sl "Вчера вечером довелось поговорить с твоим другом!"
    show pi2 smile with dspr
    me "А-а-а, с Карл Иванычем?"
    me "И как, чего интересного рассказал?"
    sl "Да ничего особенного. Книги показал."
    me "И что же читает наш уважаемый вице-консул?"
    show pi2 laugh with dspr
    me "На немецком что-то, небось?"
    sl "Да.{w} А ты откуда знаешь?"
    sl "И он разве немецкий хорошо знает? Мне показалось…"
    "Семён расхохотался."
    me "Это он тебе сам сказал? На него похоже."
    me "Всё скромничает, значит…"
    "Славя захлопала глазами."
    "Семён взглянул на неё и расхохотался ещё сильнее."
    me "Ну даёшь! Как человек, у которого в семье разговаривают по-немецки, может его не знать?"
    me "Я всё удивлялся, когда в гости к нему приходил. Хорошо хоть сразу переходили на русский."
    me "Перспектива учить ещё и немецкий меня никак не радовала."
    me "Хотя если всё-таки Карлуша внезапно соскучится по родным и задумает переехать в какой-нибудь Бонн, видимо, придётся…"
    show pi2 with dspr
    "Семён отвлёкся и посмотрел в сторону."
    show pi2 smile with dspr
    me "А, вот и он идёт!"
    "Он яростно замахал рукой."
    show kw at right with dspr
    me "У вас тут что, встреча запланирована? Или вы теперь каждое утро будете за мной заходить?"
    me "Я не против, конечно, но вы хотя бы предупреждайте…"
    show kw ser with dspr
    kw "Во-первых, доброе утро."
    "Вебер кивнул Славе."
    kw "Во-вторых, ты какого чёрта расселся? Предложи место даме!"
    "Вместо ответа Семён ещё глубже опустился в недра шезлонга."
    show kw disapp with dspr
    "Вебер покачал головой."
    show pi2 laugh with dspr
    me "Что-о? Дама же не попросила!"
    me "И вообще, проявите снисхождение, господин начальник! Я тут, вообще-то, страдающее лицо!"
    show kw ser with dspr
    kw "И чем же это ты страдаешь?"
    "Семён произнёс какую-то тарабарщину. Вебер поморщился."
    show kw with dspr
    kw "Прекрати вставлять в речь иностранные словечки. Сам же недавно говорил, что выглядит глупо."
    sl "И невежливо."
    "— прибавила Славя. И обратилась к Веберу:"
    sl "А что он сказал?"
    kw "Сказал, что является объектом жестоких домогательств со стороны лица, покушающегося на его покой, безопасность и девственность."
    show pi2 smile with dspr
    me "Да ты только послушай, как это на русском звучит! Ужас!"
    kw "Да прекрати. Тебе просто подкидывают записки, вот и всё."
    show kw smile_2 with dspr
    kw "Или я о чём-то не знаю?.."
    sl "Да, Семён, новых записок не появилось?"
    me "Нет, ручка-то у меня теперь."
    "Семён вытянул из кармана ручку."
    me "А она, интересно, не волшебная?"
    me "Кхм…{w} Желаю, чтобы на Вебера упало яблоко!"
    show kw with dspr
    $ renpy.pause (1,hard=True)
    kw "…"
    sl "…"
    $ renpy.pause (1,hard=True)
    show pi2 with dspr
    me "Что? Значит, не волшебная."
    show kw disapp with dspr
    $ renpy.pause (1,hard=True)
    hide kw with dspr
    "Вебер с кислым видом посмотрел на него, развернулся и пошёл к столовой."
    show pi2 sad with dspr
    me "Ну вот, как всегда…"
    hide pi2 with dspr
    stop ambience fadeout 2
    stop music fadeout 2
    scene black with dissolve
    window hide
    $ renpy.pause (1,hard=True)

    scene bg int_dining_hall_people_day with dissolve
    play ambience ambience_dining_hall_full fadein 2
    $ renpy.pause (1)
    window show
    $ renpy.pause (0.5)
    sl "О чём думаешь?"
    show pi2 at left with dspr
    "Едва войдя в столовую, Семён переменился в настроении: стал более собранным и резким."
    me "Тебе не кажется странным, что Мику до сих пор нет? Что-то она уж слишком задерживается."
    show kw with dspr
    kw "У неё есть друзья? Надо бы поинтересоваться у них, где она может быть."
    "— сказал Карл, отвлёкшись от своих блинчиков."
    th "И откуда достал только? Нам такого за всю смену не давали!"
    me "Может, она заболела, а? И еду ей кто-то носит?"
    show kw ser with dspr
    kw "А кто с ней живёт?"
    me "Эм…{w} Лена?"
    kw "Ты уверен?"
    me "На самом деле нет."
    kw "Ясно."
    sl "Семён, новых записок точно ведь не было?"
    show pi2 ser with dspr
    "Семён помотал головой."
    me "Да точно. Либо же я их просто не нашёл."
    me "Но ведь это не моя вина, да?"
    show kw with dspr
    kw "Кто-нибудь знает, где она живёт?"
    sl "Я знаю.{w} Но лучше будет посмотреть в музкружках, она там почти всё время проводит."
    kw "Хорошо. Сразу после завтрака отправимся туда."
    "С этими словами Вебер вновь принялся за блинчики, а Славя уныло посмотрела на молочную кашу."
    hide pi2 with dspr
    hide kw with dspr
    $ renpy.pause (2,hard=True)

    scene bg ext_dining_hall_near_day with dissolve
    play ambience ambience_camp_center_day fadein 2
    $ renpy.pause (1,hard=True)
    show pi2 with dspr
    "Выйдя из столовой, Семён подошёл к старым жигулям."
    show kw at right with dspr
    kw "Ты что, до музкружка ехать собрался?"
    kw "Даже не буду спрашивать, с собой ли у тебя права…{w} И есть ли они у тебя вообще?"
    show pi2 smile with dspr
    me "А что, тут кто-то будет их проверять?"
    me "И вообще, вас никогда не интересовало, почему эта машина здесь стоит?"
    sl "Лично меня — нет."
    me "Скучные вы…"
    hide pi2 with dspr
    "Семён заглянул в машину и со вздохом отошёл, видимо не найдя ничего интересного."
    show pi2 with dspr
    sl "Слушайте, может, она просто позже приходит? Давайте подождём немного."
    me "Ну давайте. Хотя я очень сомневаюсь, что она появится."
    kw "Это почему?"
    me "Ты, видно, забыл, что Мику из Японии. Японцы — народ пунктуальный."
    me "Даже при всей своей… «самобытности» Мику никогда не опаздывала на линейки и в столовую всегда приходила вовремя."
    sl "Может, спросим пока у кого-нибудь? Вдруг кто-то её видел?"
    show dv normal pioneer far at left with dissolve
    "Славя огляделась по сторонам и заметила Алису."
    sl "Алиса! Можно тебя на минутку?"
    show dv normal pioneer at left with dspr
    dv "Чего это вы тут могучей кучкой стоите? Бить будете?"
    me "А есть за что?"
    show dv grin pioneer at left with dspr
    dv "Всегда можно найти!"
    show dv normal pioneer at left with dspr
    dv "Так вам чего?"
    me "Алис, ты не видела Мику в последнее время? Сегодня, вчера?"
    dv "Мику?"
    dv "Дай подумать."
    dv "Ульяну видела, Шурика видела, Лену видела…"
    show dv angry pioneer at left with dspr
    dv "Ох, лучше бы Лену не видела!"
    kw "А что такое?"
    show dv normal pioneer at left with dspr
    dv "Да ничего!"
    dv "Представляешь, выхожу я из домика, значит, и тут она идёт."
    dv "И глаза у неё натурально бешеные!"
    dv "Увидела меня и говорит:"
    show dv angry pioneer at left with dspr
    dv "«А ты помнишь себя в настоящем мире?»"
    show dv normal pioneer at left with dspr
    dv "Ну я сразу поняла, что с ней опять что-то не то, хотела отойти, но она меня за руку схватила!"
    show dv angry pioneer at left with dspr
    dv "«Твои воспоминания на самом деле ложь! Всё это неправда!»"
    show dv sad pioneer at left with dspr
    dv "И захохотала."
    dv "Сейчас-то это не так страшно выглядит, но утром…"
    dv "С ней явно что-то не то творится, ребята."
    show dv angry pioneer at left with dspr
    dv "Я вам зуб даю, это всё американцы виноваты."
    dv "Засунули ей в голову жучок и сводят её с ума."
    dv "Рано или поздно она так за нож возьмётся!"
    sl "Да нет, не может такого быть.{w} Что за ерунда?"
    dv "Это ты просто её не видела! А вот вскроют кого — и припомнишь мои слова!"
    kw "Прошу прощения, что прерываю, но что там по Мику?"
    show dv normal pioneer at left with dspr
    dv "Мику…{w} Нет, её я не видела."
    me "Жаль. Если увидишь, сообщи нам, хорошо?"
    dv "Хорошо."
    show dv grin pioneer at left with dspr
    dv "А что, есть повод для паники и массовой истерики?"
    sl "Нет-нет, всё в порядке. Она у себя в кружке, наверное."
    dv "Уж наверное! Сами знаете, как за музыкой время летит."
    me "Кстати, насчёт музыки."
    me "Это не ты у Электроника магнитофон увела?"
    show dv angry pioneer at left with dspr
    dv "Сколько можно повторять, что НЕТ?"
    dv "Мало он вчера ко мне приставал, что ли?"
    dv "Откуда мне знать, где он свою рухлядь посеял? В клубе где-нибудь лежит, наверное!"
    dv "Свою музыку я на другой магнитофон ставлю, который с музкружка. Увидите Мику — спросите, она вам подтвердит."
    sl "Как думаешь, кому он мог понадобиться?"
    show dv normal pioneer at left with dspr
    dv "А мне почём знать?"
    show dv grin pioneer at left with dspr
    dv "Среди вас и так тут умных людей хватает."
    "— проговорила Алиса с хитрым видом, то и дело посматривая на Вебера."
    show kw disapp with dspr
    "Вебер вздохнул."
    kw "Это тот магнитофон, про который ты мне рассказывал?"
    me "М-гм."
    show kw with dspr
    kw "Я не радиолюбитель, но могу предположить, что с его помощью можно передавать волны на большие расстояния."
    kw "Если он принимает сигналы из Германии, то, по сути, может передавать их и обратно. Мы как раз недавно обсуждали это с товарищем."
    show pi2 surp with dspr
    me "С кем это ты обсуждал?"
    kw "С сэром Николасом. Военный атташе из посольства Австралии."
    kw "Если я правильно понял его длинную речь и страшный акцент, для подобного эксперимента потребуется преобразователь звукового сигнала, усилитель, антенна и ещё куча подобных сложных деталей."
    kw "Здесь их вряд ли можно найти, так что забудьте всё, что я сейчас сказал."
    show dv smile pioneer with dspr
    dv "Хм-м… А ты неплох.{w} Почему мы до сих пор не знакомы?"
    dv "Быть может, самое время наверстать это досадное упущение?.."
    "Семён кашлянул."
    show pi2 ser with dspr
    me "Нам пора."
    show dv sad pioneer with dspr
    dv "Эх, ладно."
    dv "Бывайте, увидимся ещё!"
    show dv grin pioneer with dspr
    dv "Я Алиса, кстати. А вы, молодой человек?.."
    me "Мы торопимся."
    dv "Ну хорошо, в другой раз…"
    stop ambience fadeout 2
    $ renpy.pause (1)
    window hide
    scene black with dissolve
    hide kw
    hide dv
    hide pi2
    $ renpy.pause (1)

    scene bg ext_music_club_verandah_day with dissolve
    play ambience ambience_camp_entrance_day fadein 2
    show pi2 at left with dissolve
    show kw with dissolve
    window show
    $ renpy.pause (0.5)
    "Подойдя ко входу, Семён принялся громко стучать."
    play sound sfx_knock_door3_dull
    $ volume(1.6,'sound')
    kw "Ты не дал мне и слова сказать той милой барышне. Неужто мы так сильно спешим?"
    show kw smile_2 with dspr
    me "А разве нет?"
    "— бросил Семён, не отвлекаясь от своего занятия."
    play sound sfx_knock_door3_dull
    $ volume(1.6,'sound')
    show kw ser with dspr
    "…"
    "Никто не отвечал."
    me "Это плохой знак."
    sl "Подожди паниковать. Вдруг она в комнате?"
    me "Хм…"
    hide kw with dspr
    "Вебер тем временем подошёл к окну и принялся всматриваться вглубь комнаты."
    "Спустя минуту он вернулся к Семёну."
    hide pi2 with dspr
    show kw with dspr
    show pi2 at left with dspr
    kw "Дай-ка я попробую."
    "Подойдя к двери, он потрогал её рукой и навалился плечом."
    play sound sfx_open_door_2
    "К удивлению Слави, дверь легко открылась."
    stop ambience fadeout 1
    $ renpy.pause (0.5)
    scene bg int_musclub_day_2 with dissolve
    play ambience ambience_music_club_day fadein 2
    $ volume(0.4,'ambience')
    show pi2 surp with dspr
    play music think_deeply fadein 2
    $ renpy.pause (2)
    sl "…"
    sl "Что это?"
    "Мику в комнате не было, но на полу был нарисован какой-то странный знак."
    "Вебер прошёл внутрь и склонился над надписью, стараясь рассмотреть её поближе."
    show kw ser at left with dspr
    kw "Семён?{w} Видел когда-нибудь нечто подобное?"
    me "Я? Нет…"
    kw "Вот и я нет."
    kw "В этот раз, видимо, записки не будет.{w} Только этот знак."
    kw "Только вот он не даёт никакой информации по Мику."
    show pi2 with dspr
    me "Если её действительно похитили, она наверняка сейчас в том же состоянии, что и Ольга Дмитриевна.{w} Если не что-то похуже."
    sl "Подождите."
    show kw surp with dspr
    sl "Семён, помнишь, в лесу мы видели краску на тропинке?"
    me "Думаешь, это она и есть?"
    "Славя ещё раз взглянула на знак."
    sl "Да, определённо."
    show pi2 with dspr
    show kw with dspr
    kw "Краска? Где вы её видели?"
    sl "В лесу, недалеко от старого лагеря."
    th "Только бы не пришлось идти туда…"
    show kw with dspr
    kw "Кстати, а это случайно не тот самый магнитофон?"
    "Вебер показал на красный магнитофон, стоящий на двух стульях чуть позади."
    show pi2 with dspr
    me "Это он!{w} Но как он здесь оказался?"
    kw "А ты догадайся."
    show pi2 surp with dspr
    "Семён побледнел."
    kw "Он здесь тоже не просто так."
    kw "Не оставил ли наш друг какого-нибудь послания?"
    hide kw with dspr
    "Вебер подошёл к магнитофону."
    show kw at left with dspr
    kw "Семён, не подскажешь, как он работает?"
    me "Секунду."
    hide pi2 with dspr
    $ renpy.pause (2)
    show pi2 with dspr
    me "А что ты хочешь сделать?"
    kw "Посмотри, есть ли там какая-нибудь кассета."
    me "…"
    me "Есть! Сейчас запущу…"
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (2)
    play sound button_01
    $ volume(0.4,'sound')
    $ renpy.pause (2,hard=True)
    play sound scary_turnitoff_reverse fadein 1
    $ volume(0.4,'sound')
    $ renpy.pause (9,hard=True)
    "…"
    show pi2 surp with dspr
    play music think_deeply fadein 2
    $ volume(0.7, "music")
    play ambience ambience_music_club_day fadein 2
    $ volume(0.4,'ambience')
    "Славя с недоумением посмотрела на Семёна."
    sl "Что это за голос?"
    me "А ты как думаешь?"
    sl "Это {i}он{/i}?"
    kw "Может быть.{w} А может, и нет."
    kw "Такую возможность нельзя исключать."
    sl "Но почему ничего не понятно?"
    me "Запись повредилась?"
    kw "Сомневаюсь. Какой тогда смысл давать нам магнитофон?"
    show kw thinking with dspr
    kw "Хм…"
    $ renpy.pause (1)
    show pi2 with dspr
    kw "Семён, можно ли эту кассету как-то назад прокрутить?"
    show pi2 ser with dspr
    me "Назад? Можно."
    me "Только я не знаю как."
    show kw with dspr
    kw "Жаль."
    kw "Значит, придётся его к Электронику нести."
    kw "Семён, поднимай."
    sl "Постойте."
    sl "Вы сейчас не шутите?"
    sl "Вы действительно не знаете, как это сделать?"
    show kw thinking with dspr
    show pi2 surp with dspr
    kw "А ты знаешь?"
    sl "Конечно! Любой школьник знает."
    sl "Семён, дай ручку, пожалуйста."
    me "Ручку?{w} Держи."
    me "Только не понимаю, зачем она тебе."
    show kw with dspr
    show pi2 with dspr
    "Славя взяла ручку, что-то покрутила в кассете и вставила её обратно в магнитофон."
    "Нажала пару кнопок."
    sl "Запускать?"
    "Семён кивнул."
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (1)
    play sound button_01
    $ renpy.pause (1)
    play sound scary_turnitoff fadein 1
    $ volume(0.6,'sound')
    $ renpy.pause (9,hard=True)
    "…"
    play ambience ambience_music_club_day fadein 2
    $ volume(0.5,'ambience')
    show kw thinking with dspr
    show pi2 with dspr
    "Повисло молчание."
    "Каждый пытался осмыслить услышанное."
    $ renpy.pause (1,hard=True)
    me "«Всё завершится сегодня»?"
    kw "Да."
    kw "Видимо, нашему другу уже наскучило играть.{w} Недолго же он продержался."
    kw "Либо же мы {i}недостаточно сильные оппоненты{/i}."
    sl "Что он хотел этим сказать? Напугать нас?"
    show kw with dspr
    kw "Скорее всего."
    kw "Какова была ваша первая мысль?"
    sl "Эм…{w} Что неплохо было бы убежать в комнату и спрятаться под кровать?"
    me "Точно. Ну и жуткий же голос."
    kw "Получается, мы должны делать всё наоборот: продолжить искать улики и вести расследование."
    me "А мы вели расследование?"
    show kw disapp with dspr
    $ renpy.pause (1.5)
    show kw with dspr
    kw "Какие у нас есть зацепки?"
    me "Хм… Краска?"
    me "Пожалуй, больше и нет ничего."
    th "Почему-то меня не оставляет ощущение, что всё это может быть ловушкой."
    th "Он оставляет подсказки и наблюдает, как мы пытаемся что-то предпринять или понять."
    th "Мы действительно играем по его правилам.{w} А что нам ещё остаётся?"
    show pi2 with dspr
    kw "Хорошо. Давайте взглянем."
    kw "На случай, если там ещё что-то осталось."
    sl "То, что краску пролили на дороге к старому лагерю, не может быть простой случайностью."
    kw "Да что за старый лагерь такой?"
    kw "Все про него упоминают, но никто не рассказывает."
    me "О, это поистине райское местечко."
    me "Но я очень надеюсь, что нам не придётся заходить внутрь."
    kw "Почему?"
    me "Пошли. По дороге объясню."
    hide pi2 with dspr
    hide kw with dspr
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1)

    scene bg ext_path_day with dissolve
    play ambience ambience_camp_entrance_day fadein 2
    show pi2 at left with dissolve
    show kw with dissolve
    window show
    $ renpy.pause (0.5)
    kw "…и никто туда не ходит?"
    me "Нет.{w} А зачем?"
    me "Там ведь нет ничего, только развалины и куча мусора."
    kw "Хм…"
    hide kw with dspr
    "Вебер будто снова погрузился в свои мысли."
    me "Славь, а мы ещё не прошли?"
    sl "Нет…{w} Наверное."
    sl "Оно было где-то здесь…"
    sl "А, вот же!"
    show stain_2 with dspr
    "Славя указала пальцем на пятно. Оно уже было не таким ярким, как вчера."
    "Вебер подошёл ближе."
    show kw ser with dspr
    kw "Действительно, та же краска."
    kw "Но почему здесь?"
    kw "И когда точно вы её здесь увидели?"
    me "Вчера…"
    sl "После полудня, часа в два."
    kw "Хм…"
    show kw thinking with dspr
    kw "Выходит, что наш милый друг сумел поймать Мику, когда она была одна и искала Лену."
    kw "Что-то с ней сделал и куда-то спрятал."
    kw "Затем где-то раздобыл краску и нарисовал знак на полу музкружка."
    kw "После этого отнёс банку или ведро куда-то в этом направлении и случайно пролил немного."
    show kw ser with dspr
    kw "Так, что ли?"
    me "Стало быть, так."
    show pi2 ser with dspr
    me "Только ты уверен, что он «случайно» её пролил?"
    me "Он хотел, чтобы мы это обнаружили."
    me "Это может быть ключом к тому, как найти следующую подсказку."
    me "Не забывай: он играет с нами."
    me "Чтобы игра была честной, у всех должны быть равные шансы."
    kw "Возможно."
    kw "Но тогда почему мы играем трое против одного?.."
    sl "Как думаете, могут тут быть ещё какие-то улики?"
    kw "Вряд ли. Эта краска сама по себе довольно…"
    voice "Ой, а что это вы тут делаете?"
    "— донёсся голос откуда-то сзади."
    show pi2 surp with dspr
    show kw surp with dspr
    "Славя обернулась и…"
    show un smile pioneer far at right with dspr
    extend " увидела улыбающуюся Лену."
    me "П-привет! А мы тут…{w} расследование проводим."
    show kw ser with dspr
    show pi2 with dspr
    show un smile pioneer at right with dspr
    un "Расследование? Интересно!"
    un "А чего расследуете? Убийство?"
    "По спине Слави пробежал холодок."
    th "Надеюсь, что нет…"
    "Славе тут же вспомнились слова Алисы."
    th "С Леной явно стоит быть настороже."
    show kw with dspr
    "Вебер сделал шаг вперёд."
    kw "Лена, скажите, пожалуйста, известно ли вам что-нибудь об этом вот…{w} пятне?"
    kw "Чисто случайно, разумеется."
    show un serious pioneer with dspr
    un "Посмотрю поближе."
    hide un with dspr
    "Лена склонилась над пятном."
    "Поизучав его с полминуты, она выпрямилась и сказала:"
    show un normal pioneer at right with dspr
    un "Это краска."
    show pi2 smile pioneer with dspr
    me "В самом деле?"
    un "Да."
    show un smile2 pioneer with dspr
    un "А что?"
    me "Да нет, ничего…{w} Спасибо за такое чудесное открытие!"
    "Лена сарказма не поняла."
    show un smile pioneer with dspr
    un "Пожалуйста."
    sl "Слушай, ты же часто гуляешь в лесу, да?"
    show un normal pioneer with dspr
    "Лена чуть смутилась."
    un "Время от времени, может быть. А что?"
    sl "Ты не видела никого подозрительного?"
    show un surprise pioneer with dspr
    un "Подозрительного?"
    sl "При тебе никто в последнее время не вёл себя странно? Необычно?"
    show un normal pioneer with dspr
    un "Нет, наверное."
    un "Хотя знаете, вчера после завтрака я кого-то видела."
    un "Он ужасно торопился и тащил с собой что-то громоздкое."
    sl "«Он»? Так это был парень?"
    un "Да, определённо."
    "Лена задумалась."
    $ renpy.pause (1)
    show un surprise pioneer with dspr
    un "Знаете что?{w} Это могло быть ведро с краской!"
    show pi2 surp with dspr
    show kw thinking with dspr
    "Славя взглянула на ребят."
    "Они олицетворяли собой полную противоположность — Семён переминался с ноги на ногу и заметно нервничал, в то время как Вебер, наоборот, оставался идеально спокойным."
    sl "Не обратила внимания, как он выглядел?"
    un "Нет, я была слишком далеко."
    un "Он промелькнул на секунду и тут же скрылся в листве."
    me "А ты не заметила, в каком направлении он пробежал?"
    un "Туда."
    "Лена показала рукой в сторону старого лагеря."
    show kw ser with dspr
    kw  "Вот значит как…"
    kw "Что ж, благодарим за содействие!"
    show un shy pioneer with dspr
    un "Да не за что…{w} А что такое-то происходит?"
    show kw thinking with dspr
    "Вебер задумался, словно прикидывая, стоит ли посвящать её в это дело."
    show kw with dspr
    "Кивнув, он подошёл к ней чуть ближе и произнёс:"
    kw "Кто-то вчера похитил Мику и начертил этой краской странный символ."
    kw "Мы пытаемся разобраться во всей ситуации, пока дело не приняло совсем недобрый оборот."
    th "Зачем?!"
    th "Зачем он ей это рассказывает?"
    th "Хотя… Если Семён прав и она действительно может быть как-то связана со всем этим, всё, что мы скажем, вряд ли будет для неё новостью."
    th "Интересно посмотреть на её реакцию…"
    show un scared pioneer with dspr
    "После слов Вебера Лена вздрогнула и пробормотала:"
    un "Так вот почему я не видела её со вчерашнего дня…"
    th "Если и прикидывается, то очень убедительно."
    "Тем временем Лена обратилась почему-то к Семёну:"
    un "Что мне делать?"
    me "Ничего. Веди себя как обычно."
    me "Только будь чуть более осторожной, ладно?"
    show un normal pioneer with dspr
    "Лена кивнула."
    un "Хорошо…{w} И вы упомянули странный знак, да?"
    un "Могу я на него взглянуть?"
    "Вебер и Семён переглянулись."
    "Видимо, говорить так много Карл изначально не планировал."
    show kw disapp with dspr
    kw "Ну хорошо."
    show kw with dspr
    kw "Только никому ни слова, договорились?"
    un "Ладно."
    stop ambience fadeout 2
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1)

    scene bg ext_musclub_day with dissolve
    play ambience ambience_camp_center_day
    window show
    $ renpy.pause (0.5)
    "Дойдя до музкружков, Вебер попросил Семёна постоять снаружи на случай, если кто-то захочет зайти в клуб."
    "Они с Леной вошли внутрь."
    "Славя же решила остаться с Семёном."
    show pi2 with dissolve
    sl "Семён…"
    "Семён не ответил."
    "Славя подошла поближе и потянула его за рукав."
    show pi2 surp with dspr
    me "А? Что такое, Славя?"
    sl "О чём думаешь?"
    show pi2 ser with dspr
    me "О том, что всё это очень странно."
    me "Но, по крайней мере, мы уже знаем чуть больше."
    sl "Я не думаю, что нам стоит принимать всё, что говорит Лена, за чистую монету."
    me "Согласен."
    me "Если верить записи на кассете, у нас осталось не так много времени."
    me "И Лена появилась как нельзя кстати."
    me "У тебя есть какие-то мысли на этот счёт?"
    sl "Три мысли."
    show pi2 smile with dspr
    me "Даже так?"
    me "Ну что ж, давай послушаем."
    sl "Первая, самая маловероятная: это совпадение."
    sl "И всё, что она сказала, — правда."
    sl "Вторая: тоже совпадение, но про «пионера, который исчез в кустах» она придумала."
    sl "Чтобы внимание привлечь, может быть, или ещё зачем-то."
    show pi2 ser with dspr
    me "Интересно…{w} А третья?"
    sl "А третья…"
    play sound sfx_open_door_2
    show kw at right with dspr
    show un normal pioneer at left with dspr
    "В этот момент дверь открылась, выпуская Карла и Лену наружу."
    un "…а может это быть каким-то символом? Иероглифом, например?"
    show kw ser with dspr
    kw "Иероглифом? Вряд ли."
    kw "Скорее всего, это какой-то оккультный или религиозный символ."
    kw "Но, к сожалению, я даже предположить не могу, как и где искать его значение."
    un "Я тоже никогда не видела ничего подобного.{w} Хотя…"
    un "Вы не пробовали в библиотеке посмотреть?"
    show kw surp with dspr
    "Вебер удивился."
    kw "В библиотеке?"
    kw "А там есть книга на эту тему?"
    un "Есть книга по криптологии и шифрам. Если вы где-то и найдёте ответ, то только там."
    show kw thinking with dspr
    kw "Ну… Может быть.{w} Попробовать стоит."
    show kw ser with dspr
    kw "Кстати, а откуда в вашей библиотеке такие книги? Я удивился, найдя «Общую психологию»."
    show un smile pioneer with dspr
    un "А, так это вы её взяли? А я-то думала, кому она могла понадобиться…"
    un "Я слышала, что некоторую часть книг привезли из старого лагеря."
    un "Во время войны там был региональный штаб Красной армии. Наверное, это их книги."
    show un normal pioneer with dspr
    un "Удивительно, что их немцы не сожгли, когда захватили здание…"
    "Славя передёрнула плечами."
    th "Опять этот старый лагерь!"
    show un shy pioneer with dspr
    un "Семён, ты меня не проводишь? После этого всего мне как-то страшно…"
    th "Да уж, тебе — и страшно? Рассказывай."
    "Но Семён будто не увидел в этой просьбе ничего странного."
    "Либо не показал вида."
    me "Ну пошли."
    hide pi2 with dspr
    hide un with dspr
    sl "Семён, останься!"
    "— внезапно сказала Славя."
    show pi2 surp with dspr
    "Семён оглянулся на неё."
    me "Не волнуйся."
    me "Я просто провожу Лену и мигом к вам."
    me "Вы пока идите в библиотеку и поищите книгу."
    me "Там встретимся."
    sl "Будь осторожен, хорошо?"
    me "Буду."
    hide pi2 with dspr
    "Он кивнул, и они с Леной отправились прочь."
    show kw with dspr
    kw "Что ты так переживаешь? Ничего с ним не случится."
    sl "Надеюсь на это…"
    th "Хоть бы я была неправа насчёт Лены."  
    hide kw with dspr
    $ renpy.pause (0.5)
    stop ambience fadeout 2
    window hide
    scene black with dissolve
    $ renpy.pause (1)

    scene bg int_library_day with dissolve
    play ambience ambience_library_day fadein 2
    window show
    $ renpy.pause (0.5)
    "Зайдя в библиотеку, Вебер огляделся."
    show kw with dspr
    kw "А где Женя?"
    kw "Она произвела впечатление довольно серьёзной девушки."
    show kw smile_2 with dspr
    th "Порой даже слишком серьёзной…"
    show kw ser with dspr
    kw "И вот библиотека открыта, а её самой нет."
    sl "Да ладно тебе. Если она отошла, значит, на то были весомые причины."
    kw "Хорошо. Надеюсь, она не огорчится, что мы здесь порылись. Причины у нас тоже весомее некуда."
    kw "Я буду смотреть вот эти стеллажи."
    kw "Вон тот большой левый — твой. Идёт?"
    hide kw with dspr
    "И, не дожидаясь ответа, принялся за работу."
    "Славя вздохнула и уныло оглядела свой стеллаж."
    th "Втроём бы справились быстрее…"
    th "И почему бы не подождать Женю, в самом деле?"
    th "Она-то уж точно знает, где что лежит."
    $ renpy.pause (1)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg int_library_day with dissolve
    "Прошло минут двадцать."
    "За это время Славя успела осмотреть половину своих полок."
    "Книги были в основном на коммунистическую тематику — Теория марксизма, Политэкономия и толкования Капитала."
    "Обычно эти темы были Славе интересны, но сейчас…"
    "Сейчас они казались чем-то далёким и совершенно не важным."
    "Словно из другой вселенной…"
    $ renpy.pause (0.5,hard=True)
    th "О! Что это?"
    "Корбышев Е., «Криминальная психология»."
    th "Интересно…"
    "Славя взяла книгу с полки и открыла наугад."
    window hide
    $ renpy.pause (0.5,hard=True)
    $ set_mode_nvl()
    "Преступление всегда имеет два корня: один лежит в личности преступника и происходит из особенностей его развития, а другой состоит из внешних для данной личности факторов, имевших на неё сильное влияние."
    "Тем не менее, ни одно преступление нельзя объяснить исключительно внешними причинами, игнорируя особенности совершившей его личности."
    "Нарисуйте какую угодно цепь внешних событий — в любом случае к окружающей среде необходимо будет прибавить факт личностной психологии."
    nvl clear
    "Если же нарисовать картину таких условий, при которых для человека исчезает возможность выбора, то к таким случаям понятие «преступления» не имеет логики и не может быть применено."
    "Но такого быть не может. {i}Выбор есть всегда{/i}."
    "Вообще, преступление предполагает вину, а вина показывает, что в личности совершившего преступление есть нечто, что послужило корнем этого преступления."
    "Вина есть преходящее настроение, сложившееся у человека под влиянием двух сил: с одной стороны, действия на него различных событий, происходивших в окружающей среде, с другой — его психической конституции."
    $ renpy.pause (0.5,hard=True)
    $ set_mode_adv()
    th "«…действия на него различных событий, происходивших в окружающей среде…»"
    th "Если это действительно так, то получается, что {i}любое{/i} преступление можно объяснить тем, что преступник чувствует себя «вне» этого мира."
    th "Тут есть над чем подумать."
    $ renpy.pause (0.5,hard=True)
    kw "Славя!"
    "Славя вздрогнула и едва не выронила книгу из рук."
    "Поставив её на место, она поспешила к Веберу."
    show kw with dspr
    "Карл держал в руках потёртую книгу."
    kw "На ней совершенно не было пыли. Примечательно, не правда ли?"
    show kw thinking with dspr
    kw "И почему-то мне кажется, что я видел эту книгу раньше."
    sl "Надеюсь, ты не забыл, как выглядел тот знак."
    show kw with dspr
    kw "Помню."
    kw "Но это не нужно — я срисовал его в блокнот."
    show notepad with dissolve
    "Вебер извлёк блокнот и показал Славе рисунок."
    sl "Остаётся только надеяться, что он здесь есть…"
    hide notepad with dspr
    "Вебер принялся листать книгу."
    "Почти сразу же он наткнулся на небольшой листок, которым была заложена одна из страниц."
    show kw ser with dspr
    kw "Так, а это что…"
    "Он занялся листком, в то время как Славя вгляделась в разворот книги."
    sl "Смотри! Это он!"
    hide kw with dspr
    show book_bookshop with dissolve
    "Ошибки быть не могло: в книге был нарисован тот же самый знак, что и в музкружках."
    "Надпись под ним гласила:"
    window hide
    $ set_mode_nvl()
    $ renpy.pause (0.5,hard=True)
    "Знак архивариуса. Основное место нахождения — гробница фараонов в Гизе, Египет."
    "Дошедшие до нас документы утверждают, что его собственноручно наносил главный архивариус, присутствовавший при захоронении фараона. Предполагалось, что этот знак позволяет душе переместиться в иной мир, в котором её ждёт перерождение на более высокую ступень."
    "В настоящее время используется фанатиками радикального толка, желающим летать снаружи всех измерений, в так называемом «нулевом пункте»."
    "Считается, что это один способов даровать человеку бессмертие."
    nvl clear
    "Фанатики утверждают, что для проведения ритуала необходимо использование жизненной силы нескольких людей. В большинстве случаев достаточно трёх жертв."
    "В фазе подготовки они вводятся в специальное состояние, в котором полностью теряют чувствительность. Через несколько дней, когда их сила иссякнет окончательно, ритуал может быть проведён."
    $ renpy.pause (0.5,hard=True)
    $ set_mode_adv()
    hide book_bookshop with dspr
    window show
    $ renpy.pause (0.5,hard=True)
    th "Трёх жертв?"
    "Славя прислонилась к стеллажу."
    th "Ольга Дмитриевна… Мику…"
    th "И кто-то третий."
    th "Кто?"
    sl "Вебер, посмотри, что здесь написано."
    sl "Вебер?.."
    "Карла не было рядом. Лишь на полу лежала записка, которую он обнаружил в книге."
    "Славя подняла её."
    stop ambience fadeout 2
    show note_final_1 with dissolve
    "…"
    hide note_final_1
    play sound paper
    show note_final_2 with dspr
    "…"
    hide note_final_2 with dspr
    "Славя свернула записку и положила её в карман."
    th "Значит, это правда. Он действительно сумасшедший!"
    th "Но где его найти?"
    "Славя напряженно думала."
    th "Надо срочно показать это Семёну! Эта записка наверняка для него."
    "Славя ринулась к двери."
    show mz normal glasses pioneer with dissolve
    $ renpy.pause (0.5,hard=True)
    th "Ой-ой…"
    mz "Славя? Ты что здесь делаешь?"
    sl "Тебя не было, а мне срочно надо было найти кое-что…"
    th "Сейчас начнётся…"
    mz "Нашла?"
    sl "Да…"
    mz "Молодец. Если больше ничего не надо, то не задерживаю."
    hide mz with dspr
    "— сказала Женя и склонилась над журналом, в котором что-то до этого писала."
    th "И всё?"
    th "Легко я отделалась."
    "Славя прошла мимо, попутно заглянув через её плечо."
    sl "А что это такое?"
    show mz normal glasses pioneer with dissolve
    mz "Журнал посещений. Руководство сказало, что все посетители должны быть записаны."
    mz "В конце смены они какие-то призы хотят выдать или ещё что…"
    show mz smile glasses pioneer with dspr
    mz "Но по секрету скажу, что они так особо умных на карандаш берут."
    mz "Часто ходишь в библиотеку, значит, много свободного времени."
    mz "А если ещё что странное читаешь…"
    show mz laugh glasses pioneer with dspr
    mz "Добро пожаловать в ГУЛАГ!"
    "Женя расхохоталась."
    th "Уж ей ли не знать, что ГУЛАГов давно нет…"
    sl "Можно посмотреть?"
    show mz normal glasses pioneer with dspr
    mz "Да смотри конечно, жалко что ли. Только зачем?"
    "Славя склонилась над журналом."
    "За вчерашний день в библиотеке были:"
    "Персунов, Вебер, Ясенева, Тихонова."
    th "Семён, Карл, Лена и я."
    th "Больше никого нет."
    th "Выходит, что записку мог подложить только кто-то из нас…"
    th "Но кто?"
    th "Семён, Вебер или Лена?"
    th "Семён — вряд ли."
    th "Вебер — нашёл её."
    th "Лена?.."
    sl "Всё сходится…"
    "Пробормотала Славя и резко бросилась из библиотеки."
    mz "Эй, ты куда?"
    hide mz with dspr
    $ renpy.pause (0.5)
    window hide
    scene black with dissolve
    $ renpy.pause (1)

    scene bg ext_house_of_mt_day with dissolve
    window show
    $ renpy.pause (0.5)
    "Я подхожу к домику и пытаюсь сбросить с себя внезапно навалившуюся усталость."
    th "Ничего. Скоро это всё закончится."
    th "С Леной пришлось повозиться, конечно."
    th "Никогда бы не подумал, что она будет так цепляться!"
    th "Ну да ладно."
    "Я подхожу к двери и вижу, что она приоткрыта."
    "Внутренне собираюсь и захожу в комнату."
    scene bg int_house_of_mt_day with dissolve
    show kw with dspr
    "В центре комнаты на стуле сидит Вебер. На его коленях лежит трость."
    kw "Ну здравствуй, Семён."
    me "Карл? Ты что здесь делаешь?"
    kw "Довольно. Пора прекратить эту игру."
    "Он поднялся."
    "Я делаю шаг назад."
    me "Что ты делаешь?"
    show kw smirk with dspr
    kw "Приношу в этот мир немного справедливости."
    show kw smile_2_2 with dspr
    "Он отставляет трость в сторону."
    me "Послушай, я…"
    kw "После.{w} Становись."
    "Я становлюсь в боевую стойку и пытаюсь расслабиться, насколько это возможно."
    "Вебер становится напротив меня, разминая пальцы."
    show kw smile_2 with dspr
    kw "Все разговоры — после. А сейчас немного развлечёмся."
    "— договаривает он, и… "
    hide kw with dspr
    with hpunch
    play sound air_01
    $ volume(1.4,"sound")
    play music final_inquiry fadein 2
    extend "я едва успеваю уклониться от быстрого удара ребром ладони."
    show kw smile_2_2 with dspr
    "Я сразу же пытаюсь контратаковать, но Карл наготове — он ставит блок и едва касается своими пальцами моего локтя."
    show redscreen with dissolve
    $ renpy.pause (0.3)
    hide redscreen with dissolve
    th "Чёрт!"
    "Прикосновение совсем лёгкое, но его хватает — я не чувствую левую руку."
    "Карл развивает преимущество — серией коротких ударов он загоняет меня в угол комнаты."
    play sound air_02
    $ volume(1.7,'sound')
    "Я пытаюсь контратаковать и перейти в наступление, но все мои попытки тщетны — Карл выше и сильнее меня. Тем более в моём распоряжении только одна рука."
    th "Ну же, мне нужно что-то сделать!"
    play sound air_03
    $ volume(1.4,'sound')
    "Я собираюсь с силами и резко выбрасываю кулак правой руки вперёд."
    hide kw with dspr
    "Вебер плавно уклоняется, словно прочитав на моём лице, куда и как я хочу нанести удар."
    show kw smile_2_2 with dspr
    "Он отвечает коротким выпадом мне в корпус."
    "Сначала я подумал, что он промахнулся, но потом почувствовал, что мышцы груди начинают неметь."
    hide kw with dspr
    play sound coughing
    $ volume(0.8,'sound')
    "Я кашляю и пару раз бью себя по рёбрам."
    "Это помогает."
    "Чуть отдышавшись, я делаю пару взмахов рукой."
    show kw smile_2_2 with dspr
    "Вебер насмешливо наблюдает за моими потугами."
    "Я осознаю, что нахожусь не в лучшем положении для атаки, и пячусь назад…"
    "Упираюсь в стену."
    "Карл сокращает дистанцию. Ребро его ладони направлено в область моей шеи."
    "Я инстинктивно поднимаю руку, пытаясь защититься…"
    hide kw with dspr
    play sound air_01
    $ volume(1.4,'sound')
    with hpunch
    play sound sfx_fall_grass
    extend " и в этот момент валюсь на пол, почему-то чувствуя острую боль в коленях."
    th "Обманул!.."
    th "Эх, недолгим получился бой…"
    stop music fadeout 2
    show kw smile_2 with dspr
    play ambience ambience_int_cabin_day fadein 2
    "Вебер склоняется надо мной и кладёт свою руку мне на грудь, чтобы я не пытался встать."
    kw "Вот и конец твоей истории, Семён.{w} И к чему это всё было?"
    kw "Чего ты добивался?"
    "Мои губы расползаются в улыбке."
    play sound sfx_open_door_2
    "Вдруг дверь с грохотом открывается."
    $ volume(1.8, "music")
    show sl scared pioneer far at right with dspr
    sl "Семён!"
    show kw surp with dspr
    "Менее чем за секунду она всё понимает и моментально бросается на Карла."
    hide sl with dspr
    "Её сил недостаточно, чтобы свалить его, но в самый раз, чтобы отвлечь."
    "Нескольких мгновений хватает, чтобы я высвободил правую руку и нанёс Карлу прицельный удар по сонной артерии."
    play sound air_01
    hide kw with dspr
    "С лёгким вздохом он валится на пол рядом со мной."
    "Я же никак не могу отдышаться."
    "Всё произошло так быстро…"
    "Я пытаюсь встать, но ничего не выходит."
    show sl surprise pioneer with dspr
    "Поднимаю глаза на Славю."
    th "Если бы не она…"
    "Славя не отводит от меня взгляда."
    hide sl with dspr
    scene cg sem_sl_room with dissolve
    me "Ты как здесь оказалась?"
    sl "Я нашла записку в книге и подумала, что Лена и есть маньяк!"
    sl "Ей нужна была третья жертва, и я подумала, что ты в опасности."
    sl "Прибежала в её домик, и увидела, что она…{w} она…"
    "У Слави на глазах выступили слёзы."
    sl "Вебер «нашёл» в библиотеке записку, которая предназначалась тебе…"
    sl "Лена отвлекла тебя, тем самым сорвав его план. Он показал мне книгу с запиской и сбежал."
    sl "Он думал, что я пойду прямиком к тебе и покажу записку, пока он сам разбирается с Леной. Думал, что у него больше времени…"
    sl "А Лена…{w} Я была неправа…"
    scene bg int_house_of_mt_day with dissolve
    show sl sad pioneer with dspr
    "Славя всхлипнула."
    sl "Но теперь всё закончилось."
    me "Да, теперь всё позади…"
    "Я обнял её."
    "Она была так близко…"
    "Она не отрываясь смотрела на меня, и её губы дрожали."
    "Я положил ей руку на шею и притянул к себе."
    hide sl with dspr
    "Наши губы встретились."
    "Оторвавшись от поцелуя, я увидел её милое лицо."
    show sl tender pioneer with dspr
    "Она смотрела на меня счастливыми глазами."
    me "Всё будет хорошо.{w} Отныне всё будет хорошо."
    "Я улыбнулся и двумя пальцами сдавил ей шею."
    hide sl with dspr
    "Её глаза потухли, и она обмякла у меня на руках."
    "Это был час моего триумфа."
    window hide
    stop ambience fadeout 2
    scene black with dissolve2
    $ renpy.pause (2,hard=True)
    jump epilogue_1



label epilogue_1:
    $ save_name = ('Схватка с неизвестностью.\nЧасть 3: Падение.')
    $ new_chapter(3, u"Схватка с неизвестностью.\nЧасть 3: Падение.")
    $ renpy.pause (2,hard=True)
    show partthree with dissolve
    $ renpy.pause (3,hard=True)
    hide partthree with dissolve
    $ persistent.sprite_time = 'night'
    $ night_time()
    scene bg darkroom_camp with dissolve
    play ambience ambience_catacombs fadein 2
    $ volume(0.7,'ambience')
    $ renpy.pause (0.5)
    show pi2 smile with dissolve
    me "Привет."
    me "Добро пожаловать в мой мир."
    play music hard_rock fadein 1
    $ volume(0.8, "music")
    $ renpy.pause (1.5,hard=True)
    show pi2 ser with dspr
    $ renpy.pause (3,hard=True)
    stop music fadeout 2
    show pi2 ser with dspr
    me "Нет, слишком громко."
    me "Неприятно."
    $ renpy.pause (1,hard=True)
    play music music_list["a_promise_from_distant_days"] fadein 2
    $ volume(0.7,"music")
    $ volume(0.2,"ambience")
    $ renpy.pause (2,hard=True)
    show pi2 smile with dspr
    me "А вот это уже лучше."
    me "Должно быть, у тебя есть ко мне пара вопросов?"
    me "С чего начнём?"
    $ renpy.pause (1,hard=True)
    window hide
    $ renpy.pause (0.5,hard=True)
    hide pi2 with dissolve
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene cg ext_park_evening with dissolve2
    me "Знаешь, а подкидывать записки себе самому было интересно."
    me "Даже приятно в какой-то мере."
    me "Ну а что?"
    me "Удивляешься.{w} Пугаешься.{w} Рассказываешь остальным."
    me "Они тоже пугаются. Создаётся атмосфера страха, разобщённости."
    me "«Кому доверять, если это мог быть любой?»"
    me "В общем, напусти в самую скучную жизнь немного тайны да приправь её мистикой, и всё заиграет новыми красками."
    me "И жить станет веселей. Не для всех, конечно, но всё же."
    scene black with dissolve
    scene bg ext_houses_night_7dl with dissolve
    me "Как думаешь, для чего я это всё затеял?"
    me "Какая в этом цель?"
    me "И есть ли она вообще?.."
    me "Даю время подумать.{w} Одно мгновение."
    $ renpy.pause (1,hard=True)
    me "Итак?"
    $ renpy.pause (1)

    label questionnaire_1:

    menu:

        "Обряд из книги":
            me "Правда?"
            me "В это действительно было так легко поверить?"
            me "И неужто даже мысли не возникло, что это уж как-то…{w} слишком странно?"
            me "Какой кошмар.{w} Я ожидал, что ваши интеллектуальные способности будут чуть повыше."
            me "Ну какой обряд? Какое бессмертие?"
            me "Мы же в 21-м веке живём!{w} Ну, кто-то из нас."
            me "И как эта книга вообще здесь оказалась? Я точно помню, что где-то видел её раньше."
            me "Где же…{w} Ну да ладно, чёрт с ней."
            me "Вообще, мне кажется, что успех идеи с обрядом объяснить очень просто: напряжённость и подозрение стали благоприятной почвой для подобных мыслей."
            me "Поверить было действительно просто. Даже слишком просто."
            me "Да, я достиг цели, но…"
            me "Я всё равно разочарован."
            jump questionnaire_1


        "Отправиться домой":
            me "Домой?"
            me "Только приехали и {i}уже домой?{/i}"
            me "Где же ваша хвалёная гостеприимность?"
            me "В общем, нет."
            me "Я не знаю, как отсюда выбраться.{w} Просто не знаю, и всё."
            me "Но ведь у всего есть финал, верно? Свой конец."
            me "Даже у вселенной, которая, вопреки распространённому мнению, далеко не бесконечна."
            me "Всё это очень похоже на сон, который почему-то не желает заканчиваться."
            me "Но почему?"
            me "Что я должен сделать, чтобы вновь вернуться в свою реальность?"
            me "Или это теперь и есть моя реальность…"
            me "Да, было бы определённо скверно."
            jump questionnaire_2


        "Я не знаю":
            me "Что, вообще нет идей?"
            me "Ну как же так?"
            me "Неужто за все эти дни не появилось ни одной мысли о том, к чему это всё идёт?"
            me "Нет?"
            me "Ну вот.{w} И к чему тогда вся моя игра?{w} Всё моё представление?"
            me "Нет, давай ты ещё чуть подумаешь."
            me "Я подожду."
            jump questionnaire_1

label questionnaire_2:
    me "А вообще, если говорить начистоту, то угадать мой мотив довольно сложно.{w} Практически невозможно."
    me "То же самое, что прочитать мысли в голове другого человека."
    me "Ты вот можешь?"
    me "Я — нет."
    me "А какое веселье могло бы получиться…"
    me "В общем, сейчас я поменяю музыку, и мы об этом поговорим."
    me "Знаешь, сколько у нас ещё времени?"
    me "{i}Очень и очень много.{/i}"
    stop music fadeout 2
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    play music snowflakes fadein 1
    stop ambience fadeout 2
    $ volume(1.8,"music")
    scene bg ext_musclub_night_7dl with dissolve2
    me "Скажи, а вот у тебя никогда не было такого желания — попасть в параллельный мир, где время застыло?"
    me "Предположим, что вход в него — нечто вроде окна.{w} Заходишь туда и смотришь на свой мир от третьего лица."
    me "Вот твоя комната. Часы висят на стене."
    me "А секундная стрелка на них не двигается — замерла."
    me "Ну как?"
    me "Не нравится?"
    me "А ты представь, как было бы здорово."
    me "Не успеваешь к дедлайну — зашёл в параллельный мир и всё сделал."
    me "Рано на работу — остановил будильник, сделал шаг в иную реальность и поспал как следует."
    me "Сказка!"
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg ext_polyana_night with dissolve
    $ renpy.pause (0.3,hard=True)
    me "Попав в лагерь, я сначала очень растерялся."
    me "«Что делать?», «Как выбраться?», «Почему я?»."
    me "Но, разобравшись, я знаешь что понял?"
    me "Что я тот самый PlayerUnknown."
    me "А это, соответственно, мои Battlegrounds."
    me "Ладно, это не совсем понятно. Давай иначе."
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg int_music_club_mattresses_night with dissolve
    me "Я не преследовал цели убить всех или что-то в этом духе."
    me "Я же не дурачок, в конце концов.{w} И не психопат."
    me "Даже оглушал всех очень аккуратно, по древней китайской методике."
    me "Кстати, вернуть всех к жизни можно за десять секунд!"
    me "Попробуешь? Я покажу как."
    me "Нет?"
    me "Ну ладно. Хозяин — барин."
    me "К тому же зачем нарушать столь сладостный момент?"
    me "Вообще, главной целью был, как ни странно, эксперимент."
    me "Можно ли создать столь вязкую и цепкую полосу лжи, чтобы в неё поверили все и, что наиболее важно, ты сам?"
    me "Вот не было у тебя вопроса, почему все записки приходили именно тебе? Почему всё это так или иначе связано с тобой?"
    me "Потому что ты главный герой?"
    me "Потому что так всегда и происходит?"
    me "Что за глупость."
    me "Всё произошло именно так, а не иначе, потому что так всё и было задумано изначально.{w} Потому что так надо."
    me "И ты поверил. {w}А как же иначе?"
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg ext_stage_normal_night with dissolve
    me "Так, если с целью мы разобрались, то самое время поговорить о средствах."
    me "На самом деле, тут уже всё сказано."
    me "Я просто убрал из своего «повествования» те места, где совершал режиссёрские действия."
    me "Например, писал записки, бросал их на видные места, рисовал знак в музкружке, ну и так далее."
    me "А что не так?{w} Кто сказал, что я должен докладывать всё без исключений?"
    me "Ну опустил пару деталей…{w} Что, ругать меня теперь за это?"
    me "Хотя да. Можно и поругать."
    me "В общем, здесь всё довольно просто."
    me "Вопросы?"

    $ renpy.pause (1,hard=True)
    label questionnaire_3:
    menu:
        "Нет":
            me "Плохо. Вопросы должны быть."
            me "Нет вопросов — стагнация."
            me "Стагнация — отсутствие развития."
            me "Попробуешь ещё?"
            jump questionnaire_3

        "Почему именно эти трое?":

            me "Действительно, почему?"
            me "Ольга Дмитриевна, Мику и Лена."
            me "Не поверишь, но ответ банален."
            me "Просто так."
            me "Да, вот так вот всё очевидно."
            me "Говорят, что просто так ничего не происходит.{w} Что всё имеет свою цель."
            me "Знаешь что?"
            me "Ерунда это всё."
            me "Вот понравились мне именно они — их я и выбрал."
            me "А может, подобраться к ним было легче всего?.."
            me "Хм."
            me "Тогда получается, что я всё-таки пошёл на поводу у здравого смысла."
            me "Об этом надо будет подумать. Времени хватит."
            jump questionnaire_4

label questionnaire_4:
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg ext_road_night2 with dissolve

    me "Я думаю, пора заканчивать. Ты как считаешь?"
    menu:
        "Пора":
            me "Хорошо."
            me "Мне тоже стало чуть скучновато."
            stop music fadeout 2
            jump final_ver_one

        "Поговорим ещё":
            me "Замечательно."
            me "Позволю задать вопрос тебе."
            me "А то только ты тут спрашиваешь. Как-то это нечестно."
            me "Скажи, как тебе вообще моя задумка, а?"
            me "Идея с записками, похищениями, атмосфера тайны? Нормально?"
            me "Согласись, что до этого в лагере было {i}сли-и-ишком{/i} уж скучно!"
            me "А, тебе, должно быть, не нравится подобное веселье…"
            me "Ну, что поделать. Мне понравилось."
            jump questionnaire_5

label questionnaire_5:
        scene black with dissolve
        $ renpy.pause (2,hard=True)
        scene bg ext_dining_hall_near_night with dissolve

        menu:
            "Хватит уже":
                me "Ладно, ладно."
                me "Переходим к десерту!"
                stop music fadeout 2
                jump final_ver_one

            "Что не так с Леной?":
                me "С Леной?"
                me "Всё с ней в порядке.{w} Просто я помог ей увидеть реальность."
                me "Так что «что-то не в порядке» здесь со всеми вами, а не с ней!"
                me "Это было слишком просто, правда."
                me "Пара задушевных разговоров тёмной ночью под луной…{w} Пара намёков…"
                with hpunch
                me "А потом — суровая и страшная правда! Как ведро холодной воды!"
                me "Вот что случается, когда, с одной стороны, на сто процентов уверен в своей правоте, а с другой — веришь человеку так сильно, что начинаешь сомневаться."
                me "К тому времени я имел на Лену такое влияние, что она поверила бы {i}всему{/i}, что я сказал."
                me "Например, что Солнце встаёт на Западе."
                me "Кстати!{w} А вдруг оно действительно там встаёт?"
                me "Ты подумай об этом. Ведь утверждают же некоторые, что Земля плоская."
                me "А вдруг они правы, м?"
                $ renpy.pause (1,hard=True)
                jump questionnaire_6

label questionnaire_6:
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg ext_backdoor_night_7dl with dissolve
    me "Ну что, поговорим ещё или хватит?"
    menu:

        "Хватит":

            me "Хорошо. Хватит так хватит."
            me "Переходим к последнему акту нашего представления!"
            stop music fadeout 2
            jump final_ver_one

        "Как ты обездвиживал жертв?":

            me "Это очень просто."
            me "Если честно, шприцы и мрачная обстановка нужны были только для запугивания. Ну и для эффекта, конечно."
            me "Обычный физраствор ещё никому не вредил."
            me "В общем, берёшь шею двумя пальцами вот так и несильно давишь."
            me "В зависимости от силы нажатия жертва проведёт следующие несколько часов — или дней — без сознания."
            me "Поводись с китайскими триадами пару лет — и не такому научишься."
            me "А знаешь, кто меня с ними познакомил?"
            me "Вебер, разумеется."
            me "Какие превратности судьбы, хе-хе."
            stop music fadeout 2
            jump final_ver_one

label final_ver_one:
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene bg darkroom_camp with dissolve
    play ambience ambience_catacombs fadein 2
    $ volume(0.4, "ambience")
    me "Эй! Подъём!"
    me "Это что же, они в отключке до сих пор?"
    me "Кому же я тогда говорил это всё?"
    me "…"
    me "Ладно, неважно."
    $ renpy.pause (1,hard=True)
    me "О, а вот и Карлуша проснулся!"
    me "Доброе утро!"
    play music ab_ovo fadein 2
    show kw sad with dspr
    kw "…"
    show kw with dspr
    kw "Ты хоть знаешь, что натворил?"
    me "Да брось.{w} Я же знаю, что тебе понравилось."
    me "Вся эта атмосфера опасности и таинственности… И не говори, что тебе это не по душе."
    kw "Одно дело — читать об этом в книгах…"
    kw "И совершенно другое — видеть всё в реальной жизни."
    kw "Ты что, совсем ничего не понимаешь?"
    me "«В реальной жизни»?"
    me "Ты называешь это {i}реальной жизнью{/i}?"
    me "Да это ты здесь сумасшедший, а не я."
    me "Сколько мы знакомы, я всегда был в твоей тени."
    me "Во всех начинаниях и делах ты был лучше меня. Умнее. Сообразительнее."
    me "Но согласись, что здесь я одержал победу."
    show kw disapp with dspr
    kw "О чём ты говоришь…"
    me "Интересно, почему ты не раскрыл меня раньше? Ты же обо всем уже давно догадался."
    me "Скажи, когда ты понял, что за всем стою именно я?"
    show kw with dspr
    kw "С самого начала."
    me "Вот как?"
    kw "Да.{w} Ну кто ещё, кроме тебя, здесь может владеть {i}такими{/i} навыками?"
    show kw smile with dspr
    kw "Не Славя же?"
    me "А ты, судя по всему, здорово к ней прикипел."
    show kw ser with dspr
    me "И что ты в ней такого нашёл?"
    me "Её же на самом деле нет. Как и всего этого мира."
    me "Она же ненастоящая."
    show kw disapp with dspr
    kw "Ошибаешься. Она как раз-таки и настоящая."
    show kw with dspr
    kw "В отличие от тебя."
    kw "Я не верю, что Семён, которого я знаю, способен на нечто подобное."
    kw "И никогда не поверю."
    me "Ты так и не открыл глаза?"
    kw "И не закрывал."
    kw "Записки.{w} Думал, я твой почерк не узнаю?"
    kw "Английский язык. Кто мне Шекспира месяц назад наизусть читал?"
    kw "Ну и, в конце концов, сама идея слишком уж… неправдивая."
    me "Ты просто завидуешь."
    show kw smile_2 with dspr
    kw "Немного, может быть."
    kw "Сыграно всё было как по нотам."
    show kw ser with dspr
    kw "Почему я не остановил тебя ранее?"
    kw "Хотелось понять весь смысл твоей игры."
    kw "Ну и посмотреть, как далеко ты сможешь зайти."
    me "Поэтому и «Психологию» стал читать?"
    show kw with dspr
    kw "Не только поэтому."
    kw "Но вообще да. И я всё-таки склоняюсь к тому, что у тебя не все дома."
    me "А у тебя-то все?"
    kw "Во всяком случае, я не душу людей от безделья."
    me "Ну да. Аргумент."
    me "Вообще, я удивился, увидев тебя здесь."
    me "Как ты это объяснишь?"
    kw "Просто."
    kw "В любой системе должно поддерживаться равновесие."
    kw "Ты каким-то образом попал сюда.{w} Как отрицательный персонаж."
    me "А ты, значит, положительный?"
    show kw smile_2 with dspr
    kw "Получается, что так."
    show kw with dspr
    kw "В общем, никто не сможет вернуться назад, пока один из нас не победит."
    kw "Твоя главная ошибка в том, что ты думаешь, что можешь навести тут бардак и вернуться чистым, будто ничего и не было."
    kw "Нулевой пункт на самом деле нисколько не нулевой."
    kw "Все твои действия в этой вселенной отразятся на реальности.{w} {i}Нашей реальности.{/i}"
    kw "Иначе во всём этом попросту бы не было смысла."
    me "Хм…"
    me "Интересная теория. Похлопаем."
    play sound clapclap
    $ renpy.pause (2,hard=True)
    show kw disapp with dspr
    $ renpy.pause (1,hard=True)
    me "И что же ты предлагаешь делать?"
    show kw with dspr
    kw "Ничего.{w} Решать тебе."
    kw "Честно говоря, мне самому интересно, какой выбор ты сделаешь."
    me "А есть из чего выбирать?"
    show kw with dspr
    kw "Всегда есть."
    me "Твоя теория…{w} Не нравится она мне."
    me "Не хочу в неё верить."
    me "А в этой вселенной возможно всё, и если я в это не верю, то этого и нет."
    show kw surp with dspr
    kw "Снова ты за старое."
    kw "Выбрось свой максимализм на помойку и просто поверь мне!"
    kw "Это всё не сон, чёрт возьми!"
    show kw disapp with dspr
    kw "Послушай, я понимаю, что за две недели здесь ты много всего себе напридумывал."
    kw "Понимаю, что я сам, возможно, не всегда был к тебе добр и лоялен."
    show kw with dspr
    stop music fadeout 2
    kw "Но это твоё последнее испытание. И от твоего выбора зависит очень многое."
    kw "Итак?"
    $ renpy.pause (1,hard=True)

    menu:
            "Вебер прав":
                jump foregone_conclusion

            "Это моя вселенная":
                jump ending_bad

label ending_bad:
    $ renpy.pause (2,hard=True)
    me "Нет."
    me "Не верю я тебе."
    play music just_memories
    me "Этот мир был создан для меня."
    me "Сам подумай: целых две недели я делал что хотел."
    me "Веселился как хотел."
    me "Да и вообще, все обстоятельства словно сами собой сложились как нельзя лучше для моего плана!"
    me "Совпадение? Не думаю."
    me "Я бы сказал, что это рай, но слишком уж он… иллюзорен."
    me "Но знаешь, что приятнее всего?"
    me "Я здесь хозяин."
    me "Ну вот кто мне может противостоять?{w} Ты?"
    me "Не смеши. Ты даже и пошевелиться не можешь, руки-то связаны!"
    show kw disapp with dspr
    kw "Ну и что ты будешь делать?"
    me "Что буду делать?"
    me "Сделаю этот мир своим.{w} Окончательно."
    me "К сожалению, здесь тебе нет места."
    me "Извини, Карлуша…"
    "…"
    $ renpy.pause (1,hard=True)
    hide kw with dissolve
    stop music fadeout 2
    stop ambience fadeout 2
    window hide
    scene black with dissolve2
    $ renpy.pause (2,hard=True)
    jump ending_bad_2

label ending_bad_2:
    $ prolog_time()
    scene black
    window show
    play music music_list["lightness_radio_bus"] fadein 3
    $ volume(0.7, "music")
    "…"
    th "М-м-м…"
    th "Какой странный звук…"
    th "Откуда он?"
    th "И как же шея затекла…"
    $ renpy.pause (1,hard=True)
    scene bg pi213_cabinet_night with dissolve
    "Я с усилием открываю глаза и вижу перед собой свой собственный стол."
    "Я перевожу взгляд с одного предмета на другой и осознаю, что нахожусь в кабинете, на своём рабочем месте."
    th "Как это я так…"
    "На часах показывает половину одиннадцатого."
    th "Неплохо так вздремнул — почти пять часов."
    scene bg widnow_outside_marked with dissolve
    $ renpy.pause (2,hard=True)
    stop music fadeout 2
    "Я встаю и закрываю окно. Музыки больше не слышно."
    "Взглянув  наружу, я вижу перед собой пустынную улицу. Неудивительно — кому нужно ночью бродить по консульскому кварталу?"
    th "Только бродягам-консулам…"
    "Улыбнувшись своей шутке, я собираюсь задёрнуть штору, как моё внимание привлекает нечто красное на асфальте."
    scene bg widnow_outside_marked_close with dspr
    "…"
    th "Чёрт!"
    scene bg pi213_cabinet_night with dissolve
    "Я мгновенно вспоминаю свой сон."
    "Вспоминаю его во всех деталях и подробностях, будто всё это было наяву."
    me "Что же я наделал?"
    "Пошатываясь, я дошёл до дивана и рухнул в его мягкие объятия.{w} Обхватил голову руками."
    me "Это не мог быть я. Ну просто не мог."
    me "Словно моя худшая сторона взяла контроль надо мной…"
    me "Будто весь здравый смысл был вытеснен сиюминутной идеей, фантомом!"
    me "Который издали был похож на неопровержимый факт, а на деле оказался пустышкой."
    window hide
    $ set_mode_nvl()
    "Я прокручивал в голове две недели, проведённые в лагере."
    "И ужасался тому, что сделал сам. Тому монстру, которого я сотворил внутри себя."
    $ renpy.pause (0.5)
    $ set_mode_adv()
    $ prolog_time()
    window show
    show prologue_dream
    show sl smile pioneer with dissolve
    show dv smile pioneer at right with dissolve
    "Я представляю лица Слави и Алисы."
    hide sl 
    hide dv
    show mi smile pioneer with dissolve
    show us smile pioneer at right with dissolve
    "Мику и Ульяны."
    hide mi
    hide us
    show el normal pioneer with dissolve
    "Даже Электроника с его магнитофоном!"
    hide el
    show kw with dissolve
    "И Вебера…"
    $ renpy.pause (1,hard=True)
    me "Вебер…"
    me "Я же его…"
    hide kw with dspr
    hide prologue_dream
    $ renpy.pause (0.5)
    with hpunch
    th "Нет! Этого не может быть!"
    th "Это был сон, и он закончился. Не счастливым концом, но закончился."
    th "Вебер сейчас дома, смотрит свой любимый снукер.{w} Или работает."
    me "Сейчас я ему позвоню. Он снимет трубку и скажет, что всё хорошо."
    me "Так всё и будет."
    "Я подхожу к столу и снимаю трубку старого телефона."
    play sound dialling
    "Набираю номер Карла и попутно думаю, что его присутствие в моём сне было очень уж странным."
    th "М-да… Мне действительно стоит перестать проводить с ним столько времени."
    "…"
    th "Недоступен?{w} Странно."
    th "Так, а какой сегодня день?{w} Пятница?"
    $ renpy.pause (1,hard=True)
    th "А, ну тогда всё ясно!"
    th "По пятницам Вебер получает инструкции из центра и выключает телефон."
    th "А я уж на мгновение испугался!"
    me "Чего только не напридумываешь себе…"
    $ volume(0.4, "sound")
    "Я улыбнулся и протянул руку за курткой."
    th "Надо будет завтра ему обо всём рассказать."
    th "Он, наверное, подумает, что я накурился.{w} Ну и пусть!"
    th "Я и сам подумать не мог, что внутри меня живёт такой злой гений."
    th "Сейчас-то об этом смешно вспоминать, конечно."
    scene bg corridor with dspr
    "Я выхожу из кабинета и вижу пустые коридоры."
    th "Ну а кому тут быть-то в такой поздний час?"
    th "Всё равно из прожжённых трудоголиков здесь только я да Вебер."
    "…"
    "Проходя мимо его кабинета, я вижу, что дверь чуть приоткрыта."
    th "Странно…"
    th "Он здесь?.."
    $ renpy.pause (1,hard=True)
    scene bg kw_office with dspr
    "Я осторожно захожу и вижу, что в кабинете никого нет."
    th "Уф, а я уж…"
    scene bg kw_office_close with dspr
    play music lonesome_summer
    "И замечаю Вебера, лежащего на диване."
    "Его глаза закрыты."
    th "Что с ним?!"
    th "Спит?"
    th "Он же рассказывал мне, как «заснул на работе и оказался в лагере»."
    "Я подхожу к нему и трогаю его за плечо."
    me "Вебер! Вставай!"
    "Но он не двигается."
    "Мною начинает овладевать страх."
    me "Карл! Очнись!"
    with hpunch
    me "Да что с тобой? Очнись!"
    "Я пробую перевернуть его на бок и случайно касаюсь шеи."
    "Она холодная…"
    "В двух местах красные следы… От пальцев."
    "{i}Моих пальцев{/i}."
    scene bg darkroom_camp with dissolve
    show prologue_dream
    show kw with dspr
    kw "У тебя ещё есть шанс всё исправить."
    kw "Нужно всего лишь сделать правильный выбор."
    me "Я знаю, что делать."
    me "Ты ничего не почувствуешь."
    $ renpy.pause (0.5,hard=True)
    show kw disapp with dspr
    $ renpy.pause (0.5,hard=True)
    kw "Видимо, это бесполезно."
    kw "Поступай, как считаешь нужным."
    $ renpy.pause (0.5,hard=True)
    hide prologue_dream
    scene bg kw_office with dspr
    $ renpy.pause (0.5,hard=True)
    me "Нет…"
    me "Это ведь было не по-настоящему!"
    me "Это было не здесь!"
    "Я срываюсь на крик."
    "Едва сдерживая слёзы, я сажусь на пол рядом с диваном."
    th "Я не мог сделать этого. Я спал."
    th "Получается, это был кто-то другой?"
    th "Но кто?"
    th "Что мне теперь делать?"
    "Я машинально тянусь за телефоном."
    "Не знаю зачем. Вряд ли он хоть как-то поможет мне."
    th "Да и кому я позвоню?"
    "Вместе с телефоном я достаю…"
    show note_guesswho with dissolve
    "…"
    hide note_guesswho
    play sound paper
    show note_guesswho_2 with dissolve
    "…"
    hide note_guesswho_2 with dissolve
    $ renpy.pause (0.5,hard=True)
    th "Невозможно."
    "Мои мысли роились. Понять что-то в этом хаосе было невозможно."
    th "Если во сне записки писал я сам, то кто же пишет их здесь?"
    window hide
    $ renpy.pause (2,hard=True)
    scene black with dissolve
    $ renpy.pause (2,hard=True)
    scene anim prolog_1 with dissolve 
    $ renpy.pause (1)
    "Пройдёт несколько мгновений перед тем, как я всё осознаю."
    "Я хочу окончить свою историю здесь.{w} Почему?"
    "Потому что впереди лишь боль и страдание."
    "Находясь во власти сна, я не знаю, где реальность, а где фантом."
    "Может ли быть, что всё, абсолютно всё из этого ложь?"
    "Что я всё ещё сплю и вижу себя со стороны?"
    "Что я навечно застыл в параллельном мире, в котором нет ничего, кроме обмана и фальши?"
    "Лишь время покажет."
    $ renpy.pause (2,hard=True)
    scene black with dissolve
    $ renpy.pause (1)
    scene bg tv_screen with dspr
    $ set_mode_nvl()
    "К экстренным новостям: сообщается, что в минувшую ночь в одном из представительств нашего государства за рубежом произошла утечка газа."
    "Жертвами стали вице-консул и его помощник. Как утверждают их коллеги, они задержались на рабочем месте, проводя анализ концепции внешней политики, которая, как известно, была опубликована два дня назад."
    "Рок настиг их во сне. Их тела скоро будут доставлены на родину."
    "Руководитель представительства дал комментарий, в котором особо отмечал талант и усердие погибших сотрудников."
    "Причины утечки остаются неизвестными. Наиболее вероятная — авария на служебной станции."
    nvl clear
    "Мы выражаем соболезнования семьям погибших и скорбим вместе с ними."
    "Детали происшествия будут выясняться."
    nvl clear
    $ renpy.pause (0.5)
    scene black with dissolve2
    $ renpy.pause (2,hard=True)
    call screen menu_mist_1337


label foregone_conclusion:
    $ volume(0.7,'ambience')
    me "Ну…"
    with hpunch
    me "Чёрт возьми!"
    me "Ты заставил меня сомневаться!"
    me "Опять свои психологические штучки используешь, да?"
    show kw smile_2 with dspr
    kw "Ловкость рук — и никакого мошенничества."
    me "А?.."
    hide kw with dspr
    $ renpy.pause (0.5,hard=True)
    with hpunch
    $ renpy.pause (0.5,hard=True)
    play music happy_ending_melody fadein 3
    stop ambience fadeout 2
    $ volume(0.8, "music")
    me "Эй, слезь с меня!"
    me "Когда только руки успел развязать?"
    show kw smile_2 with dspr
    kw "Пока ты туда-сюда ходил и что-то бормотал, времени было более чем достаточно."
    show kw thinking with dspr
    kw "Прекрати брыкаться!"
    kw "Если я тебя перестану держать, ты обойдёшься без глупостей?"
    me "Да, да! Только отпусти, мне нечем дышать!"
    show kw smile with dspr
    kw "Ну, вот так хорошо."
    me "…"
    show kw ser with dspr
    kw "Ну так что? Веришь мне или нет?"
    me "Если я скажу, что не верю, снова бросишься?"
    kw "Да."
    me "Тогда не верю."
    show kw thinking with dspr
    $ renpy.pause (1,hard=True)
    me "Ну ладно, ладно!"
    me "Ты реален."
    me "Если про других я не уверен, то насчёт тебя нет никаких сомнений."
    me "Кто ещё мог одолеть меня в тайцзицюань так легко?"
    show kw smile_2 with dspr
    kw "А ты что там доходяга, что здесь…"
    with hpunch
    me "Чего?!"
    show kw smile_2_2 with dspr
    kw "Но доходяга особенный.{w} Свой."
    me "Ты прощён."
    show kw ser with dspr
    kw "Ты, может, приведёшь в чувство всех остальных?"
    kw "Лену, Славю, Мику и вожатую?"
    me "А может, так их и оставим?"
    me "Пусть поспят. Спать полезно."
    show kw thinking with dspr
    kw "Так."
    me "Ла-адно! Ты только не нервничай."
    me "И да, почему ты сам их не разбудишь? Ты же знаешь, куда нажимать."
    show kw with dspr
    kw "Я предпочту, чтобы это сделал ты."
    me "А есть разница?"
    kw "Конечно."
    show kw ser with dspr
    kw "А мы где находимся, кстати? В старом лагере?"
    me "Да. Где же ещё?"
    kw "Ну, неудивительно. Близко, плюс никто не сунет носа."
    me "Место замечательное. А слухов-то сколько ходит…"
    me "Порой их бывает достаточно, чтобы придумать то, чего на самом деле нет."
    show kw with dspr
    kw "Погоди. Ты как нас всех сюда перетащил?"
    me "Не напоминай. Пришлось ходить пять раз."
    me "Пять! Представь только!"
    me "И это после того, как ты мне навалял!"
    me "Не стыдно тебе?"
    show kw ser with dspr
    kw "А мне должно быть стыдно?"
    kw "Даю ровно три секунды спрятаться."
    me "Так, тихо! Иначе ошибусь и бедная Лена навсегда останется во власти сновидений."
    me "Как думаешь, что ей может сниться?"
    show kw smile with dspr
    kw "Призрак в доспехах."
    me "Что?"
    kw "Что?"
    me "…"
    $ renpy.pause (1,hard=True)
    me "Вроде всё…"
    me "Они должны прийти в себя минут через пять."
    show kw smile_2 with dspr
    kw "Есть идеи, что им скажешь?"
    me "Эм…{w} «Сюрприз»?"
    kw "Лучше уж тогда молчи."
    show kw ser with dspr
    kw "Кстати, а почему я пришёл в сознание?"
    kw "Ты же всем одинаково шею сжимал, наверное?"
    me "Не знаю."
    me "Может, у тебя сопротивляемость высокая?"
    kw "Не говори глупостей. К {i}этому{/i} сопротивляемость у всех одинаковая."
    me "Ну тогда не знаю."
    me "Может, я тебе полегче сжал?"
    show kw smile_2 with dspr
    kw "Да? А с чего это вдруг такая милость?"
    me "Ну…{w} Ты же мне друг."
    show kw smile_2_2 with dspr
    kw "Друг, говоришь?"
    me "А что, нет?"
    hide kw with dspr
    $ renpy.pause (2,hard=True)
    with hpunch
    me "Эй, ты что, меня обнимаешь? Прекрати!"
    me "Прекрати немедленно!"
    window hide
    stop music fadeout 2
    stop ambience fadeout 2
    scene black with dissolve2
    $ renpy.pause (2,hard=True)

    scene black
    $ prolog_time()
    play music music_list["lightness_radio_bus"] fadein 3
    $ volume(0.7, "music")
    $ renpy.pause (2,hard=True)
    window show
    "…"
    th "М-м-м…"
    th "Какой странный звук…"
    th "Откуда он?"
    th "И как же шея затекла…"
    scene bg pi213_cabinet_night with dspr
    "Я с усилием открываю глаза и вижу перед собой свой собственный стол."
    "Перевожу взгляд с одного предмета на другой и осознаю, что нахожусь в кабинете, на своём рабочем месте."
    th "Как это я так…"
    "Часы показывают половину одиннадцатого."
    th "Неплохо так вздремнул — почти пять часов."
    scene bg widnow_outside_marked with dspr
    stop music fadeout 2
    "Я встаю и закрываю окно. Музыки больше не слышно."
    "Взглянув наружу, я вижу перед собой пустынную улицу. Неудивительно — кому нужно ночью бродить по консульскому кварталу?"
    th "Только бродягам-консулам…"
    "Улыбнувшись своей шутке, я собираюсь задёрнуть штору, как моё внимание привлекает нечто красное на асфальте."
    scene bg widnow_outside_marked_close with dspr
    "…"
    th "Чёрт!"
    scene bg pi213_cabinet_night with dspr
    "Я мгновенно вспоминаю свой сон. Вспоминаю его во всех деталях и подробностях, будто всё это было наяву."
    th "А говорили, что сны забываются…"
    th "Это правда тот самый знак?"
    scene bg widnow_outside with dspr
    "Я разворачиваюсь обратно к окну."
    "Ничего нет.{w} Знак будто испарился."
    "А был ли он вообще?"
    th "Что за чертовщина…"
    scene bg pi213_cabinet_night with dspr
    "Пошатываясь, я дошёл до дивана и рухнул в его мягкие объятия."
    "Обхватил голову руками."
    me "Что же я наделал?"
    me "Как только я мог додуматься до такого? Записки, похищения…"
    me "И ради чего? Просто так?"
    me "Ну и идиот…"
    show prologue_dream
    "Я принялся вспоминать всё, что произошло со мной в лагере за те две недели."
    "Передо мной словно наяву возникали лица пионеров."
    show un smile pioneer with dspr
    show dv smile pioneer at right with dspr
    "Лена и Алиса."
    hide un
    hide dv
    show el normal pioneer with dspr
    "Электроник и его магнитофон."
    "Не знаю почему, но магнитофон запомнился мне особенно отчётливо."
    hide el with dspr
    show sl smile2 pioneer with dspr
    "Славя."
    th "Как же мерзко я с ней поступил.{w} И ведь не извинишься даже."
    th "Пожалуй, она единственная из лагеря, кого я могу назвать другом. Настоящим другом."
    th "Ведь у меня, кроме Вебера, и друзей-то нет."
    th "А Вебер…"
    hide sl
    scene bg darkroom_camp with dissolve
    show prologue_dream
    "Я внезапно вспоминаю про Карла.{w} Он ведь тоже был в том сне!"
    "Вспоминаю наш поединок…"
    "Разговор в старом лагере…"
    "Как он удержал меня от пропасти."
    "Достаточно было сделать маленький шаг — и путь назад был бы навсегда потерян."
    scene bg pi213_cabinet_night with dissolve
    play music bestfriends fadein 1
    th "Интересно, а он тоже…"
    "Не успев додумать мысль до конца, я срываюсь с места."
    scene bg corridor with dspr
    th "Быстрее, быстрее! В его кабинет!"
    play sound sfx_open_door_2
    scene bg kw_office with dspr
    $ volume(1.4, "sound")
    "Я с грохотом открываю дверь и едва ли не влетаю в комнату."
    show kw_tie with dspr
    "Вебер сидит на диване и зевает."
    me "Карлуша!"
    kw "!!!"
    with hpunch
    hide kw_tie with dspr
    "Одним прыжком я достигаю дивана и падаю на Вебера."
    kw "Что с тобой, чёрт тебя дери? Веди себя прилично!"
    "Он пытается освободиться, но я вцепился в него мёртвой хваткой."
    show kw_tie with dspr
    me "Как же я рад, что с тобой всё в порядке!"
    kw "А что со мной может быть не в порядке?"
    kw "И что ты здесь делаешь так поздно?"
    show kw_tie disapp with dspr
    kw "Ладно, раз уж ты здесь, то поставь чайник."
    kw "Я едва проснулся. Ничего не соображаю пока."
    show kw_tie with dspr
    kw "Скажи в понедельник секретарям, чтобы выходили курить на улицу."
    kw "Их махорку нюхать просто невозможно!"
    kw "Чего они такого, интересно, туда добавляют, что всякая ерунда снится потом?"
    me "Ерунда?"
    me "Что тебе приснилось?"
    "Вебер изо всех сил старается выглядеть серьёзным, но не выдерживает и улыбается."
    show kw_tie smile with dspr
    kw "Ты не поверишь.{w} Что я снеговик в космосе."
    kw "Это как вообще?"
    kw "Пора переставать так много работать."
    me "Знаешь, мне тоже приснился сон."
    me "Очень интересный."
    kw "Куда уж интереснее моего?"
    me "О, намного интереснее.{w} Ты там был!"
    show kw_tie with dspr
    kw "Да? И что я в нём делал?"
    me "Спасал мир."
    me "И меня."
    window hide
    scene black with dissolve2
    stop ambience fadeout 2
    $ renpy.pause (2,hard=True)
    call screen menu_mist_1337_good
    $ renpy.pause (2,hard=True)










label mist1337_bonus_story:
    $ save_name = ('Схватка с неизвестностью.\nБонусная история.')
    $ new_chapter(3, u"Схватка с неизвестностью.\nБонусная история.")
    stop music fadeout 2
    stop ambience fadeout 2
    $ renpy.pause (2,hard=True)
    show bonus_cover with dissolve
    $ renpy.pause (2,hard=True)
    hide bonus_cover with dissolve
    scene black with dissolve
    window show
    "Действие происходит за полгода до основных событий."
    window hide
    $ persistent.sprite_time = 'day'
    $ day_time()
    $ renpy.pause (1,hard=True)
    scene bg pi213_cabinet with dissolve
    window show
    $ renpy.pause (0.5,hard=True)
    th "Хм…"
    th "И как люди вообще это едят?!"
    play music association fadein 4
    $ renpy.pause (0.5,hard=True)
    "Обед."
    "Я сижу на работе и уныло тыкаю вилкой в макароны прозрачного цвета."
    "Коллега вернулся из Японии и угостил {i}фунчозой{/i}."
    th "Блин, лучше бы кота с качающейся лапкой привёз!"
    play sound making_tea
    $ renpy.pause (0.3,hard=True)
    "Я наливаю чая и решаю предпринять последнюю попытку."
    th "Давай, ты сможешь! Это всего лишь лапша.{w} Даже не острая."
    th "Хотя лучше была бы острой…"
    "…"
    $ renpy.pause (2,hard=True)
    with hpunch
    th "Нет, хватит!"
    th "Через дорогу есть отличная чифанька, где всего за 13 юаней полноценный обед из риса, риса и риса."
    th "Ведь что может быть лучше риса в Китае?"
    "Я радостно отодвигаю тарелку и накидываю куртку."
    scene bg corridor_day with dissolve
    col "Сём, ты куда?"
    me "На обед!"
    col "Прихватишь мне тоже чего-нибудь? У нас тут еды вообще не осталось."
    me "О’кей."
    me "У меня в кабинете целый пакет фунчозы, кстати.{w} Можешь забрать."
    col "Да? А тебе не нужна, что ли?"
    me "Не, мне она не понравилось. Безвкусная какая-то."
    col "Безвкусная?"
    col "А ты соус положил?"
    stop music fadeout 2
    $ renpy.pause (1,hard=True)
    me "А там был соус?"
    col "…"
    me "…"
    $ renpy.pause (1,hard=True)
    play music association fadein 2
    me "Она в шкафу, короче."
    col "Замётано."
    scene bg consular_street with dissolve
    play ambience street_consular fadein 2
    $ volume(0.7, "music")
    "Выйдя на улицу, я полной грудью вдохнул свежий шанхайский воздух."
    th "Свежий? В центре города?"
    th "Да! В центре города!"
    "Ещё неделю назад я заметил, что начал говорить сам с собой.{w} Пусть даже в мыслях, но всё же."
    "А что случилось неделю назад?"
    "Ну…{w} Ничего особенного."
    "Наступила середина октября."
    "Я получил нагоняй от начальства за ночёвки на работе.{w} Премию тоже получил, кстати."
    "Вебер уехал в командировку."
    th "Хм…"
    th "Да нет, я бы не сказал, что мне скучно и не с кем поговорить.{w} Или сказал бы?"
    th "Есть ли смысл себя обманывать?"
    "Я кашляю и собираюсь перебежать через дорогу…"
    stop music fadeout 2
    play sound_loop watchoutforthepolice
    $ volume(0.4,'sound_loop')
    $ renpy.pause (0.5,hard=True)
    extend " как вдруг слышу полицейскую сирену!"
    me "???"
    "Через несколько секунд замечаю мотоцикл."
    "Он несётся на большой скорости, удирая от двух полицейских машин."
    th "Что за лихач…"
    "Я решаю подождать и дать ему проехать."
    play sound bike_around
    $ volume(1.2,'sound')
    "Он проезжает мимо меня и тормозит прямо у входа в консульство."
    me "Так."
    me "Опять какой-то пьяный соотечественник?"
    me "Хотя вроде не вечер же ещё.{w} Кто ж в обед так сильно заряжается?"
    "Я с интересом наблюдаю за мотоциклистом."
    stop sound_loop
    "Полицейские останавливаются неподалёку и выходят из машин, держа в руках пистолеты."
    "Впереди идёт человек в военной форме."
    th "Кажется, дело серьёзное…"
    show pol at left with dissolve
    pol "Немедленно повернитесь и снимите шлем."
    pol "Вы хоть представляете, с какой скоростью ехали?"
    waeber_unknown "Да-да…{w} И что с того?"
    "Человек со вздохом снимает шлем…"
    show kw jacket at center with dissolve
    $ renpy.pause (0.3,hard=True)
    with hpunch
    me "???"
    "Теперь вздыхает уже полицейский."
    show pol disapp with dspr
    pol "Господин вице-консул, ну сколько можно?"
    pol "Вы думаете, у нас больше дел нет, кроме как за вами по всему Пудуну гоняться?"
    show kw jacket sad with dspr
    kw "Так могли бы и не гоняться."
    show pol with dspr
    pol "А если бы вы сшибли кого-нибудь?"
    show kw jacket thinking with dspr
    kw "Бросьте.{w} Знаете же, что не сшиб бы."
    show kw jacket disapp with dspr
    "Вебер склонил голову."
    kw "Сержант Ван, примите мои глубочайшие извинения.{w} Случай был очень срочный."
    kw "Больше не повторится."
    pol "Господин Вебер, иногда мне кажется, что вы пользуетесь своим дипломатическим иммунитетом сверх меры."
    pol "Боюсь, мне придётся доложить об этом в департамент."
    "Вебер поклонился."
    show kw jacket with dspr
    kw "Хорошего вам дня, господа."
    hide pol with dissolve
    "Полицейские чуть поворчали, сели в машины и уехали."
    "Я подошёл поближе."
    me "Ну и что это было?"
    kw "Где?"
    me "Только что."
    show kw jacket smile_2_2 with dspr
    kw "Я ничего не заметил.{w} А что-то было?"
    kw "Расскажешь?"
    show kw jacket smirk with dspr
    play music chilmilk fadein 3
    $ volume(0.65, "music")
    $ volume(0.7,'ambience')
    "Вебер не выдерживает и начинает хохотать."
    "Я тоже не могу сохранять серьёзное выражение лица."
    me "Я думал, ты вернёшься только послезавтра!"
    show kw jacket with dspr
    kw "Я тоже так думал. Но обстоятельства…"
    me "Стоило так торопиться?"
    kw "Разумеется, стоило.{w} Дело чрезвычайной важности."
    me "Да? Какое же?"
    kw "Ну как же…"
    show kw jacket smile_2 with dspr
    kw "Увидеть моего дорогого друга!"
    kw "Иди сюда, обнимемся!"
    "Я уклоняюсь от его распростёртых объятий."
    me "Тебя всего неделю не было.{w} Неделю, не месяц!"
    me "И ты крупно попал, в курсе?"
    me "Если этот Ван действительно доложит наверх, у тебя могут быть неприятности.{w} Причём серьёзные, судя по его тону и форме."
    kw "Не бойся, не доложит."
    me "Да? А с чего такая уверенность?"
    kw "Ну…"
    show kw jacket smile_2_2 with dspr
    kw "Может быть, с того, что он брат моей жены…"
    me "Какой жены?!"
    me "Это ты за неделю успел?"
    show kw jacket smile_2 with dspr
    kw "Ладно, ладно, успокойся.{w} Мы с ним давние знакомые, вот и всё."
    kw "Ещё до того, как я приехал сюда."
    kw "Держался он сурово потому, что ты стоял и смотрел."
    kw "Ну и его ребята ещё были рядом."
    show kw jacket ser with dspr
    kw "А если честно, мне крупно повезло. На въезде в город мог бы стоять и не Ван."
    kw "А медленнее ехать было нельзя."
    kw "Ты куда направляешься, кстати? Обедать?"
    me "Да.{w} Пойдёшь со мной?"
    kw "Я бы рад, но вот эту штуку надо срочно доставить начальству."
    "Вебер достал из багажника мотоцикла небольшую деревянную коробочку."
    me "Думаю, спрашивать, что внутри, нет смысла?"
    show kw jacket smile_2 with dspr
    $ renpy.pause (2,hard=True)
    me "Судя по твоему виду, так и есть…"
    show kw jacket smile with dspr
    kw "Если согласишься подождать несколько минут, я к тебе присоединюсь."
    me "Две минуты.{w} Перерыв не резиновый."
    show kw jacket smile_2 with dspr
    kw "Пять."
    me "Остановимся на четырёх?"
    kw "Годится."
    hide kw with dspr
    play sound steps
    $ volume(0.7,'sound')
    "Вебер исчезает в здании."
    "Я прислоняюсь к гранитному забору."
    "Надо найти какое-нибудь дело на четыре минуты."
    "…"
    "…"
    "Точно! Телефон же есть."
    show phone_01 with dspr
    th "Та-ак…"
    th "О, какое-то сообщение…"
    hide phone_01
    show phone_02
    th "Непонятно от кого…"
    th "Посмотреть, что ли?"
    hide phone_02
    show phone_03
    th "Ссылка на imgur…"
    hide phone_03
    show phone_04
    stop music fadeout 2
    th "???"
    th "Кто это?"
    th "Лучше уж Твиттер полистать…"
    show kw spring smile_2 with dspr
    kw "Я готов, пошли."
    me "Как ты так быстро?!"
    me "И переодеться успел?"
    me "Ты вообще нормальный человек?"
    kw "Пошли уже, оставь телефон в покое."
    hide phone_04 with dspr
    hide kw with dspr
    window hide
    stop ambience fadeout 2
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (0.5,hard=True)
    scene bg shanghai_str with dissolve
    $ renpy.pause (0.5,hard=True)
    play ambience street_crowded fadein 3
    $ volume(0.4,'ambience')
    window show
    "Мы вышли на Банд и направились к знакомой харчевне."
    show kw spring with dspr
    kw "Скажи, Семён…{w} Тебя никогда не интересовал Сучжоу?"
    me "Сучжоу?"
    me "Ни разу там не был, но многое слышал. Вроде хороший город."
    me "Старинный, красивый. Много воды."
    me "А что?"
    kw "Ничего…"
    kw "Как насчёт… "
    show kw spring smile_2 with dspr
    extend "Съездить туда?"
    me "В принципе, можно. А когда?"
    me "На выходных?"
    show kw spring smile_2_2 with dspr
    kw "Нет. {w}Прямо сейчас."
    with hpunch
    me "Что?"
    me "Сейчас?"
    me "Ты, видимо, тоже голоден, раз такие мысли в голову лезут."
    me "Ничего, сейчас поедим, и всё пройдёт."
    me "Смотри: сколько пальцев я показываю?"
    show kw spring with dspr
    me "Ну сколько?"
    show kw spring ser with dspr
    me "Ты посчитать не можешь?"
    kw "Три пальца, три."
    kw "…"
    kw "Лучше б ты такие знаки на рок-концертах показывал."
    kw "Так поедешь?"
    me "Да как мы поедем-то? Мы на службе, вообще-то!"
    kw "Я тебя уже отпросил. Заявление вчерашним числом уже лежит у советника в кабинете."
    me "…"
    me "То есть ты написал от моего лица заявление и даже не спросил меня самого?"
    show kw spring smile_2_2 with dspr
    $ renpy.pause (0.8,hard=True)
    me "Ну да, чего я удивляюсь…"
    $ renpy.pause (0.5,hard=True)
    scene black with dspr
    $ renpy.pause (0.5,hard=True)
    scene bg shanghai_str with dspr
    $ renpy.pause (0.5,hard=True)
    "Я взял баоцзы, Вебер — чашку риса, и мы расположились за столом на улице."
    show kw spring with dspr
    me "А что нам там делать?"
    me "Это как-то связано с твоей командировкой и тем… деревянным ящиком?"
    show kw spring smile_2 with dspr
    kw "Это называется шкатулка, Семён.{w} Шкатулка."
    me "…"
    me "А вообще, почему ты раньше не предупредил? Я бы хоть сходил вещи собрал."
    kw "Мы на день. Завтра вечером вернёмся."
    me "А какой смысл ехать на день?"
    $ renpy.pause (0.5,hard=True)
    me "А, я понял! Тебе просто надо что-то передать!"
    me "А одному ехать лень, и поэтому ты берёшь меня с собой!"
    me "Раскусил!"
    show kw spring smile_2_2 with dspr
    kw "Да-да, молодец."
    me "Кстати."
    me "Как поедем?"
    kw "Лучше всего будет на поезде. Всего час в одну сторону."
    me "Эх… А я ожидал увлекательное путешествие на мотоцикле."
    show kw spring with dspr
    kw "На мотоцикле как-нибудь в другой раз."
    kw "Вдвоём мы, конечно, влезем, но будет жутко неудобно."
    kw "Ладно, ты пока доедай, мне нужно позвонить."
    hide kw with dspr
    th "Вот так вот."
    th "Утром был обычный день.{w} Всё шло своим чередом."
    th "И вдруг, словно из ниоткуда, появляется вот этот вот господин и рушит все мои планы!"
    th "Планы…{w} Которых у меня не было?"
    th "Ну ладно. По крайней мере, скучно точно не будет."
    $ renpy.pause (0.5,hard=True)
    scene black with dspr
    $ renpy.pause (0.5,hard=True)
    scene bg shanghai_str with dspr
    $ renpy.pause (0.5,hard=True)
    "Я проглотил последний пельмешек и поднялся."
    show kw spring with dspr
    kw "Ты всё?"
    me "А ты всё?"
    kw "Давно уже."
    me "Ты сегодня какой-то необычайно быстрый."
    me "Тебя зовут Гонзалес?"
    show kw spring thinking with dspr
    kw "Шутка категории Б."
    me "Улыбнись!{w} Что по телефону сказали?"
    show kw spring disapp with dspr
    kw "Напомнили, чтобы завтра во второй половине дня мы уже были здесь."
    kw "Вечером должен приехать какой-то важный чиновник…{w} И угадай, кто поедет в аэропорт его встречать?"
    me "Ты?"
    show kw spring with dspr
    kw "Мы. Вместе поедем."
    kw "Придётся брать машину. Втроём на мотоцикл мы точно не уместимся."
    kw "Ну ладно, ты готов?"
    me "Да."
    kw "Тогда на вокзал."
    stop music fadeout 2
    stop ambience fadeout 2
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg railway_station_1 with dissolve
    $ renpy.pause (0.5,hard=True)
    play ambience city_traffic fadein 2
    window show
    "Мы вышли из метро и оказались у здания вокзала Хунцяо."
    me "Ты знаешь, где билеты покупать?"
    show kw spring with dspr
    kw "Нам не нужно."
    me "Как не нужно?"
    hide kw with dspr
    $ renpy.pause (0.5,hard=True)
    show kw spring with dspr
    "Вебер вздохнул и извлёк из кармана два билета."
    kw "Паспорт, надеюсь, с собой?"
    me "…"
    "Я взял у него билет и посмотрел дату покупки."
    "Вчера в 11 вечера."
    "…"
    me "Как это понимать?"
    show kw spring smile_2_2 with dspr
    kw "Как хочешь."
    me "…"
    me "Раз ты такой умный, скажи, где тут на поезд садиться?"
    show kw spring ser with dspr
    kw "Ты что, ни разу не выезжал из Шанхая?"
    me "Нет. А зачем?"
    me "Мне и здешнего шума более чем хватает."
    kw "М-да…"
    kw "Отсутствует у тебя аллель авантюризма…"
    with hpunch
    me "А-а?"
    show kw spring smile_2 with dspr
    kw "Пошли."
    stop ambience fadeout 2
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (0.5,hard=True)
    window show
    "Мы зашли в здание вокзала и без труда нашли нужную платформу."
    "Вернее, Вебер нашёл. Я всё время попросту шёл за ним."
    "Видимо, он довольно часто здесь бывает."
    window hide
    $ renpy.pause (0.5,hard=True)
    scene bg train_inside with dissolve
    $ renpy.pause (0.5,hard=True)
    play ambience train_moving fadein 2
    window show
    "Мы сели в вагон и почти сразу поехали."
    me "Это что, первый класс?"
    show kw spring smile_2 with dspr
    kw "Тебе твоя зарплата позволяет ездить первым классом?"
    kw "Везучий ты!"
    me "Прекрати меня уже троллить!"
    me "Сложно найти другое развлечение?"
    show kw spring smile_2_2 with dspr
    kw "Да брось, тебе же нравится."
    me "Нравится?"
    me "Я, по-твоему, мазохист?"
    kw "А разве нет?"
    hide kw with dspr
    "Я сердито ткнул Вебера в плечо и отвернулся."
    th "Заставил ехать чёрт знает куда…{w} И смеётся ещё!"
    th "И зачем я вообще согласился?"
    "Я достал из кармана телефон и наушники."
    play music loved_ones fadein 2
    $ renpy.pause (1,hard=True)
    $ volume(0.3, "ambience")
    $ renpy.pause (2,hard=True)
    th "Так-то лучше."
    $ renpy.pause (2,hard=True)
    stop ambience fadeout 2
    "Шум поезда постепенно умолк, растворившись в мелодии."
    th "Поистине, «chill lo-fi» — лучшее, что могло случиться с музыкой."
    th "Вся история развития вселенной и человечества была дорогой именно к этому."
    show blink
    "Я закрыл глаза."
    $ renpy.pause (0.5,hard=True)
    th "Интересно, зачем вообще мы едем?"
    th "Какое-то тайное / секретное / страшное партийное задание?"
    th "Хм…"
    th "В Сучжоу-то?"
    th "Да чего там может быть секретного?"
    th "Кто-то украдёт всю воду в городе?"
    th "…"
    $ renpy.pause (2,hard=True)
    th "Какие странные мысли…"
    th "Я, похоже, это…"
    $ renpy.pause (2,hard=True)
    stop music fadeout 2
    $ renpy.pause (2,hard=True)
    with hpunch
    kw "Эй, вставай!"
    show unblink
    scene bg train_inside with dspr
    show kw spring
    "Я резко открываю глаза и непонимающе смотрю перед собой."
    me "Ты кто?"
    show kw spring thinking with dspr
    kw "Ты всегда засыпаешь в дороге, да?"
    kw "В прошлый раз, когда мы ездили в Гонко…"
    with hpunch
    stop music fadeout 2
    me "Совпадение!"
    me "Да и не спал я! Глаза просто прикрыл на мгновение!"
    show kw spring smile_2 with dspr
    kw "В самом деле?"
    kw "Надо будет запомнить, что мгновение у тебя…"
    "Вебер взглянул на часы."
    kw "Ровно 47 минут."
    kw "Мы приехали, кстати."
    "Я только сейчас огляделся и понял, что поезд не едет и в вагоне мы одни."
    me "Ну пошли, чего ждём-то?"
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (0.5,hard=True)
    scene bg su_railway with dissolve
    play ambience street_consular fadein 3
    window show
    "Мы вышли на улицу."
    me "И куда дальше?"
    show kw spring ser with dspr
    "Вебер достал телефон и принялся что-то смотреть."
    me "Сириусли? А так ты не помнишь?"
    show kw spring with dspr
    kw "Нет."
    me "Нет?"
    kw "Нет, конечно. У меня же голова, а не компьютер."
    kw "Ладно, пошли оставим вещи в отеле."
    kw "Стало как-то пасмурно. Не пошёл бы дождь."
    me "Имеешь в виду {i}вот эти{/i} вещи?"
    "Сказал я, показав на небольшую кожаную сумку, которую Вебер держал в руке."
    kw "Ну да. Таскаться с ней повсюду не очень удобно."
    kw "Вызови такси."
    me "А чего я-то?"
    kw "У тебя приложение на телефоне."
    me "А у тебя нет?"
    kw "У меня мотоцикл."
    me "Ар-р-ргх…"
    "Больше аргументов у меня не было."
    $ renpy.pause (1.5,hard=True)
    scene bg su_str with dissolve
    "Такси привезло нас на какую-то широкую улицу."
    th "И это центр города?"
    th "Мало чем отличается от Шанхая…"
    show kw spring ser with dspr
    kw "Не волнуйся. Зелень, вода и мосты ждут впереди."
    me "???"
    me "Моя мысль была настолько очевидной?"
    show kw spring smile_2 with dspr
    kw "Разумеется."
    kw "Исторический и экономический центры находятся в совершенно разных районах."
    kw "Сейчас мы в экономической части."
    me "Спасибо, капитан! Я понял!"
    hide kw with dspr
    "Мы выбрались из машины, и Вебер показал рукой куда-то вперёд."
    show kw spring with dspr
    kw "Нам чуть дальше."
    me "Ты любитель прогуляться?"
    kw "А ты нет?"
    me "Нет."
    kw "А зачем тебе шагомер в телефоне?"
    me "Чтобы лишнего не нагулять, балбес!"
    kw "Смотри. Потолстеешь."
    me "Не страшно."
    $ renpy.pause (1,hard=True)
    scene bg su_ppl with dissolve
    "Мы прошли до конца улицы и встали у перекрёстка."
    me "Нам прямо?"
    me "И почему здесь столько мотоциклистов?"
    show kw spring disapp with dspr
    kw "Это мопеды."
    me "Всё равно."
    show kw spring surp with dspr
    kw "Как это «{i}всё равно{/i}»?"
    kw "Мопеды намного меньше и скорость у них ниже!"
    kw "К тому же большинство этих не на бензине, а на электричестве!"
    th "Ага-а…"
    th "Нашёлся и у нашего принца зазор в доспехах…"
    me "Тихо-тихо!"
    me "Чего загорелось-то так?"
    show kw spring ser with dspr
    kw "Ничего не загорелось."
    kw "Просто ты очевидные вещи называешь неправильно."
    me "Так почему их так много-то?"
    show kw spring with dspr
    kw "Они очень удобные в плане езды по городу."
    kw "Больше манёвренности, чем у машины.{w} Легче получить права."
    kw "Опять же, электричество дешевле бензина."
    me "Может, тоже такой взять?"
    kw "Для Шанхая это вряд ли будет хорошей идеей."
    kw "У нас движение намного плотнее."
    kw "Тем более где ты его хранить будешь?"
    me "А где ты свой мотоцикл хранишь?"
    show kw spring smile with dspr
    kw "Во дворе."
    me "И не крадут?"
    kw "Нет."
    me "Так я тоже так буду ставить."
    kw "Нет, лучше не надо."
    me "Почему?"
    show kw spring smile_2 with dspr
    kw "У тебя украдут в первую же ночь."
    play music loveis fadein 2
    $ volume(0.6, "ambience") 
    me "Почему это у тебя не крадут, а у меня украдут?"
    show kw spring smirk with dspr
    kw "Потому что все знают, что ты растяпа."
    $ renpy.pause (1,hard=True)
    with hpunch
    me "!!!"
    "Вебер расхохотался."
    me "Ты же в курсе, что дома у меня живут два паука — Василий и Аркадий?"
    me "Так вот."
    me "Однажды я не удержусь, принесу одного к тебе домой и положу в кровать."
    show kw spring surp with dspr
    me "Посмотрю, кто из них крупнее, и положу."
    "Вебер ощутимо вздрогнул."
    kw "Ну уж нет!"
    me "Уже не так весело, да?"
    "Я был в курсе его маленькой фобии."
    th "Удар ниже пояса, но что поделать…"
    "Как следует насладившись выражением на его лице, я махнул рукой."
    me "Ладно, так и быть. Живи, дитя."
    show kw spring smile_2_2 with dspr
    kw "Choke me harder, daddy!"
    me "???"
    show kw spring with dspr
    kw "Тебе что-то послышалось?"
    me "Да…"
    $ renpy.pause (0.5,hard=True)
    stop ambience fadeout 2
    window hide
    scene black with dissolve
    hide kw spring
    $ renpy.pause (1,hard=True)
    window show
    "Мы перешли дорогу и вышли к отелю."
    "Вебер поговорил на ресепшене, и нам выдали ключ от комнаты."
    "Мы оставили там вещи и вышли."
    $ renpy.pause (1,hard=True)
    scene bg hotel_nearby with dissolve
    play ambience street_consular fadein 3
    $ volume(0.5, "ambience")
    $ renpy.pause (1,hard=True)
    show kw spring ser with dspr
    kw "Итак."
    kw "Наша первая встреча запланирована в саду «Master of the Nets»."
    me "«Master of the Nets»?"
    me "А на русском никак?"
    me "Да что это за понты такие?"
    me "Типа прожил пару лет за границей и можешь вставлять в речь английские словечки?"
    me "Как же низко вы пали, господин!"
    show kw spring disapp with dspr
    kw "Успокойся."
    show kw spring with dspr
    kw "Я просто не знаю, как это название будет по-русски."
    me "Сад Мастера нитей?.."
    kw "Не звучит."
    kw "Ладно, неважно."
    kw "В наказание за твою строптивость до сада мы пойдём пешком."
    kw "В следующий раз будешь думать, прежде чем спорить со старшими."
    kw "С консулом ты же так не споришь?"
    kw "Так кто тебе право дал с вице-консулом так себя вести?"
    me "Ладно, ладно."
    me "Чего завёлся?"
    me "Консула я не знаю с восьми лет."
    me "А так — кто знает, может, и с ним бы спорил."
    me "Слушай, а давай ты консулом станешь, а?"
    me "А я вице-консулом!"
    me "Ещё больше времени вместе будем проводить!"
    show kw spring thinking with dspr
    kw "…"
    kw "Пожалуй, это первый раз в жизни, когда я готов отказаться от повышения…"
    scene bg scenery_qiao_2 with dissolve
    me "Смотри! Мост!"
    "Выглядело действительно впечатляюще."
    "Вода была тёмно-зелёного цвета и вкупе со всей зеленью и атмосферой города производила впечатление чего-то необычного."
    show kw spring smile with dspr
    kw "Ты знал, что Сучжоу называют китайской Венецией?"
    me "Правда? Не знал."
    kw "Тут таких мостов…"
    show kw spring smile_2 with dspr
    extend " пруд пруди!"
    me "Ба-дум-тсс."
    me "Значит, ты здесь уже был?"
    show kw spring with dspr
    kw "Пару раз."
    me "Пару раз?"
    kw "В прошлом году и месяц назад."
    me "А сейчас?.."
    kw "Нет. Даже не спрашивай."
    me "Опять твои тайные делишки?"
    show kw spring smile_2 with dspr
    kw "А как иначе?"
    "Долгий взгляд."
    th "Ну же…"
    th "Ещё немного…"
    show kw spring disapp with dspr
    kw "Ладно."
    show kw spring with dspr
    kw "Мне надо передать одну очень важную вещь нашему агенту."
    kw "Основная загвоздка в том, что я не знаю, как выглядит он, а он не знает, как выгляжу я."
    kw "Задача осложняется тем, что у нас есть три потенциальных места, где он может быть."
    kw "Это сад, приозёрная территория и Пинцзянлу — главная улица Сучжоу."
    show kw spring ser with dspr
    kw "Ещё более задача осложняется тем, что наши соперники с сам понимаешь какой страны прознали про нашу затею и наверняка попытаются её сорвать."
    kw "Я подозреваю, что в каждом из трёх мест у них есть по человеку."
    kw "Теперь всё зависит от того, с какого раза мы попадём в нужную точку."
    me "А связаться с ним ты не можешь?"
    kw "Увы, нет. В целях безопасности связь с ним поддерживалась по шифрованному каналу в консульстве."
    me "А что ж ты его с собой не взял?"
    show kw spring thinking with dspr
    "Вебер вздохнул."
    kw "Шутишь?"
    kw "Я, конечно, понимаю, что ты гантельки по выходным поднимаешь, но тарелку приёма спутниковой связи весом килограмм в четыреста ты бы явно не осилил."
    me "Я понял, хватит."
    me "Так куда мы идём сначала?"
    kw "В сад."
    $ renpy.pause (1,hard=True)
    window hide
    stop ambience fadeout 2
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg garden_outside_path with dissolve
    window show
    "Спустя полчаса Вебер сказал, что мы почти пришли."
    "Мои ноги дрожали от напряжения, и я пытался выдумать какую-нибудь мало-мальски дельную отговорку, чтобы присесть отдохнуть."
    me "А там продают лимонад?"
    show kw spring with dspr
    kw "Только сидр."
    me "Тоже сойдёт."
    show kw spring ser with dspr
    "Вебер закатил глаза и хлопнул себя ладонью по лбу."
    kw "Ну какой сидр? Это же сад-музей!"
    kw "Я бы сказал, что тебе голову напекло, но учитывая, что сейчас пасмурно…"
    me "Ой, всё!"
    "Впереди мы увидели кассу."
    me "Тут что, за вход ещё платить надо?"
    show kw spring ser with dspr
    kw "А ты как думал?"
    kw "Надо же им как-то всю эту красоту сохранять."
    hide kw with dspr
    $ renpy.pause (1,hard=True)
    show kw spring with dspr
    "Вебер отошёл и через несколько минут вернулся с билетами."
    me "Серьёзно? 40 юаней за вход?!"
    kw "Да брось. Просто наслаждайся прогулкой."
    me "Ну ладно…"
    scene bg garden_2 with dspr
    play ambience ambience_camp_entrance_day_people fadein 2
    "Мы вошли в сад."
    "Он был очень… аутентичным."
    "Казалось бы, типичный китайский сад, чудом сохранившийся с 14-го века, но…"
    "На меня он произвёл впечатление необычайного, диковинного места."
    "Так много воды, растительности и камней!.."
    th "Да, немудрено, что здесь столько людей…"
    "Я ходил туда-сюда и фотографировал всё на телефон под неодобрительные взгляды Вебера."
    kw "Ты бы лучше так всё смотрел, чем через экран своего… смартфона."
    me "Одно другому не мешает."
    me "Сноб!"
    scene bg garden_stones with dspr
    play ambience ambience_camp_entrance_day_people fadein 2
    me "Смотри, какие камни!"
    show kw spring with dspr
    kw "Их трогать нельзя, ты же в курсе?"
    me "Да что им будет-то…"
    me "Пятьсот лет простояли, столько же ещё простоят."
    "Я дотрагиваюсь до верхнего камня."
    "К моему удивлению, его поверхность совсем не гладкая, как могло показаться на первый взгляд."
    "Хотя за столько лет он уже должен бы блестеть, наверное…"
    me "Эм…"
    show kw spring smile_2 with dspr
    kw "Что такое?"
    kw "Тебя кто-то схватил?"
    me "Там что-то есть."
    kw "Где, в камне?"
    me "Да."
    "Я вожу рукой по внутренней части камня и достаю… "
    show cigarette with dspr
    $ renpy.pause (0.5,hard=True)
    show kw spring ser with dspr
    extend "сигарету?"
    me "???"
    kw "Дай-ка посмотреть."
    hide cigarette with dspr
    "Вебер берёт у меня сигарету и проводит ею у себя под носом."
    show kw spring smile with dspr
    kw "М-м-м…"
    kw "Табак!"
    me "А ты ожидал {i}унюхать{/i} что-то иное?"
    kw "Ну знаешь…{w} Травы много бывает."
    kw "Судя по лейблу, она не из Китая."
    kw "Лежит здесь уже минимум дней пять."
    me "С чего ты взял?"
    show kw spring with dspr
    kw "Ну… Хотя бы с того, что последний раз дождь здесь был в прошлый вторник."
    kw "Помнишь, у нас тоже ливень шёл весь день?"
    kw "Обычно у Шанхая и Сучжоу примерно одинаковая погода, так как они недалеко друг от друга."
    show kw spring ser with dspr
    kw "Но зачем кому-то оставлять её здесь?"
    me "Может, это какой-то знак для других людей?"
    show kw spring smile_2_2 with dspr
    kw "Совсем забил голову шпионскими идеями, да?"
    kw "Держи."
    "Вебер отдаёт мне сигарету."
    me "Но я не курю…"
    kw "А я курю?"
    me "Бывает, что и куришь…"
    kw "Поэтому и держи."
    stop ambience fadeout 2
    stop music fadeout 2
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg garden_bookshop with dissolve
    play ambience ambience_library_day fadein 2
    window show
    "Мы зашли в одну из пагод и оккупировали пару деревянных стульев."
    me "Ну как, напал на след своего Джеймса Бонда?"
    show kw spring with dspr
    kw "Нет. Сомневаюсь, что он вообще здесь."
    kw "Видишь, сколько людей? Это как иголку в стоге сена искать."
    me "А что именно ты должен ему передать? Ту шкатулку?"
    me "А что в ней?"
    me "Коды запуска межконтинентальных ракет?"
    show kw spring disapp with dspr
    kw "Я же просил не спрашивать…"
    me "Молчу-молчу!"
    me "Кстати, а что это за здание?"
    show kw spring with dspr
    kw "Библиотека императора."
    kw "Временная, конечно."
    kw "Когда он ехал из одного города в другой, то порой останавливался отдохнуть где-то посередине."
    kw "Порой его «отдых» затягивался на пару дней, а то и больше, так что специально для него стали строить подобные сады и все здания."
    kw "Так как император приезжал не так часто, большую часть времени здесь жил кто-то другой."
    kw "Вот этот мастер нитей, например."
    show kw spring ser with dspr
    kw "Хотя я более чем уверен, что он был либо художником, либо поэтом."
    kw "А «мастер нитей»… Титул, должно быть?"
    me "Смотри, там книги продают!"
    me "Пошли посмотрим!"
    hide kw with dspr
    "Я вскочил и направился к продавцу."
    me "Здравствуйте!"
    me "Есть у вас что-нибудь интересное?"
    show bkm with dissolve
    bkm "Добрый день, юноша. Смотря что вы считаете интересным."
    bkm "У нас есть стихи поэтов 7-го — 8-го веков и истории из жизни того периода."
    me "Давайте стихи."
    me "Какие авторы есть?"
    bkm "Выбор широкий. Ли Бо, Ду Фу, Бо Цзюйи…"
    show kw spring at right with dissolve
    kw "Бери Ду Фу. Он не такой сложный, как остальные."
    me "Ты читал?"
    kw "Немного. В институте ещё."
    kw "А зачем ты хочешь взять?.."
    me "Вокабуляр прокачивать."
    show kw spring thinking with dspr
    kw "?"
    me "Ну слова новые узнавать."
    show kw spring ser with dspr
    kw "А."
    kw "Для этой цели лучше бы подошла современная литература…"
    kw "Но раз уж тебе интересно, то почему бы и нет."
    kw "Дайте нам, пожалуйста, один сборник Ду Фу и…"
    kw "У вас нет произведений поэта Сюй Чжимо?"
    kw "«Флорентийская ночь», например."
    kw "Это двадцатый век, но всё же…"
    show bkm smile with dspr
    bkm "О, вы разбираетесь в поэзии?"
    bkm "Вы учёный?"
    kw "Не совсем.{w} Но культуру Китая люблю."
    bkm "Сейчас посмотрю…"
    $ renpy.pause (1,hard=True)
    bkm "К сожалению, «Флорентийской ночи» нет.{w} Есть «Скитания в облаках»."
    show kw spring smile with dspr
    kw "Это тоже подойдёт."
    bkm "Рад сообщить, что при покупке двух книг третью вы получаете в подарок!"
    show kw spring smile_2 with dspr
    kw "Вот оно как. Это мы удачно зашли."
    me "Не то слово."
    me "А что за книга?"
    bkm "Наугад из всего ассортимента по выбору продавца."
    hide bkm with dspr
    "Продавец закрывает глаза, оборачивается к стеллажам и протягивает руку."
    show bkm laugh with dspr
    bkm "Вот и ваш подарок!"
    "Продавец положил книги в тканевый пакет и с поклоном вручил нам."
    me "Спасибо!"
    "Вебер тем временем отсканировал QR-код магазина телефоном."
    show kw spring smile with dspr
    kw "Благодарю вас за хороший сервис."
    bkm "Что вы. Будем рады видеть вас ещё."
    hide kw with dspr
    hide bkm with dspr
    "Я пока решил взглянуть на подаренную книгу."
    show book_title with dspr
    me "Ух ты… Заголовок латиницей…"
    hide book_title with dspr
    show book_bookshop with dissolve
    "Я открыл книгу где-то в середине и увидел странный знак."
    me "Вебер, смотри! Тут какие-то пометки карандашом!"
    me "Вебер-р-р!"
    hide book_bookshop with dspr
    "Карл стоял возле прилавка и о чём-то говорил с продавцом."
    me "Ну ладно, потом покажу."
    "Я подошёл к ним."
    show kw spring ser at left with dspr
    show bkm at right with dspr
    bkm "К озеру вы можете попасть с помощью автобуса или метро. Я бы порекомендовал второй вариант — будет намного быстрее."
    bkm "Выходите из парка, идёте прямо и упираетесь прямо в станцию."
    bkm "Выбираете зелёную ветку и едете на восток три остановки."
    bkm "Выходите на станции Сянмэн."
    show kw spring smile with dspr
    kw "Спасибо."
    kw "Семён, пошли."
    stop ambience fadeout 2
    hide bkm with dspr
    $ renpy.pause (0.5,hard=True)
    scene bg garden_outside_path with dissolve
    "Мы вышли из парка."
    me "Всё-таки его здесь не было?"
    show kw spring ser with dspr
    kw "Вряд ли."
    kw "Он бы успел нас заметить. Парк-то небольшой."
    me "Да уж, небольшой!"
    me "Я уже себе все ноги оттоптал!"
    show kw spring disapp with dspr
    kw "Да ты из магазина только что вышел!"
    me "Ладно, не важно…"
    me "Ну что, на метро?"
    show kw spring with dspr
    kw "Нет."
    kw "Едем на Пинцзянлу."
    me "Как это? Ты же спрашивал, как доехать до озера!"
    kw "Ну и что?"
    kw "Думаешь, замести следы будет излишним?"
    kw "К тому же откуда ты знаешь, что продавцу можно доверять?"
    me "Может быть, потому что он… продавец?"
    show kw spring thinking with dspr
    kw "Не знаю, не знаю…"
    kw "В поэзии он вроде разбирается…"
    show kw spring with dspr
    kw "Всё равно, дело слишком важное, чтобы ослаблять бдительность."
    kw "Пошли. Я примерно представляю, куда идти."
    window hide
    hide kw with dspr
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    play ambience ambience_day_countryside_ambience fadein 2
    $ volume(0.8,'ambience')
    scene bg pingjianglu_boat with dissolve
    $ renpy.pause (1,hard=True)
    window show
    "Улица оказалась недалеко — буквально в десяти минутах ходьбы."
    show kw spring ser at right with dspr
    kw "Смотри в оба."
    kw "Обращай внимание на людей, которые явно не хотят привлекать к себе внимания."
    me "Эм…{w} Это как?"
    kw "Которые выглядят {i}слишком обычно{/i}."
    kw "Понятно?"
    me "Нет."
    show kw spring thinking with dspr
    "Вебер сердито цокнул языком."
    kw "Ладно. Просто иди и смотри по сторонам."
    me "…"
    hide kw with dspr
    "Я отвернулся и стал наблюдать за обстановкой."
    "Людей вокруг было не слишком много."
    "И что интересно, большая их часть была не иностранцами, а китайцами."
    th "Какой странный способ внутреннего туризма…"
    th "Хотя у нас тоже много кто из Москвы ездит в Казань, посмотреть на мечеть и Кремль…"
    "Но чего в Казани точно нет — так это реки, текущей прямо посередине города!"
    "Хотя там есть озеро Кабан.{w} Но ведь озеро — не так здорово, да?"
    "Можно даже арендовать лодочку и покататься."
    me "Ты бы хотел здесь жить?"
    show kw spring at right with dspr
    kw "Нет, вряд ли."
    kw "Подобные города интересны с точки зрения туризма."
    kw "А так, здесь слишком скучно."
    kw "Хотя мне кажется, по сравнению с Шанхаем любой город покажется скучным…"
    kw "Ну ладно, зато час езды всего."
    show kw spring smile_2 with dspr
    kw "А почему спрашиваешь?"
    me "Просто так."
    me "Мне город понравился."
    me "Действительно, не такой шумный, как Шанхай или Гуанчжоу."
    me "И посмотри вокруг — так много зелени и воды!"
    me "Может быть, в душе мне больше нравятся именно такие города."
    me "Тихие и маленькие."
    me "Давай, как нам будет лет по шестьдесят, сюда переедем.{w} М-м-м?"
    kw "Ты что, собрался со мной всю жизнь провести?"
    me "А почему нет? Весело же!"
    show kw spring smile with dspr
    kw "У меня, признаться, были другие планы…"
    me "Ну вот."
    me "А я-то думал, что мы будем с тобой как Холмс и Ватсон…"
    me "Или как Пуаро и Гастингс…"
    show kw spring smirk with dspr
    kw "Ты же в курсе, что и Ватсон, и Гастингс впоследствии женились?"
    kw "Тебе тоже придётся."
    me "А кто тебе сказал, что ты Холмс?"
    show kw spring with dspr
    "Вебер хотел что-то ответить, но вдруг резко сделал шаг в сторону."
    kw "Сюда!"
    hide kw with dspr
    "Он схватил меня за руку и втащил за угол."
    with hpunch
    me "Эй, в чём дело?"
    show kw spring with dspr
    kw "Прислонись к стене."
    me "Да зачем?"
    show kw spring ser with dspr
    "Вебер показал пальцем в сторону дороги."
    me "И что там?"
    kw "Смотри внимательнее.{w} Чуть левее."
    "Я присмотрелся…"
    show bkm far at right with dissolve
    "И увидел вдалеке давешнего продавца."
    "Он стоял посреди улицы и озирался, словно кого-то потерял."
    show kw spring smile_2 with dspr
    kw "Какое странное стечение обстоятельств, не так ли?"
    me "Да уж, очень странное."
    me "Пойдём поздороваемся?"
    show kw spring thinking with dspr
    kw "…"
    me "Понял-понял! Не время для шуток."
    me "Думаешь, он за нами?"
    show kw spring with dspr
    "Вебер ещё раз взглянул на него."
    kw "Скорее всего."
    me "А как он нас нашёл?"
    me "Ты же ему сказал, что мы к озеру идём?"
    kw "Видимо, он просто шёл за нами всю дорогу."
    me "И что мы будем делать?"
    hide bkm with dspr
    show kw spring ser with dspr
    "Вебер задумался."
    kw "Ты помнишь примерно, где находится наш отель?"
    me "Шутишь, что ли?"
    kw "Хм…"
    show kw spring with dspr
    kw "Ну ладно, вроде пока не темнеет."
    kw "Телефон у тебя с собой? Давай сюда."
    hide kw with dspr
    "Вебер забрал у меня телефон и открыл навигатор."
    show kw spring with dspr
    kw "Посмотри."
    kw "Сейчас мы с тобой разделимся, и тебе надо будет явиться вот сюда."
    kw "Понял?"
    me "Ну…"
    me "Понял."
    me "А зачем нам разделяться?"
    show kw spring ser with dspr
    kw "Чтобы избавиться от слежки."
    kw "Что за глупый вопрос?"
    me "А что ты хочешь делать?"
    kw "Когда услышишь сигнал, беги вон на ту улицу."
    me "Какой сигнал?"
    show kw spring smile_2 with dspr
    kw "Ты поймёшь."
    hide kw with dspr
    "Вебер улыбнулся и без лишних слов скрылся из виду."
    th "Что он опять придумал?"
    "Я решил подождать."
    show bkm far at right with dspr
    "Найдя взглядом продавца, я увидел, что он довольно медленно идёт по улице и кого-то высматривает."
    "Он подошёл к торговцу закусками и что-то у него спросил."
    "Тот помотал головой."
    show bkm far angry with dspr
    "Продавец рассердился."
    "Выглядело всё очень странно."
    hide bkm with dspr
    $ renpy.pause (0.5,hard=True)
    play sound boom_01
    $ renpy.pause (0.2,hard=True)
    with hpunch
    "Вдруг раздался взрыв."
    "Ладно, не взрыв. Хлопок."
    "Я дёрнулся было посмотреть, но вспомнил, что Вебер велел мне ждать сигнала."
    th "Должно быть, это он."
    "Я высунулся из-за угла, убедился, что продавца нет поблизости, и побежал к параллельной улице."
    stop ambience fadeout 2
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg park_1 with dissolve
    $ renpy.pause (1,hard=True)
    play ambience ambience_camp_center_day fadein 2
    window show
    "Пройдя метров триста, я вышел к небольшому парку."
    th "Судя по всему, это здесь."
    "Я спрятал телефон в карман и стал оглядываться по сторонам."
    "Вебера пока нигде не было видно."
    "Я решил пройти чуть дальше на случай, если он уже ждёт меня где-то неподалёку."
    $ renpy.pause (0.5,hard=True)
    scene bg park_lake with dspr
    "Я побродил по парку минут десять и вышел к небольшому озеру."
    th "Не удивлюсь, если в этом городе действительно воды больше, чем земли…"
    "Интересно, что людей здесь совсем нет."
    "Учитывая то, насколько это место живописное, это было даже в какой-то мере необычно."
    "Ни туристов, фотографирующих красоты древнего Китая для Инстаграма…"
    "Ни парочек, сидящих на траве с намерением провести пикник…"
    "В общем, никого."
    "Кроме меня."
    whoisthat "И меня."
    "Я вздрогнул и обернулся."
    play music music_list["mystery_girl_v2"] fadein 2
    $ volume(0.7, "music")
    $ volume(0.5,'ambience')
    show uv smile with dissolve
    "Прямо на меня смотрела девочка…{w} с кошачьими ушками?"
    me "Втф…"
    me "Это как вообще?"
    kit "Вот так."
    kit "Ты можешь даже не говорить. Просто думай — я пойму."
    th "Думать? О чём думать?"
    th "А уши, интересно, настоящие?.."
    show uv upset with dspr
    kit "А ты как думаешь?"
    kit "Конечно настоящие!"
    kit "Потрогать не дам.{w} Не на первой встрече."
    th "Блин! Она действительно читает мои мысли!"
    me "А ты кто?"
    show uv normal with dspr
    kit "Я?"
    show uv smile with dspr
    kit "Я Юля."
    me "Юля?.."
    me "Просто Юля и всё?"
    uv "Ну да. А тебе ещё что-то нужно?"
    uv "А, да! Чуть не забыла!"
    uv "Я ещё твой хранитель!"
    me "Чего? Хранитель?"
    me "Да быть того не может!"
    me "Мой «хранитель» не может быть кошкодевочкой!"
    me "У меня-то таких ушей нет!"
    me "Или есть?"
    "На всякий случай я потрогал макушку и убедился, что за эти несколько минут ничего нового на голове не появилось."
    show uv upset with dspr
    "Девушка вздохнула."
    uv "Думаешь, я ошиблась?"
    uv "Смотри."
    show uv normal with dspr
    uv "Видеть твои мысли я могу?"
    me "Ну… Да вроде. Если это не какой-то трюк."
    uv "Хорошо."
    uv "Далее прошлое."
    uv "Это ты сегодня утром думал, что тебе неимоверно скучно?"
    me "Ну я… Да так много кто думает, знаешь!"
    uv "Потом ты внезапно обрадовался."
    me "А поконкретнее?"
    show uv upset with dspr
    uv "Не могу поконкретнее."
    uv "Я вижу только чувства и эмоции в ближайшем прошлом и будущем."
    show uv smile with dspr
    uv "В общем, прекращай валять дурака, Семён. Ошибки быть не может."
    me "Ну ладно…"
    me "Чем обязан своему милому…{w} хранителю?"
    show uv surprise with dspr
    uv "В смысле чем обязан?"
    uv "Я почувствовала, что ты в смертельной опасности!"
    show uv rage with dspr
    "Юля приняла воинственный вид."
    uv "Показывай, где они!"
    me "Кто «они»?"
    uv "Ну негодяи, которые хотят тебя убить!"
    "Я огляделся вокруг. В радиусе метров трёхсот не было ни души."
    "Что было очень кстати! Не уверен, как люди вокруг отнеслись бы к…{w} таким ушкам."
    me "Да нет здесь никого…"
    show uv surprise with dspr
    me "Да и вообще всё со мной нормально!"
    me "Видишь: цел, жив, здоров…{w} Ну, почти здоров."
    me "Насморк считается за смертельную опасность?"
    show uv upset with dspr
    uv "Сомневаюсь."
    uv "Если тебе действительно ничего не угрожает… {w}Что же я почувствовала?"
    uv "Заряд энергии был настолько высоким, что у меня едва голова не раскололась!"
    th "Хм…"
    me "Не может же это быть тот продавец?.."
    uv "Какой продавец?"
    uv "Ну…"
    "Я заметил неподалёку беседку и предложил Юле продолжить разговор там."
    stop music fadeout 2
    hide uv with dissolve
    window hide
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    scene bg park_jul with dissolve
    play ambience ambience_camp_center_day fadein 2
    show uv smile with dissolve
    window show
    uv "Вот так, значит…"
    me "Как думаешь, могла опасность исходить от него?"
    uv "Вряд ли. Это же был обычный смертный, да?"
    uv "Тем более твой друг его отвлёк…"
    uv "Нет, дело тут наверняка в чём-то другом."
    th "Смертный?.."
    show uv normal with dspr
    "Юля задумалась."
    uv "Понимаешь, сейчас ты входишь в такую пору жизни, когда каждое твоё решение будет иметь довольно ощутимые последствия."
    uv "Может быть, ты думал о чём-то и собирался сделать какой-то выбор?"
    me "Да нет же!"
    me "Единственное моё важное решение за последние несколько дней — это какой соус к лапше брать."
    "Юля пристально посмотрела на меня."
    show uv smile with dspr
    uv "Ладно, раз уж я здесь…"
    uv "Дам тебе совет."
    uv "Думай тщательно."
    uv "Скоро тебе предстоит сделать выбор, от которого будет зависеть твоя судьба и судьбы многих других вокруг тебя."
    uv "Ну, не многих…{w} Но пары людей точно."
    uv "Я не могу сказать, что это будет: это ещё не определено."
    uv "Это {i}должно{/i} случиться, но какую форму это примет — неизвестно."
    "Мне стало не по себе."
    me "А как скоро это случится?"
    uv "Не знаю. Может, завтра."
    uv "А может, и через год."
    uv "Для вечных существ время — понятие довольно весёлое."
    me "А ты вечное существо?"
    show uv laugh with dspr
    uv "Хотелось бы верить!"
    uv "Тысяча лет пролетела как один миг. Хотя для кого-то и столетие кажется бесконечным."
    me "То есть я у тебя уже не первый…{w} подопечный?"
    uv "Нет. Первый и последний."
    show uv smile with dspr
    uv "Представь линию. Без начала и конца."
    uv "Просто прямую."
    uv "Представил?"
    uv "А теперь чуть ниже нарисуй отрезок."
    uv "Так вот, первая линия — это моя жизнь."
    uv "Отрезок — твоя."
    uv "Я жила много веков до тебя и буду жить ещё долго после."
    uv "Но суть моего существования — ты."
    show uv upset with dspr
    uv "Тебе, наверное, не совсем понятно, да?"
    me "Почему? Всё понятно."
    show uv smile with dspr
    uv "Хорошо."
    uv "То, как ты проживёшь свою жизнь, какие {i}выборы{/i} совершишь и решения примешь, отразится и на мне тоже."
    uv "В какой-то мере твоя жизнь тебе не принадлежит. Она принадлежит нам обоим."
    uv "Поэтому я так испугалась, когда почувствовала опасность."
    me "Хм."
    me "А можно нескромный вопрос?"
    show uv upset with dspr
    uv "Нескромный? Ну давай."
    me "Почему ты так странно выглядишь?"
    show uv smile with dspr
    "Юля улыбнулась."
    uv "Потому что ты так захотел."
    me "Я?"
    me "Ничего я не хотел!"
    me "Я вообще больше собак люблю."
    uv "Себя не обманешь, Семён."
    me "Всё равно не понимаю."
    uv "Всё ты понимаешь…"
    show uv upset with dspr
    "Вдруг Юля скривилась."
    uv "Жаль прерывать нашу беседу, но моё время на исходе."
    uv "Я рада, что нам удалось поговорить."
    uv "Место и впрямь замечательное."
    hide uv with dspr
    "Юля поднялась."
    show uv smile with dspr
    uv "А, кстати!"
    uv "Чуть не забыла!"
    uv "Плохая новость: через две минуты ты обо мне забудешь!"
    with hpunch
    me "ЧЕГО?!"
    show uv laugh with dspr
    uv "Да-да, вот так вот."
    uv "Секрет фирмы не может выдать никто.{w} Ну, {i}почти{/i} никто."
    show uv smile with dspr
    uv "Но выше нос, есть и хорошая новость: мысль про важность выбора у тебя останется."
    uv "Считай, что нам удалось обхитрить систему."
    uv "Если в следующий раз тебе снова будет грозить опасность, я перемещу тебя в свой мир."
    uv "Находиться здесь вот так слишком трудно."
    uv "Только посмотри, как странно здесь всё выглядит."
    me "Твой мир?"
    show uv laugh with dspr
    uv "Тебе он понравится."
    show uv smile with dspr
    uv "Прощай!"
    uv "Я буду следить за тобой, пока ты спишь."
    me "Стой! У тебя…"
    hide uv with dissolve
    "Юля улыбнулась и исчезла."
    "Без вспышки, огней или дыма."
    "Просто взяла и исчезла."
    me "Хвост…"
    "Я тряхнул головой и опустился обратно на скамейку."
    th "Даже не буду спрашивать, что сейчас было."
    th "Опасность…{w} Юля…"
    "Всё смешалось воедино."
    th "Я сделал глубокий вдох и…"
    with hpunch
    "…"
    kw "Эй! Вот ты где!"
    show kw spring with dspr
    me "Блин, сколько можно тебя ждать?"
    me "Я уже полчаса тут сижу!"
    show kw spring thinking with dspr
    kw "Так ты сам виноват. Я тебе отметку перед входом в парк поставил, а ты вообще в другом месте."
    me "А позвонить никак?"
    kw "У тебя недоступен."
    "Я достал телефон."
    me "И правда…"
    show kw spring ser with dspr
    kw "В общем, так."
    kw "Я связался с центром, и мне сказали, что операция отменяется."
    kw "Слишком высок риск провалиться."
    kw "Завтра утром поедем домой."
    kw "А пока — в отель."
    kw "Марш-марш!"
    me "Может, на такси?.."
    show kw spring smirk with dspr
    kw "Какое тебе такси?"
    kw "Двигай давай!"
    kw "Утешай себя мыслью, что гулять полезно."
    me "Но ведь…{w} 25 тысяч шагов уже…"
    hide kw with dissolve
    window hide
    stop ambience fadeout 2
    $ renpy.pause (0.5,hard=True)
    scene black with dissolve
    $ renpy.pause (1,hard=True)
    window show
    "На следующее утро мы позавтракали и сразу же отправились на поезд."
    scene bg train_inside with dissolve
    $ renpy.pause (1,hard=True)
    play ambience train_moving fadein 2
    window show
    show kw spring with dspr
    me "Как думаешь, кем в итоге был тот продавец?"
    me "Агентом? Или вражеским шпионом?"
    kw "Не знаю. Быть может, обыкновенным продавцом."
    me "Тогда почему он следил за нами?"
    kw "Может, передать чего хотел?"
    show kw spring smile_2 with dspr
    kw "Или ещё одну книгу подарить?"
    "Вебер расхохотался."
    me "Да уж. Весело тебе."
    hide kw with dspr
    "Кстати, надо будет глянуть дома ту книгу."
    "Можно было бы и сейчас, но…{w} Что-то не хочется."
    "Тем более мы уже почти приехали."
    stop ambience fadeout 2
    $ renpy.pause (1,hard=True)
    scene bg consular with dissolve
    play ambience street_consular fadein 2
    "Добравшись до консульства, я собирался было зайти внутрь, но увидел, что Вебер остался на улице."
    me "Ты чего? Куда-то ещё поедешь?"
    show kw spring smile_2 with dspr
    kw "Да, мне ещё нужно доделать некоторые дела."
    kw "Увидимся!"
    hide kw with dspr
    "Карл махнул рукой и куда-то быстро пошёл."
    "Я хмыкнул."
    th "Не сидится же ему на месте…"
    th "Удивительно. Прошёл всего лишь день."
    th "А мне показалось, что целая вечность…"
    stop ambience fadeout 2
    $ renpy.pause (1,hard=True)
    scene bg pi213_cabinet_box with dissolve
    "Поздоровавшись с коллегами, я направился к своему кабинету."
    "Пора бы и поработать."
    "Я снял пиджак и аккуратно повесил его на стул."
    play sound telephone_ring fadein 3
    $ volume(0.3,'sound')
    "Зазвонил телефон."
    "Я потянулся за трубкой и увидел на столе… {w}ту небольшую коробочку, которую вчера показывал Вебер."
    "…"
    th "Как она здесь оказалась???"
    "Я поднял трубку телефона и сказал, что перезвоню."
    "Вряд ли собеседник на том конце провода остался доволен таким ответом, но что поделать."
    th "Если уж она находится в моём кабинете…"
    th "Лежит на моём столе…"
    th "Я имею право её открыть?"
    th "Ух. Надеюсь, там не бомба."
    "Я аккуратно поднял крышку и увидел кольцо с ярким синим камнем."
    th "Хм?.."
    "Под ним лежал листок бумаги."
    "…"
    play music blue_h_one fadein 2
    window hide
    $ set_mode_nvl()
    $ renpy.pause (0.5,hard=True)
    "Привет, Семён!"
    "Если ты это читаешь и меня нет рядом, то всё прошло замечательно!"
    "Зная, как ты не любишь экскурсии и отлынивать от работы, мне пришлось придумать целый план, чтобы хоть как-то вытащить тебя на день."
    "Надеюсь, тебе понравилось наше небольшое путешествие в Сучжоу. Красивый город, не так ли?"
    "Ну что ж…"
    "С днём рождения!"
    "Расти большой и не забывай о том, что каждый пришёл в этот мир с какой-то целью. Миссией, если угодно."
    "Выполни свою полностью и не сворачивай с пути."
    "Я буду рядом, если понадобится помощь."
    "Выбирай внимательно."
    "Всегда твой,"
    "Карл Вебер."
    $ renpy.pause (0.5,hard=True)
    $ set_mode_adv()
    window show
    $ renpy.pause (0.5,hard=True)
    "Я взглянул на календарь."
    "Двадцать третье число."
    "Действительно."
    "Вчера был мой день рождения."
    $ renpy.pause (1.5,hard=True)
    window hide
    $ renpy.pause (0.2,hard=True)
    scene black with dissolve2
    $ renpy.pause (2,hard=True)
    call screen menu_mist_1337_good
    $ renpy.pause (2,hard=True)


    label mist1337_quit:
    stop music fadeout 2
    return