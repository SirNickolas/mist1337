.PHONY: configure all zip clean distclean

SRC_DIR := src
TRG_DIR := Mist1337
SRCS := file_start.rpy
ARCHIVE := $(TRG_DIR).zip
BASE_COMMIT := upstream

COPIES := $(SRCS:%.rpy=$(TRG_DIR)/%.edit.rpy)
PATCHES := $(SRCS:%=$(TRG_DIR)/%.patch)

all: $(COPIES) $(PATCHES)

configure:
	mkdir -p $(TRG_DIR)

zip: $(ARCHIVE)

clean:
	$(RM) $(COPIES) $(PATCHES) $(ARCHIVE)

distclean: clean
	-rmdir -p $(TRG_DIR)

$(COPIES): $(TRG_DIR)/%.edit.rpy: $(SRC_DIR)/%.rpy
	cp $^ $@

$(PATCHES): $(TRG_DIR)/%.patch: $(SRC_DIR)/%
	git diff -U1 --ignore-space-at-eol $(BASE_COMMIT) $^ | unix2dos >$@

$(ARCHIVE): $(COPIES) $(PATCHES)
	zip --recurse-paths --filesync -q $@ $(TRG_DIR)
